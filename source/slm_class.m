classdef slm_class < mirror_class
    % Class for SLM.
    
    properties (SetAccess=private)
        ax % Axis handle
        command1_app % Last commands actually applied, 1D (float) [n_act]
        command2_app % Last commands actually applied, 2D (float)
            % [n_act_h, n_act_w]
        fig % Figure handle
        flat % Flat command
        flat_factory % Factory flat command
        gain % SLM gain, the amount of OPD (m) introduced by one unit of input
            % signal
    end
    
    properties (Constant)
        d_act = 16 % Number of active elements across the aperture diameter
            % >>> Put the correct value
        lambda = 800 % Operating wavelength (nm) % >>> Put the correct value
        min_comm = 0 % Minimum command value
        max_comm = 255 % Maximum command value
        flat_factory_lambda = 800 % Wavelength used to measure the factory flat
            % command (nm)
        n_act = 792 * 600 % Total number of active elements
        n_act_w = 792 % Number of active elements across the width
        n_act_h = 600 % Number of active elements across the height
        tel_res = 192 % Number of pixels across the aperture diameter in OOMAO
            % >>> Put the correct value. If this value is the same for SLM and
            % DM, move it to the 'mirror' class
        type = 'slm' % Type of active mirror (DM or SLM)
    end
    
    properties (Constant, Hidden)
        gains = [115, 118, 119, 121, 123, 126, 128, 131, 133, 135, 138, 140, ...
            142, 144, 147, 149, 152, 154, 157, 159, 162, 165, 167, 170, 172, ...
            174, 176, 178, 180, 182, 183, 185, 187, 189, 191, 193, 196, 198, ...
            201, 203, 206, 208, 210, 212, 214, 216, 217, 218, 221, 223, 224]
            % Signal levels to produce a 2*pi modulation at the wavelengths in
            % 'gain_lambdas'
        gain_lambdas = [620, 630, 633, 640, 650, 660, 670, 680, 690, 700, ...
            710, 720, 730, 740, 750, 760, 770, 780, 790, 800, 810, 820, 830, ...
            840, 850, 860, 870, 880, 890, 900, 910, 920, 930, 940, 950, 960, ...
            970, 980, 990, 1000, 1010, 1020, 1030, 1040, 1050, 1060, 1064, ...
            1070, 1080, 1090, 1100] % Wavelengths used to measure the gains (nm)
        type_str = 'SLM' % Text string of the type of active mirror
    end
    
    methods (Access=protected)
        
        function connect_control(obj)
            %{
            Connect the hardware.
            %}
            
            % Calculate the gain
            [~, gain_idx] = min(abs(obj.gain_lambdas - obj.lambda));
            gain_2pi = obj.gains(gain_idx);
            obj.gain = obj.lambda * 1e-9 / gain_2pi;
            
            % Load the flat
            obj.load_factory_flat()
            
            % Prepare the SLM display
            g_root = groot;
            screens = g_root.MonitorPositions;
            obj.fig = figure('position', ...
                [screens(2), screens(4), obj.n_act_w, obj.n_act_h], ...
                'menubar', 'none', 'toolbar', 'none', 'resize', 'off');
            obj.ax = axes('parent', obj.fig);
            axis off
            set(obj.ax, 'position', [0, 0, 1, 1], 'visible' , 'off')
            colormap('gray');
        end
        
        function load_factory_flat(obj)
            %{
            Load the factory flat command and use it as default flat command.
            %}
            
            % Load the factory flat command
            flat_factory_load = load(fullfile(fileparts(obj.folder), 'data', ...
                append('flat_factory_', num2str(obj.flat_factory_lambda), ...
                '.mat')));
            obj.flat_factory = flat_factory_load.map;
            obj.load_flat(obj.flat_factory);
        end
        
        function load_flat(obj, flat)
            %{
            Load the flat command.
            
            Parameters:
            flat (float) [n_act]: flat command
            %}
            
            % Load the flat command
            obj.flat = flat;
        end
        
        function disconnect_control(obj)
            %{
            Disconnect the hardware.
            %}
            
            % Delete the SLM display
            close(obj.fig)
        end
        
        function push_control(obj)
            %{
            Apply a command. The command is first wrapped and converted to
            8-bit integers.
            %}
            
            % Prepare the command
            obj.convert()
            
            % Apply the command
            imagesc(obj.ax, obj.command2_app)
            caxis(obj.ax, [obj.min_comm, obj.max_comm])
            axis off
            set(obj.ax, 'position', [0, 0, 1, 1], 'visible' , 'off')
            drawnow
            
            % Show the command
            if obj.show
                obj.show_comm()
            end
        end
        
        function convert(obj)
            %{
            Wrap the command, convert it to 8-bit integers and reshape it in 2D.
            %}
            
            % Wrap the command % >>> Substitute the wrapping by lenslets
            obj.command1_app = obj.command1;
            obj.command1_app(obj.command1_app < obj.min_comm) = obj.min_comm;
            obj.command1_app(obj.command1_app > obj.max_comm) = obj.max_comm;
            
            % Convert the command
            obj.command1_app = uint8(obj.command1_app);
            
            % Reshape the command
            obj.command2_app = reshape(obj.command1_app, ...
                [obj.n_act_h, obj.n_act_w]);
        end
        
        function  movie_conv = load_control(obj, movie_struct)
            %{
            Load a movie of commands and convert the values from m to input
            values.
            
            Parameters:
            movie_struct (struct): structure loaded from the movie file
            
            Return:
            movie_lin (float) [(obj.n_act_w * obj.n_act_h), t]: movie of
                commands
            %}
            
            % Load the file
            movie = movie_struct.slmWfCube;
            movie_size = size(movie);
            
            if ~((movie_size(1) == obj.n_act_h) && ...
                    (movie_size(2) == obj.n_act_w))
                error(append('Error: ''movie'' input value has to be an ', ...
                    'array of ', string(obj.n_act), ...
                    ' elements in every frame.'))
            end
            
            movie_lin = reshape(movie, obj.n_act, movie_size(3));
            movie_conv = movie_lin / obj.gain;
        end
    end
end

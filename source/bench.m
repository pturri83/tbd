classdef bench < handle
    %{
    Class for the whole TBD.
    %}
    
    properties (SetAccess=private)
        dm % DM object
        folder % Absolute path of the folder of the source code
        show_m % Show active mirrors' commands (true or false)
        slm % SLM object
        verbose % Show messages in the command window (true or false)
        wait % Wait for the hardware to settle at the beginning (WFS camera
            % temperature) (true or false)
        wfs % WFS object
    end
    
    methods
        
        function obj = bench(varargin)
            %{
            Creator. All hardware is connected.
            
            Parameters:
            show_m (logic, default=true): show active mirrors' commands
            verbose (logic, default=true): show messages in the command window
            wait (logic, default=false): wait for the hardware to settle at the
                beginning (WFS camera temperature)
            %}
            
            % Parse inputs
            p = inputParser;
            p.addParameter('show_m', true, @islogical);
            p.addParameter('verbose', true, @islogical);
            p.addParameter('wait', false, @islogical);
            p.parse(varargin{:});
            
            % Set the verbose status
            obj.show_m = p.Results.show_m;
            obj.verbose = p.Results.verbose;
            obj.wait = p.Results.wait;
            
            % Find source code folder
            folder = mfilename('fullpath');
            obj.folder = folder(1: (end - 6));
            
            % Connect hardware
            obj.connect()
            static.print_com(obj.verbose, 'Bench created')
        end
        
        function delete(obj)
            %{
            Destructor. Hardware is disconnected.
            %}
            
            % Delete
            obj.disconnect()
            static.print_com(obj.verbose, 'Bench deleted')
        end
        
        function connect(obj)
            %{
            Connect all TBD devices. The WFS detector is connected first to give
            it some time to cool down.
            %}
            
            % Connect
            obj.wfs = wfs_class(obj.verbose, obj.wait);
            obj.dm = dm_class(obj.verbose, obj.show_m);
            obj.slm = slm_class(obj.verbose, obj.show_m);
            
            % Settle the hardware at the end
            if ~obj.wait
                obj.wfs.wait_temp()
            end
        end
        
        function disconnect(obj)
            %{
            Disconnect all TBD devices.
            %}
            
            % Disconnect
            obj.dm.delete();
            obj.slm.delete();
            obj.wfs.delete();
        end
        
        function set_show(obj, show)
            %{
            Set if active mirrors' commands are shown.
            
            Parameters:
            show (logic): show active mirrors' command
            %}
            
            % Check input values
            if ~islogical(show)
                error('Error: ''show'' input value has to be logical.')
            end
            
            % Change if active mirrors' command are shown
            obj.dm_obj.show = show;
            obj.slm_obj.show = show;
        end
        
        function set_verbose(obj, verbose)
            %{
            Set if functions are verbose.
            
            Parameters:
            verbose (logic): show messages in the command window
            %}
            
            % Check input values
            if ~islogical(verbose)
                error('Error: ''verbose'' input value has to be logical.')
            end
            
            % Change verbosity
            obj.dm_obj.verbose = verbose;
            obj.slm_obj.verbose = verbose;
            obj.wfs_obj.verbose = verbose;
        end
        
        function set_wait(obj, wait)
            %{
            Set if the bench should wait for hardware to settle.
            
            Parameters:
            wait (logic): wait for hardware to settle
            %}
            
            % Check input values
            if ~islogical(wait)
                error('Error: ''wait'' input value has to be logical.')
            end
            
            % Change verbosity
            obj.wfs_obj.wait = wait;
        end
    end
end

classdef (Abstract = true) mirror_class < handle
    %{
    Superclass for active mirrors (DM or SLM).
    %}
    
    properties (SetAccess=private)
        ax_show % Axis handle to show the commands
        command1 % Last commands, 1D (float) [n_act]
        command2 % Last commands, 2D (float) [n_act_h, n_act_w]
        connected % Connected (true or false)
        fig_show % Figure handle to show the commands
        folder % Absolute path of the folder of the source code
        influence % Influence function array
        movie % Movie of commands (m)
        movie_frame % Current frame in the movie of commands (0: movie not yet
            % played)
        movie_frame_frac % Precise fractional current frame in the movie of
            % commands (0: movie not yet played)
        movie_frames % Number of frames in the movie of commands
        movie_step % Number of frames  stepped in the movie of commands
        movie_wrap % Movie of commands has wrapped around at least once (true or
            % false)
        oomao % Associated OOMAO object
        show % Show the commands
        verbose % Show messages in the command window (true or false)
    end
    
    properties (SetAccess=private, Abstract)
        flat % Flat command
        flat_factory % Factory flat command
    end
    
    properties (Abstract, Constant)
        d_act % Number of active elements across the aperture diameter
        n_act_w % Number of active elements across the width
        n_act_h % Number of active elements across the height
        n_act % Total number of active elements
        tel_res % Number of pixels across the aperture diameter in OOMAO
        type % Type of active mirror (DM or SLM)
    end
    
    properties (Abstract, Constant, Hidden)
        type_str % Text string of the type of active mirror
    end
    
    methods
        
        function obj = mirror_class(verbose, show)
            %{
            Creator. Hardware is connected and an OOMAO object is associated.
            
            Parameters:
            verbose (logic): show messages in the command window
            %}
            
            % Find source code folder
            folder = mfilename('fullpath');
            obj.folder = folder(1: (end - 13));
            
            % Connect the hardware
            obj.verbose = verbose;
            obj.show = show;
            obj.connect()
            
            % Create the OOMAO object
            obj.influence = gaussianInfluenceFunction(0.4, 0.5); % >>> Replace
                % this synthetic influence function with the real one
            obj.oomao = deformableMirror(obj.d_act, 'modes', obj.influence, ...
                'resolution',  obj.tel_res);
            static.print_com(obj.verbose, '%s created', obj.type_str)
        end
        
        function delete(obj)
            %{
            Destructor. Hardware is disconnected and the associated OOMAO object
            is deleted.
            %}
            
            % Delete
            obj.disconnect()
            obj.oomao.delete()
            static.print_com(obj.verbose, '%s deleted', obj.type_str)
        end
        
        function connect(obj)
            %{
            Connect the mirror.
            %}
            
            % Connect
            static.print_com(obj.verbose, '%s connecting...', obj.type_str)
            obj.connect_control()
            obj.connected = true;
            static.print_com(obj.verbose, '%s connected', obj.type_str)
        end
        
        function disconnect(obj)
            %{
            Disconnect the mirror.
            %}
            
            % Disconnect
            static.print_com(obj.verbose, '%s disconnecting...', obj.type_str)
            obj.disconnect_control()
            obj.connected = false;
            static.print_com(obj.verbose, '%s disconnected', obj.type_str)
        end
        
        function push(obj, array, varargin)
            %{
            Apply a command.
            
            Parameters:
            array (float) [n_act]: commands array
            silent (logic): silence messages in the command window, overriding
                the default option
            %}
            
            % Parse inputs
            p = inputParser;
            p.addParameter('silent', false, @islogical);
            p.parse(varargin{:});
            
            % Check input values
            if ~isfloat(array)
                error('Error: ''array'' input value has to be a float array.')
            end
            
            size_arr = size(array(:));
            
            if ~(size_arr(1) == obj.n_act)
                error(append('Error: ''array'' input value has to be an ', ...
                    'array of ', string(obj.n_act), ' elements.'))
            end
            
            % Apply the flat
            obj.command1 = array + obj.flat(:);
            
            % Apply the command
            obj.command2 = reshape(obj.command1, [obj.n_act_h, obj.n_act_w]);
            obj.push_control()
            
            if ~p.Results.silent
                static.print_com(obj.verbose, '%s command applied', ...
                    obj.type_str)
            end
        end
        
        function set_show(obj, show)
            %{
            Set if commands are shown.
            
            Parameters:
            show (logic): show commands
            %}
            
            % Check input values
            if ~islogical(show)
                error('Error: ''show'' input value has to be logical.')
            end
            
            % Change if commands are shown
            obj.show = show;
        end
        
        function show_comm(obj)
            %{
            Show the last command. The original command is showed, not the
            wrapped one.
            %}
            
            % Apply the command
            if isempty(obj.fig_show)
                obj.fig_show = figure('name', append(obj.type_str, ...
                    ' commands'));
                obj.ax_show = axes('parent', obj.fig_show);
            end
            
            imagesc(obj.ax_show, obj.command2)
            drawnow
        end
        
        function load_movie(obj, path)
            %{
            Load a movie of commands.
            
            Parameters:
            path (string): path of the movie file
            %}
            
            % Check input values
            if ~ischar(path)
                error('Error: ''path'' input value has to be characters.')
            end
            
            % Load the movie file
            static.print_com(obj.verbose, 'Loading %s movie...', obj.type_str)
            movie_struct = load(path);
            movie_load = obj.load_control(movie_struct);
            obj.movie = movie_load;
            movie_size = size(obj.movie);
            obj.movie_frames = movie_size(2);
            obj.movie_frame = 0;
            obj.movie_frame_frac = 0;
            obj.movie_step = 1;
            obj.movie_wrap = false;
            static.print_com(obj.verbose, '%s movie has loaded', obj.type_str)
        end
        
        function step_movie(obj)
            %{
            Step the movie of commands.
            %}
            
            % Advance the movie
            if obj.movie_frame_frac == 0
                next_frame = 1;
            else
                next_frame = obj.movie_frame_frac + obj.movie_step;
            end
            
            % Step the movie
            obj.set_movie_frame(next_frame);
        end
        
        function set_movie_frame(obj, movie_frame)
            %{
            Move to a movie frame and apply the command.
            
            Parameters:
            movie_frame (float): fractional movie frame to move to
            %}
            
            % Check input values
            if ~islogical(isfloat(movie_frame))
                error('Error: ''movie_frame'' input value has to be float.')
            end
            
            % Set the movie frame
            obj.movie_frame_frac = movie_frame;
            
            if obj.movie_frame_frac > obj.movie_frames
                obj.movie_frame_frac = obj.movie_frame_frac - obj.movie_frames;
                obj.movie_wrap = true;
            elseif obj.movie_frame_frac <= 0
                obj.movie_frame_frac = obj.movie_frame_frac + obj.movie_frames;
                obj.movie_wrap = true;
            end
            
            obj.movie_frame = round(obj.movie_frame_frac);
            
            % Move to the movie frame
            obj.push(obj.movie(:, obj.movie_frame), 'silent', true);
            static.print_com(obj.verbose, '%s movie has stepped to %u', ...
                obj.type_str, obj.movie_frame)
        end
        
        function set_movie_step(obj, movie_step)
            %{
            Set the movie step.
            
            Parameters:
            movie_step (int): movie step size
            %}
            
            % Check input values
            if ~islogical(isinteger(int8(movie_step)))
                error('Error: ''movie_step'' input value has to be integer.')
            end
            
            % Set the movie step
            obj.movie_step = movie_step;
        end
    end
    
    methods (Access=protected, Static)
        
        function connect_control()
        end
        
        function disconnect_control()
        end
        
        function push_control()
        end
        
        function load_control()
        end
    end
end

classdef dm_class < mirror_class
    % Class for DM.
    
    properties (SetAccess=private)
        flat % Flat command
        flat_factory % Factory flat command
    end
    
    properties (Constant)
        d_act = 16 % Number of active elements across the aperture diameter
            % >>> Put the correct value
        n_act = 290 % Total number of active elements % >>> Put the correct
            % value
        n_act_w = 800 % Number of active elements across the width % >>> Put the
            % correct value
        n_act_h = 600 % Number of active elements across the height % >>> Put
            % the correct value
        tel_res = 192 % Number of pixels across the aperture diameter in OOMAO
            % >>> Put the correct value. If this value is the same for SLM and
            % DM, move it to the 'mirror' class
        type = 'dm' % Type of active mirror (DM or SLM)
    end
    
    properties (Constant, Hidden)
        type_str = 'DM' % Text string of the type of active mirror
    end
    
    methods (Access=protected)
        
        function connect_control(obj)
            %{
            Connect the hardware.
            %}
            
            % >>> Write code to connect
        end
        
        function disconnect_control(obj)
            %{
            Disconnect the hardware.
            %}
            
            % >>> Write code to disconnect
        end
    end
end

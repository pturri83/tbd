classdef static
    %{
    Class for static methods.
    %}
    
    methods (Static)
        
        function print_com(verbose, text, varargin)
            %{
            Print a message in the command window.
            
            Parameters:
            verbose (logic): show message in the command window
            %}
            
            % Check input values
            if ~ischar(text)
                error('Error: ''text'' input value has to be string.')
            end
            
            % Print message
            if verbose
                fprintf(append('********** ', text, '\n'), varargin{:});
            end
        end
    end
end

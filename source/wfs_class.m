classdef wfs_class < handle
    % Class for WFS.
    
    properties (SetAccess=private)
        bias % Bias master frame
        connected % Connected (true or false)
        dark % Dark master frame, bias-corrected
        dark_exposure % Exposure time of the dark master frame (s)
        exposure % Exposure time [s]
        folder % Absolute path of the folder of the source code
        frame % Captured frame, calibrated for bias and dark
        master_exposure = 1 % Exposure time of a frame to generate a dark or
            % flat field master frame
        master_n = 1000 % Number of frames to generate a master frame
        n_px % Total number of pixels
        oomao % Associated OOMAO object
        shutter = 'close' % Shutter status ('open' or 'close')
        temp = -15 % Target detector temperature [C]
        temp_max_wait = 30 % Maximum number of iterations waiting to reach the
            % target detector temperature
        temp_t = 5 % Time to wait on each iteration to reach the target detector
            % temperature [s]
        temp_tol = 1 % Target detector temperature tolerance [C]
        verbose % Show messages in the command window (true or false)
        wait % Wait for the hardware to settle (WFS camera temperature) (true or
            % false)
    end
    
    properties (Constant)
        d_lens = 16 % Number of lenslets across the aperture diameter
        fill_lens = 0.75 % Fill factor for an active lenslet
        px_lens = 8 % Width of a lenslet on the detector [px]
        x_px = 128 % Width of the detector [px]
        y_px = 128 % Height of the detector [px]
    end
    
    methods
        
        function obj = wfs_class(verbose, wait)
            %{
            Creator. Hardware is connected and an OOMAO object is associated.
            
            Parameters:
            verbose (logic): show messages in the command window
            wait (logic): wait to reach the target temperature
            %}
            
            % Check input values
            if ~islogical(verbose)
                error('Error: ''verbose'' input value has to be logical.')
            end
            
            if ~islogical(wait)
                error('Error: ''wait'' input value has to be logical.')
            end
            
            % Find source code folder
            folder = mfilename('fullpath');
            obj.folder = folder(1: (end - 10));
            
            % Connect the hardware
            obj.verbose = verbose;
            obj.wait = wait;
            obj.n_px = obj.x_px * obj.y_px;
            obj.connect()
            
            % Create the OOMAO object
            obj.oomao = shackHartmann(obj.d_lens, ...
                (obj.d_lens * obj.px_lens), obj.fill_lens);
            static.print_com(obj.verbose, 'WFS created')
        end
        
        function delete(obj)
            %{
            Destructor. Hardware is disconnected and the associated OOMAO object
            is deleted.
            %}
            
            % Delete
            obj.disconnect()
            obj.oomao.delete()
            static.print_com(obj.verbose, 'WFS deleted')
        end
        
        function connect(obj)
            %{
            Connect the WFS detector.
            %}
            
            % Connect the detector and turn on the cooler
            static.print_com(obj.verbose, 'WFS detector connecting...')
            ret = AndorInitialize('');
            CheckError(ret);
            ret = CoolerON();
            CheckWarning(ret);
            obj.connected = true;
            
            % Set the detector temperature, exposure timeread mode, trigger,
            % shutter and region of interest
            obj.set_temp()
            obj.set_exposure(1)
            ret = SetAcquisitionMode(1);
            CheckWarning(ret);
            ret = SetReadMode(4);
            CheckWarning(ret);
            ret = SetTriggerMode(0);
            CheckWarning(ret);
            obj.set_shutter('close')
            ret = SetImage(1, 1, 1, obj.x_px, 1, obj.y_px); 
            CheckWarning(ret);
            static.print_com(obj.verbose, 'WFS detector connected')
        end
        
        function disconnect(obj)
            %{
            Disconnect the WFS detector.
            %}
            
            % Disconnect the detector
            static.print_com(obj.verbose, 'WFS detector disconnecting...')
            AbortAcquisition;
            ret = CoolerOFF();
            CheckWarning(ret);
            obj.set_shutter('close')
            ret = AndorShutDown;
            CheckWarning(ret);
            obj.connected = false;
            static.print_com(obj.verbose, 'WFS detector disconnected')
        end
        
        function temp_out = get_temp(obj)
            %{
            Measure the temperature of the WFS detector.
            
            Output:
            temp_out (float): measured temperature [C]
            %}
            
            % Measure temperature
            [ret, temp_out, ~, ~, ~] = GetTemperatureStatus();
            CheckWarning(ret);
            static.print_com(obj.verbose, ...
                'WFS detector temperature is %.1f C', temp_out)
        end
        
        function set_temp(obj, varargin)
            %{
            Set the temperature of the WFS detector.
            
            Parameters:
            temp (float, default=obj.temp): target temperature [C]
            %}
            
            % Parse inputs
            p = inputParser;
            p.addParameter('temp', obj.temp, @isfloat);
            p.parse(varargin{:});
            
            % Set the temperature
            obj.temp = p.Results.temp;
            ret = SetTemperature(obj.temp);
            CheckWarning(ret);
            static.print_com(obj.verbose, ...
                'WFS detector temperature set at %.1f C', obj.temp)
            
            % Wait to reach temperature
            if obj.wait
                obj.wait_temp()
            end
        end
        
        function wait_temp(obj)
            %{
            Wait for the WFS detector to reach the target temperature.
            %}
            
            % Wait for temperature
            static.print_com(obj.verbose, ...
                'Waiting for WFS detector temperature to reach %.1f C...', ...
                obj.temp)
            temp_now = obj.get_temp;
            i_temp = 0;
            temp_fail = false;
            
            while temp_now > (obj.temp + obj.temp_tol)
                pause(obj.temp_t);
                temp_now = obj.get_temp;
                i_temp = i_temp + 1;
                
                if i_temp == obj.temp_max_wait
                    temp_fail = true;
                    break
                end
            end
            
            if temp_fail
                temp_fail_str = 'not ';
            else
                temp_fail_str = '';
            end
            
            static.print_com(obj.verbose, append('WFS detector has ', ...
                temp_fail_str, 'reached the temperature'))
        end
        
        function set_shutter(obj, oc, varargin)
            %{
            Set the WFS detector shutter open or closed.
            
            Parameters:
            oc (string): shutter status ('open' or 'close')
            silent (logic): silence messages in the command window, overriding
                the default option
            %}
            
            % Parse inputs
            p = inputParser;
            p.addParameter('silent', false, @islogical);
            p.parse(varargin{:});
            
            % Check input values
            switch oc
                case 'open'
                    open_flag = 1;
                case 'close'
                    open_flag = 2;
                otherwise
                    error(append('Error: ''oc'' input value has to be', ...
                        ' ''open'' or ''close''.'))
            end
            
            % Set the shutter
            ret = SetShutter(1, open_flag, 1, 1);
            CheckWarning(ret);
            obj.shutter = oc;
            
            if ~p.Results.silent
                static.print_com(obj.verbose, ...
                    'WFS detector shutter status is %s', obj.shutter)
            end
        end
        
        function set_exposure(obj, time, varargin)
            %{
            Set the WFS detector exposure time.
            
            Parameters:
            time (float): exposure time [s]
            silent (logic): silence messages in the command window, overriding
                the default option
            %}
            
            % Parse inputs
            p = inputParser;
            p.addParameter('silent', false, @islogical);
            p.parse(varargin{:});
            
            % Check input values
            if ~isfloat(time)
                error(append('Error: ''time'' input value has to be float', ...
                    ' or integer.'))
            end
            
            % Set the exposure time
            ret = SetExposureTime(time);
            CheckWarning(ret);
            obj.exposure = time;
            
            if ~p.Results.silent
                static.print_com(obj.verbose, ...
                    'WFS detector exposure time is %f s', obj.exposure)
            end
        end
        
        function acquire(obj, varargin)
            %{
            Take an image with the WFS detector.
            
            Parameters:
            bias (logic): calibrate the image using the master bias
            dark (logic): calibrate the image using the master dark. The 'bias'
                parameter has to be true
            closed (logic): keep the shutter closed
            silent (logic): silence messages in the command window, overriding
                the default option
            %}
            
            % Parse inputs
            p = inputParser;
            p.addParameter('bias', true, @islogical);
            p.addParameter('dark', true, @islogical);
            p.addParameter('closed', false, @islogical);
            p.addParameter('silent', false, @islogical);
            p.parse(varargin{:});
            
            % Take image
            if ~p.Results.silent
                static.print_com(obj.verbose, ...
                    'Starting WFS detector integration of %f s...', ...
                    obj.exposure)
            end
            
            if ~p.Results.closed
                obj.set_shutter('open', 'silent', p.Results.silent)
            end
            
            ret = StartAcquisition();
            CheckWarning(ret);
            WaitForAcquisitionTimeOut(obj.exposure + 10000);
            [ret, image_lin] = GetOldestImage(obj.n_px);
            CheckWarning(ret);
            
            % Check if the image was taken
            while ret == atmcd.DRV_ACQUIRING
                static.print_com(obj.verbose, ...
                    'Waiting for the WFS detector acquisition...')
                WaitForAcquisitionTimeOut(obj.exposure + 10000);
                [ret, image_lin] = GetOldestImage(obj.n_px);
                CheckWarning(ret);
            end
            
            % Complete acquisition
            ret = AbortAcquisition;
            CheckWarning(ret);
            
            if ~p.Results.closed
                obj.set_shutter('close', 'silent', p.Results.silent)
            end
            
            % Calibrate the image
            image = double(reshape(image_lin, obj.y_px, obj.x_px));
            
            if p.Results.bias
                image = image - obj.bias;
                
                if p.Results.dark
                    image = image - (obj.dark * obj.exposure / ...
                        obj.dark_exposure);
                end
            end
            
            % Store the frame
            obj.frame = image;
            
            if ~p.Results.silent
                static.print_com(obj.verbose, ...
                    'WFS detector integration completed')
            end
        end
        
        function image = master(obj, varargin)
            %{
            Take a master frame, a frame obtained from the median of a number of
            images of the same subject taken in the same condition.
            
            Parameters:
            bias (logic): calibrate the image using the master bias
            dark (logic): calibrate the image using the master dark. The 'bias'
                parameter has to be true
            closed (logic): keep the shutter closed
            
            Output:
            image (array[float]): detector image
            %}
            
            % Parse inputs
            p = inputParser;
            p.addParameter('bias', true, @islogical);
            p.addParameter('dark', true, @islogical);
            p.addParameter('closed', false, @islogical);
            p.parse(varargin{:});
            
            % Take frames
            frames = zeros(obj.y_px, obj.x_px, obj.master_n);
            
            for i_frame = 1: obj.master_n
                static.print_com(obj.verbose, 'Frame %i / %i', i_frame, ...
                    obj.master_n)
                obj.acquire('silent', true, 'bias', p.Results.bias, 'dark', ...
                    p.Results.dark, 'closed', p.Results.closed);
                frames(:, :, i_frame) = obj.frame;
            end
            
            % Calculate master frame
            image = median(frames, 3);
        end
        
        function get_bias(obj)
            %{
            Take and store a master bias frame. 1000 exposures with closed
            shutter of 0 s each are used.
            %}
            
            % Set exposure time
            static.print_com(obj.verbose, ...
                'Taking WFS detector master bias frame...')
            exposure_tmp = obj.exposure;
            obj.set_exposure(0, 'silent', true)
            
            % Take master bias frame
            obj.bias = obj.master('bias', false, 'dark', false, 'closed', true);
            
            % Set exposure time
            obj.set_exposure(exposure_tmp, 'silent', true);
            static.print_com(obj.verbose, ...
                'WFS detector master bias frame taken')
        end
        
        function get_dark(obj)
            %{
            Take and store a master dark frame, bias-corrected. 1000 exposures
            with closed shutter of 1 s each are used.
            %}
            
            % Set exposure time
            static.print_com(obj.verbose, ...
                'Taking WFS detector master dark frame...')
            exposure_tmp = obj.exposure;
            obj.set_exposure(obj.master_exposure, 'silent', true)
            obj.dark_exposure = obj.master_exposure;
            
            % Take master dark frame
            obj.dark = obj.master('bias', true, 'dark', false, 'closed', true);
            
            % Set exposure time
            obj.set_exposure(exposure_tmp, 'silent', true);
            static.print_com(obj.verbose, ...
                'WFS detector master dark frame taken')
        end
        
        function calibrate(obj)
            %{
            Calibrate the WFS detector by taking and storing the bias and dark
            master frames. The calibration is then automatically saved in files,
            for future uses.
            %}
            
            % Calibrate the detector
            obj.get_bias()
            obj.get_dark()
            % >>> Add flat fielding
            
            % Save the calibration
            bias_frame = obj.bias;
            save(append(fileparts(obj.folder), '\data\wfs_bias.mat'), ...
                'bias_frame')
            dark_frame = obj.dark;
            dark_frame_exposure = obj.dark_exposure;
            save(append(fileparts(obj.folder), '\data\wfs_dark.mat'), ...
                'dark_frame', 'dark_frame_exposure')
            % >>> Add flat fielding
        end
        
        function calibrate_load(obj)
            %{
            Load the WFS detector bias and dark master frames.
            %}
            
            % Load the calibration
            bias_struct = load(append(fileparts(obj.folder), ...
                '\data\wfs_bias.mat'));
            obj.bias = bias_struct.bias_frame;
            dark_struct = load(append(fileparts(obj.folder), ...
                '\data\wfs_dark.mat'));
            obj.dark = dark_struct.dark_frame;
            obj.dark_exposure = dark_struct.dark_frame_exposure;
            % >>> Add flat fielding
        end
    end
end

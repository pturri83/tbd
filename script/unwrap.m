%{
Script to unwrap a factory flat command.
%}

% Clear the session
clear
clc
close all
drawnow

% Parameters
load_orig = true; % Start from the original BMP file instead from an existing
    % MAT file
wvl = 800; % Wavelength (nm)
box = [200, 650, 50, 500]; % Box to unwrap [left, right, top, bottom]
level = 100; % Cutoff value
hilo = 'lo'; % Change the values above ('hi') or below ('lo') the cutoff level
offset = false; % Offset the map to have a low value of 0
save_map = false; % Save the output

% Load and convert the data
if load_orig == true
    map_orig = imread(append('..\drivers\Hamamatsu\', ...
        'deformation_correction_pattern\CAL_LSH0802405_', num2str(wvl), ...
        'nm.bmp'));
    map = double(map_orig);
elseif load_orig == false
    map_struct = load(append('../data/flat_factory_', num2str(wvl), '.mat'));
    map = map_struct.map;
end

figure('name', 'Wrapped map', 'units', 'normalized', 'position', ...
    [0.02, 0.72, 0.18, 0.2])
imagesc(map)
daspect([1, 1, 1])
colorbar
hold on
line([box(1), box(1)], [box(3), box(4)], 'color', 'k', 'linestyle', '--', ...
    'linewidth', 2)
line([box(1), box(2)], [box(4), box(4)], 'color', 'k', 'linestyle', '--', ...
    'linewidth', 2)
line([box(2), box(2)], [box(3), box(4)], 'color', 'k', 'linestyle', '--', ...
    'linewidth', 2)
line([box(1), box(2)], [box(3), box(3)], 'color', 'k', 'linestyle', '--', ...
    'linewidth', 2)
hold off

% Cut the map
map_box = map(box(3): box(4), box(1) : box(2));
figure('name', 'Wrapped box', 'units', 'normalized', 'position', ...
    [0.22, 0.72, 0.18, 0.2])
imagesc(map_box)
daspect([1, 1, 1])
colorbar

% Select values
if strcmp(hilo, 'hi')
    map_where = (map_box > level);
elseif strcmp(hilo, 'lo')
    map_where = (map_box < level);
end

figure('name', 'Box level', 'units', 'normalized', 'position', ...
    [0.42, 0.72, 0.18, 0.2])
imagesc(map_where)
daspect([1, 1, 1])
colorbar

% Substitute values
if strcmp(hilo, 'hi')
    map_box(map_where) = map_box(map_where) - 255;
elseif strcmp(hilo, 'lo')
    map_box(map_where) = map_box(map_where) + 255;
end

figure('name', 'Unwrapped box', 'units', 'normalized', 'position', ...
    [0.62, 0.72, 0.18, 0.2])
imagesc(map_box)
daspect([1, 1, 1])
colorbar

% Recombine maps
map(box(3): box(4), box(1) : box(2)) = map_box;

% Offset map
if offset == true
    map = map - min(map(:));
end

figure('name', 'Unwrapped and offset map', 'units', 'normalized', ...
    'position', [0.82, 0.72, 0.18, 0.2])
imagesc(map)
daspect([1, 1, 1])
colorbar

% Save the output
if save_map == true
    save(append('..\data\flat_factory_', num2str(wvl), '.mat'), 'map')
end






%{
Script to test the bench hardware.
%}

% Clear the session
clear
clc
close all
drawnow

% Parameters
calibrate = false; % Calibrate the WFS detetctor instead of loading it

% Test the bench
bnc = bench(); % Start the bench

if calibrate == true
    bnc.wfs.calibrate() % Calibrate the WFS detector and save it
else
    bnc.wfs.calibrate_load() % Load a previous WFS detector calibration
end

bnc.wfs.set_exposure(0.01) % Change the WFS camera exposure time
bnc.wfs.acquire(); % Take an image with the WFS camera
figure('name', 'WFS camera')
imagesc(bnc.wfs.frame)
xlabel('x [px]')
ylabel('y [px]')
drawnow
test_comm = rand(bnc.slm.n_act_h, bnc.slm.n_act_w) * 10;
test_comm([1, bnc.slm.n_act_h], :) = 255;
test_comm(:, [1, bnc.slm.n_act_w]) = 255;
bnc.slm.push(test_comm(:)) % Apply an SLM command
bnc.slm.load_movie('..\data\test\movie.mat') % Load the SLM movie
bnc.slm.set_movie_step(-4.3) % Change the step size of the SLM movie
bnc.slm.set_movie_frame(1000) % Change the starting frame of the SLM movie

for i_frame = 1: abs(bnc.slm.movie_frames / bnc.slm.movie_step)
    bnc.slm.step_movie() % Step through the SLM movie
end

bnc.delete % Delete the bench

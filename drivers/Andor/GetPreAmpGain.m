function [ret, gain] = GetPreAmpGain(index)
% GetPreAmpGain For those systems that provide a number of pre amp gains to apply to the data as it is read out; this function retrieves the amount of gain that is stored for a particular index. The number of gains available can be obtained by calling the GetNumberPreAmpGainsGetNumberPreAmpGains function and a specific Gain can be selected using the function SetPreAmpGainSetPreAmpGain.
%
% SYNOPSIS : [ret, gain] = GetPreAmpGain(index)
%
% INPUT index: gain index
%         0 - to GetNumberPreAmpGainsGetNumberPreAmpGains()-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gain returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid index.
%        gain: gain factor for this index.
% REMARKS : C++ Equiv : unsigned int GetPreAmpGain(int index, float * gain);
%
% SEE ALSO : IsPreAmpGainAvailable GetNumberPreAmpGains SetPreAmpGain GetCapabilities 
[ret, gain] = atmcdmex('GetPreAmpGain', index);

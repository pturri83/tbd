function [ret] = SetReadoutRegisterPacking(mode)
% SetReadoutRegisterPacking This function will configure whether data is packed into the readout register to improve frame rates for sub-images.
% Note: It is important to ensure that no light falls outside of the sub-image area otherwise the acquired data will be corrupted.  Only currently available on iXon+ and iXon3.
%
% SYNOPSIS : [ret] = SetReadoutRegisterPacking(mode)
%
% INPUT mode: register readout mode
%         0 - Packing Off
%         1 - Packing On
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Readout mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid readout mode passed.
% REMARKS : C++ Equiv : unsigned int SetReadoutRegisterPacking(int mode);
%
% SEE ALSO : GetAcquisitionTimings SetAccumulationCycleTime SetAcquisitionMode SetExposureTime SetKineticCycleTime SetNumberAccumulations SetNumberKinetics 
[ret] = atmcdmex('SetReadoutRegisterPacking', mode);

function [ret, pInputImage, pOutputImage] = PostProcessNoiseFilter(iOutputBufferSize, iBaseline, iMode, fThreshold, iHeight, iWidth)
% PostProcessNoiseFilter This function will apply a filter to the input image and return the processed image in the output buffer.  The filter applied is chosen by the user by setting Mode to a permitted value.
%
% SYNOPSIS : [ret, pInputImage, pOutputImage] = PostProcessNoiseFilter(iOutputBufferSize, iBaseline, iMode, fThreshold, iHeight, iWidth)
%
% INPUT iOutputBufferSize: The baseline associated with the image.
%       iBaseline: The mode to use to process the data.
%         1 - Use Median Filter.
%         2 - Use Level Above Filter.
%         3 - Use interquartile Range Filter.
%         4 - Use Noise Threshold Filter.
%       iMode: This is the Threshold multiplier for the Median, interquartile
%         and - Noise Threshold filters.  For the Level Above filter this is
%         Threshold - count above the baseline.
%       fThreshold: The height of the image.
%       iHeight: The width of the image.
%       iWidth: 
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition prepared.
%          DRV_NOT_SUPPORTED DRV_NOT_INITIALIZED - Camera does not support Noise filter processing. System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Invalid pointer (i.e. NULL).
%          DRV_P4INVALID - Baseline less than zero.
%          DRV_P5INVALID - Invalid Filter mode.
%          DRV_P6INVALID - Threshold value not valid for selected mode.
%          DRV_P7INVALID - Height less than zero.
%          DRV_P8INVALID DRV_ERROR_BUFFSIZE - Width less than zero.
%        pInputImage: The input image data to be processed.
%          at32 - * pOutputImage:	The output buffer to return the processed image.
%        pOutputImage: The size of the output buffer.
% REMARKS : C++ Equiv : unsigned int PostProcessNoiseFilter(at_32 * pInputImage, at_32 * pOutputImage, int iOutputBufferSize, int iBaseline, int iMode, float fThreshold, int iHeight, int iWidth);
%
% SEE ALSO : 
[ret, pInputImage, pOutputImage] = atmcdmex('PostProcessNoiseFilter', iOutputBufferSize, iBaseline, iMode, fThreshold, iHeight, iWidth);

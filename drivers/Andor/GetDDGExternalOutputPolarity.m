function [ret, puiPolarity] = GetDDGExternalOutputPolarity(uiIndex)
% GetDDGExternalOutputPolarity This function gets the current polarity of a selected external output.
%
% SYNOPSIS : [ret, puiPolarity] = GetDDGExternalOutputPolarity(uiIndex)
%
% INPUT uiIndex: index of external output.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Polarity returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - Invalid external output index.
%        puiPolarity: current polarity of external output (0 - Positive, 1 - Negative).
% REMARKS : C++ Equiv : unsigned int GetDDGExternalOutputPolarity(at_u32 uiIndex, at_u32 * puiPolarity);
%
% SEE ALSO : GetCapabilities GetDDGExternalOutputEnabled SetDDGExternalOutputPolarity SetDDGGateStep 
[ret, puiPolarity] = atmcdmex('GetDDGExternalOutputPolarity', uiIndex);

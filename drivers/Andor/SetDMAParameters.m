function [ret] = SetDMAParameters(MaxImagesPerDMA, SecondsPerDMA)
% SetDMAParameters In order to facilitate high image readout rates the controller card may wait for multiple images to be acquired before notifying the SDK that new data is available. Without this facility, there is a chance that hardware interrupts may be lost as the operating system does not have enough time to respond to each interrupt. The drawback to this is that you will not get the data for an image until all images for that interrupt have been acquired.
% There are 3 settings involved in determining how many images will be acquired for each notification (DMA interrupt) of the controller card and they are as follows:
% 1. The size of the DMA buffer gives an upper limit on the number of images that can be stored within it and is usually set to the size of one full image when installing the software. This will usually mean that if you acquire full frames there will never be more than one image per DMA.
% 2. A second setting that is used is the minimum amount of time (SecondsPerDMA) that should expire between interrupts. This can be used to give an indication of the reponsiveness of the operating system to interrupts. Decreasing this value will allow more interrupts per second and should only be done for faster pcs. The default value is 0.03s (30ms), finding the optimal value for your pc can only be done through experimentation.
% 3. The third setting is an overide to the number of images calculated using the previous settings. If the number of images per dma is calculated to be greater than MaxImagesPerDMA then it will be reduced to MaxImagesPerDMA. This can be used to, for example, ensure that there is never more than 1 image per DMA by setting MaxImagesPerDMA to 1. Setting MaxImagesPerDMA to zero removes this limit. Care should be taken when modifying these parameters as missed interrupts may prevent the acquisition from completing.
%
% SYNOPSIS : [ret] = SetDMAParameters(MaxImagesPerDMA, SecondsPerDMA)
%
% INPUT MaxImagesPerDMA: Override to the number of images per DMA if the calculated value is higher than this. (Default=0, ie. no override)
%       SecondsPerDMA: Minimum amount of time to elapse between interrrupts. (Default=0.03s)
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - DMA Parameters setup successfully.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - MaxImagesPerDMA invalid
%          DRV_P2INVALID - SecondsPerDMA invalid
% REMARKS : C++ Equiv : unsigned int SetDMAParameters(int MaxImagesPerDMA, float SecondsPerDMA);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDMAParameters', MaxImagesPerDMA, SecondsPerDMA);

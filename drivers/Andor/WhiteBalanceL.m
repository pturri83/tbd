function [ret, wRed, wGreen, wBlue, fRelR, fRelB, info_iSize, info_iX, info_iY, info_iAlgorithm, info_iROI_left, info_iROI_right, info_iROI_top, info_iROI_bottom, info_iOperation] = WhiteBalance()
%  Legacy Wrapper for WhiteBalance For colour sensors only
% Calculates the red and blue relative to green factors to white balance a colour image using the parameters stored in info.
% Before passing the address of an WhiteBalanceInfo structure to the function the iSize member of the structure should be set to the size of the structure. In C++ this can be done with the line:
% nfo-> iSize = sizeof(WhiteBalanceInfo);
% Below is the WhiteBalanceInfo structure definition and a description of its members:
% struct WHITEBALANCEINFO {
% int iSize;  // Structure size.
% int iX;      // Number of X pixels. Must be >2.
% int iY;     // Number of Y pixels. Must be >2.
% int iAlgorithm; // Algorithm to used to calculate white balance.
% int iROI_left; // Region Of interest from which white balance is calculated
% int iROI_right; // Region Of interest from which white balance is calculated
% int iROI_top; // Region Of interest from which white balance is calculated
% int iROI_bottom; // Region Of interest from which white balance is calculated
% WhiteBalanceInfo;
% iX and iY are the image dimensions. The number of elements of the input, red, green and blue arrays are iX x iY.
% iAlgorithm sets the algorithm to use. The function sums all the colour values per each colour field within the Region Of interest (ROI) and calculates the relative to green values as: 0) _fRelR = GreenSum / RedSum and _fRelB = GreenSum / BlueSum; 1) _fRelR = 2/3 GreenSum / RedSum and _fRelB = 2/3 GreenSum / BlueSum, giving more importance to the green field.
% iROI_left, iROI_right, iROI_top and iROI_bottom define the ROI with the constraints:
% iROI_left0 <= iROI_left < iROI_right <= iX and 0 <= iROI_ bottom < iROI_ top <= iX
%
% SYNOPSIS : [ret, wRed, wGreen, wBlue, fRelR, fRelB, info_iSize, info_iX, info_iY, info_iAlgorithm, info_iROI_left, info_iROI_right, info_iROI_top, info_iROI_bottom, info_iOperation] = WhiteBalance()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          SUCCESS - White balance calculated.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Invalid pointer (i.e. NULL).
%          DRV_P3INVALID - Invalid pointer (i.e. NULL).
%          DRV_P4INVALID - Invalid pointer (i.e. NULL).
%          DRV_P5INVALID - Invalid pointer (i.e. NULL).
%          DRV_P6INVALID - One or more parameters in info is out of range
%          DRV_DIVIDE_BY_ZERO_ERROR - The sum of the green field within the ROI is zero. _fRelR and _fRelB are set to 1
%        wRed: red field.
%        wGreen: green field.
%        wBlue: blue field.
%        fRelR: the relative to green red factor.
%        fRelB: the relative to green blue factor.
%        info: white balance information structure
% REMARKS : C++ Equiv : unsigned int WhiteBalance(WORD * wRed, WORD * wGreen, WORD * wBlue, float * fRelR, float * fRelB, WhiteBalanceInfo * info);
%
% SEE ALSO : DemosaicImage GetMostRecentColorImage16 
[ret, wRed, wGreen, wBlue, fRelR, fRelB, info] = atmcdmex('WhiteBalance');
info_iSize = info.iSize;
info_iX = info.iX;
info_iY = info.iY;
info_iAlgorithm = info.iAlgorithm;
info_iROI_left = info.iROI_left;
info_iROI_right = info.iROI_right;
info_iROI_top = info.iROI_top;
info_iROI_bottom = info.iROI_bottom;
info_iOperation = info.iOperation;

function [ret] = SetDDGAddress(t0, t1, t2, t3, address)
% SetDDGAddress THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetDDGAddress(t0, t1, t2, t3, address)
%
% INPUT t0: 
%       t1: 
%       t2: 
%       t3: 
%       address: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDDGAddress(BYTE t0, BYTE t1, BYTE t2, BYTE t3, BYTE address);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGAddress', t0, t1, t2, t3, address);

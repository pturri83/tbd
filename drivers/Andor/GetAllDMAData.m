function [ret, arr] = GetAllDMAData(size)
% GetAllDMAData THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, arr] = GetAllDMAData(size)
%
% INPUT size: 
% OUTPUT ret: Return Code: 
%        arr: 
% REMARKS : C++ Equiv : unsigned int GetAllDMAData(at_32 * arr, long size);
%
% SEE ALSO : 
[ret, arr] = atmcdmex('GetAllDMAData', size);

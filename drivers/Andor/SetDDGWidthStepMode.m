function [ret] = SetDDGWidthStepMode(mode)
% SetDDGWidthStepMode This function will set the current gate width step mode.
%
% SYNOPSIS : [ret] = SetDDGWidthStepMode(mode)
%
% INPUT mode: the gate step mode.
%         0 - constant.
%         1 - exponential.
%         2 - logarithmic.
%         3 - linear.
%         100 - off.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gate step mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Gate step not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid gate step mode.
% REMARKS : C++ Equiv : unsigned int SetDDGWidthStepMode(at_u32 mode);
%
% SEE ALSO : SetDDGWidthStepCoefficients GetDDGWidthStepMode GetDDGWidthStepCoefficients 
[ret] = atmcdmex('SetDDGWidthStepMode', mode);

function [ret] = Filter_SetAveragingFrameCount(frames)
% Filter_SetAveragingFrameCount Sets the averaging frame count.
%
% SYNOPSIS : [ret] = Filter_SetAveragingFrameCount(frames)
%
% INPUT frames: The averaging frame count to use.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Averaging frame count set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid frame count.
% REMARKS : C++ Equiv : unsigned int Filter_SetAveragingFrameCount(int frames);
%
% SEE ALSO : Filter_GetAveragingFrameCount 
[ret] = atmcdmex('Filter_SetAveragingFrameCount', frames);

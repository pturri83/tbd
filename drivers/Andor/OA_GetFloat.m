function [ret, fFloatValue] = OA_GetFloat(pcModeName, pcModeParam)
% OA_GetFloat This function is used to get the values for floating point type acquisition parameters.
% Values are retrieved from memory for the specified mode name.
%
% SYNOPSIS : [ret, fFloatValue] = OA_GetFloat(pcModeName, pcModeParam)
%
% INPUT pcModeName: The name of the mode for which an acquisition parameter will be retrieved.
%       pcModeParam: The name of the acquisition parameter for which a value will be retrieved.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted
%          DRV_P1INVALID - Null mode parameter.
%          DRV_P2INVALID - Null mode parameter.
%          DRV_P3INVALID - Null float value.
%        fFloatValue: The value of the acquisition parameter.
% REMARKS : C++ Equiv : unsigned int OA_GetFloat(const char * pcModeName, const char * pcModeParam, float * fFloatValue);
%
% SEE ALSO : OA_SetFloat 
[ret, fFloatValue] = atmcdmex('OA_GetFloat', pcModeName, pcModeParam);

function [ret] = SetPhotonCountingDivisions(noOfDivisions, divisions)
% SetPhotonCountingDivisions This function sets the thresholds for the photon counting option.
%
% SYNOPSIS : [ret] = SetPhotonCountingDivisions(noOfDivisions, divisions)
%
% INPUT noOfDivisions: number of thresholds to be used.
%       divisions: threshold levels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Thresholds accepted.
%          DRV_P1INVALID - Number of thresholds outside valid range
%          DRV_P2INVALID - Thresholds outside valid range
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_NOT_SUPPORTED - Feature not supported.
% REMARKS : C++ Equiv : unsigned int SetPhotonCountingDivisions(at_u32 noOfDivisions, at_32 * divisions);
%
% SEE ALSO : SetPhotonCounting GetNumberPhotonCountingDivisions 
[ret] = atmcdmex('SetPhotonCountingDivisions', noOfDivisions, divisions);

function [ret, pInputImage, pOutputImage] = PostProcessCountConvert(iOutputBufferSize, iNumImages, iBaseline, iMode, iEmGain, fQE, fSensitivity, iHeight, iWidth)
% PostProcessCountConvert This function will convert the input image data to either Photons or Electrons based on the mode selected by the user.  The input data should be in counts.
%
% SYNOPSIS : [ret, pInputImage, pOutputImage] = PostProcessCountConvert(iOutputBufferSize, iNumImages, iBaseline, iMode, iEmGain, fQE, fSensitivity, iHeight, iWidth)
%
% INPUT iOutputBufferSize: The size of the output buffer.
%         data - data
%       iNumImages: The number of images if a kinetic series is supplied as the input
%       iBaseline: The baseline associated with the image.
%         1 - - Convert to Electrons
%         2 - - Convert to Photons
%       iMode: The mode to use to process the data.
%       iEmGain: The gain level of the input image.
%       fQE: The Quantum Efficiency of the sensor.
%       fSensitivity: The Sensitivity value used to acquire the image.
%       iHeight: The height of the image.
%       iWidth: The width of the image.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition prepared.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Invalid pointer (i.e. NULL).
%          DRV_P4INVALID - Number of images less than zero.
%          DRV_P5INVALID - Baseline less than zero.
%          DRV_P6INVALID - Invalid count convert mode.
%          DRV_P7INVALID - EMGain less than zero.
%          DRV_P8INVALID DRV_P9INVALID - QE less than zero.
%          DRV_P10INVALID - Sensitivity less than zero.
%          DRV_P11INVALID - Height less than zero.
%          DRV_ERROR_BUFFSIZE - Width less than zero.
%        pInputImage: The input image data to be processed.
%          at32 - * pOutputImage:	The output buffer to return the processed image.
%        pOutputImage: The output buffer to return the processed image.
% REMARKS : C++ Equiv : unsigned int PostProcessCountConvert(at_32 * pInputImage, at_32 * pOutputImage, int iOutputBufferSize, int iNumImages, int iBaseline, int iMode, int iEmGain, float fQE, float fSensitivity, int iHeight, int iWidth);
%
% SEE ALSO : 
[ret, pInputImage, pOutputImage] = atmcdmex('PostProcessCountConvert', iOutputBufferSize, iNumImages, iBaseline, iMode, iEmGain, fQE, fSensitivity, iHeight, iWidth);

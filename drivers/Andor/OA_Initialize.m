function [ret] = OA_Initialize(pcFilename, uiFileNameLen)
% OA_Initialize This function will initialise the OptAcquire settings from a Preset file and a User defined file if it exists.
%
% SYNOPSIS : [ret] = OA_Initialize(pcFilename, uiFileNameLen)
%
% INPUT pcFilename: The name of a user xml file.  If the file exists then data will be read from the file.  If the file does not exist the file name may be used when the user calls WriteToFile().
%       uiFileNameLen: The length of the filename.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null filename.
%          DRV_OA_CAMERA_NOT_SUPPORTED - Camera not supported.
%          DRV_OA_GET_CAMERA_ERROR - Unable to retrieve information about the
%          DRV_OA_INVALID_STRING_LENGTH - model of the Camera.
%          DRV_OA_ANDOR_FILE_NOT_LOADED - The parameter has an invalid length, i.e. > 255.
%          DRV_OA_USER_FILE_NOT_LOADED - Preset Andor file failed to load.
%          DRV_OA_FILE_ACCESS_ERROR - Supplied User file failed to load.
%          DRV_OA_PRESET_AND_USER_FILE_NOT_LOADED - Failed to determine status of file.
% REMARKS : C++ Equiv : unsigned int OA_Initialize(const char * pcFilename, int uiFileNameLen);
%
% SEE ALSO : OA_WriteToFile 
[ret] = atmcdmex('OA_Initialize', pcFilename, uiFileNameLen);

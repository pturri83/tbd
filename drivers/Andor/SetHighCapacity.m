function [ret] = SetHighCapacity(state)
% SetHighCapacity This function switches between high sensitivity and high capacity functionality. With high capacity enabled the output amplifier is switched to a mode of operation which reduces the responsivity thus allowing the reading of larger charge packets during binning operations.
%
% SYNOPSIS : [ret] = SetHighCapacity(state)
%
% INPUT state: Enables/Disables High Capacity functionality
%         1 - Enable High Capacity (Disable High Sensitivity)
%         0 - Disable High Capacity (Enable High Sensitivity)
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - State parameter was not zero or one.
% REMARKS : C++ Equiv : unsigned int SetHighCapacity(int state);
%
% SEE ALSO : GetCapabilities 
[ret] = atmcdmex('SetHighCapacity', state);

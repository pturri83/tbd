function [ret, arr, validfirst, validlast] = GetImages(first, last, size)
% GetImages This function will update the data array with the specified series of images from the circular buffer. If the specified series is out of range (i.e. the images have been overwritten or have not yet been acquired then an error will be returned.
%
% SYNOPSIS : [ret, arr, validfirst, validlast] = GetImages(first, last, size)
%
% INPUT first: index of first image in buffer to retrieve.
%       last: index of last image in buffer to retrieve.
%       size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Images have been copied into array.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_GENERAL_ERRORS - The series is out of range.
%          DRV_P3INVALID - Invalid pointer (i.e. NULL).
%          DRV_P4INVALID - Array size is incorrect.
%          DRV_NO_NEW_DATA - There is no new data yet.
%        arr: data storage allocated by the user.
%        validfirst: index of the first valid image.
%        validlast: index of the last valid image.
% REMARKS : C++ Equiv : unsigned int GetImages(long first, long last, at_32 * arr, long size, long * validfirst, long * validlast);
%
% SEE ALSO : GetImages16 GetNumberNewImages 
[ret, arr, validfirst, validlast] = atmcdmex('GetImages', first, last, size);

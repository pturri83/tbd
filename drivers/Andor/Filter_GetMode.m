function [ret, mode] = Filter_GetMode()
% Filter_GetMode Returns the current Noise Filter mode.
%
% SYNOPSIS : [ret, mode] = Filter_GetMode()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Filter mode returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Noise Filter processing not available for this camera.
%          DRV_P1INVALID - Invalid mode (i.e. NULL pointer)
%        mode: Noise Filter mode.
% REMARKS : C++ Equiv : unsigned int Filter_GetMode(unsigned int * mode);
%
% SEE ALSO : Filter_SetMode 
[ret, mode] = atmcdmex('Filter_GetMode');

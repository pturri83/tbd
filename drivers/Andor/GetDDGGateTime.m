function [ret, puiDelay, puiWidth] = GetDDGGateTime()
% GetDDGGateTime This function can be used to get the actual gate timings for a USB iStar.
%
% SYNOPSIS : [ret, puiDelay, puiWidth] = GetDDGGateTime()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timings returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - USB iStar not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - Delay has invalid memory address.
%        puiDelay: gate delay time in picoseconds.
%        puiWidth: gate width time in picoseconds.
% REMARKS : C++ Equiv : unsigned int GetDDGGateTime(at_u64 * puiDelay, at_u64 * puiWidth);
%
% SEE ALSO : GetCapabilities SetDDGGateTimeSetDDGGateStep 
[ret, puiDelay, puiWidth] = atmcdmex('GetDDGGateTime');

function [ret] = SetSpool(active, method, path, framebuffersize)
% SetSpool This function will enable and disable the spooling of acquired data to the hard disk or to the RAM.
% With spooling method 0, each scan in the series will be saved to a separate file composed of a sequence of 32-bit integers.
% With spooling method 1 the type of data in the output files depends on what type of acquisition is taking place (see below).
% Spooling method 2 writes out the data to file as 16-bit integers.
% Spooling method 3 creates a directory structure for storing images where multiple images may appear in each file within the directory structure and the files may be spread across multiple directories. Like method 1 the data type of the image pixels depends on whether accumulate mode is being used.
% Method 4 Creates a RAM disk for storing images so you should ensure that there is enough free RAM to store the full acquisition.
% Methods 5, 6 and 7 can be used to directly spool out to a particular file type, either FITS, SIF or TIFF respectively. In the case of FITS and TIFF the data will be written out as 16-bit values.
% Method 8 is similar to method 3, however the data is first compressed before writing to disk. In some circumstances this may improve the maximum rate of writing images to disk, however as the compression can be very CPU intensive this option may not be suitable on slower processors.
% The data is stored in row order starting with the row nearest the readout register. With the exception of methods 5, 6 and 7, the data acquired during a spooled acquisition can be retrieved through the normal functions. This is a change to previous versions; it is no longer necessary to load the data from disk from your own application.
%
% SYNOPSIS : [ret] = SetSpool(active, method, path, framebuffersize)
%
% INPUT active: Enable/disable spooling
%         0 - Disable spooling.
%         1 - Enable spooling.
%       method: Indicates the format of the files written to disk
%         0 - Files contain sequence of 32-bit integers
%         1 - Format of data in files depends on whether multiple accumulations are being taken for each scan. Format will be 32-bit integer if data is being accumulated each scan; otherwise the format will be 16-bit integer.
%         2 - Files contain sequence of 16-bit integers.
%         3 - Multiple directory structure with multiple images per file and multiple files per directory.
%         4 - Spool to RAM disk.
%         5 - Spool to 16-bit Fits File.
%         6 - Spool to Andor Sif format.
%         7 - Spool to 16-bit Tiff File.
%         8 - Similar to method 3 but with data compression.
%       path: String containing the filename stem. May also contain the path to the directory into which the files are to be stored.
%       framebuffersize: This sets the size of an internal circular buffer used as temporary storage. The value is the total number images the buffer can hold, not the size in bytes. Typical value would be 10. This value would be increased in situations where the computer is not able to spool the data to disk at the required rate.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
% REMARKS : C++ Equiv : unsigned int SetSpool(int active, int method, char * path, int framebuffersize);
%
% SEE ALSO : GetSpoolProgress 
[ret] = atmcdmex('SetSpool', active, method, path, framebuffersize);

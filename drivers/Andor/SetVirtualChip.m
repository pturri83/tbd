function [ret] = SetVirtualChip(state)
% SetVirtualChip THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetVirtualChip(state)
%
% INPUT state: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetVirtualChip(int state);
%
% SEE ALSO : 
[ret] = atmcdmex('SetVirtualChip', state);

function [ret, IRQ] = GetIRQ()
% GetIRQ THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, IRQ] = GetIRQ()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        IRQ: 
% REMARKS : C++ Equiv : unsigned int GetIRQ(int * IRQ);
%
% SEE ALSO : 
[ret, IRQ] = atmcdmex('GetIRQ');

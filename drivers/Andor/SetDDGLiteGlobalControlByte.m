function [ret] = SetDDGLiteGlobalControlByte(control)
% SetDDGLiteGlobalControlByte THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetDDGLiteGlobalControlByte(control)
%
% INPUT control: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDDGLiteGlobalControlByte(char control);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGLiteGlobalControlByte', control);

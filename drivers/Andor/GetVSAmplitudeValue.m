function [ret, value] = GetVSAmplitudeValue(index)
% GetVSAmplitudeValue This Function is used to get the value of the Vertical Clock Amplitude found at the index passed in.
%
% SYNOPSIS : [ret, value] = GetVSAmplitudeValue(index)
%
% INPUT index: Index of VS amplitude required
%         0 - to GetNumberVSAmplitudes()-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Vertical Clock Amplitude value returned
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Invalid index.
%          DRV_P2INVALID - Invalid value pointer.
%        value: Returns Value of Vertical Clock Amplitude that matches index passed in
% REMARKS : C++ Equiv : unsigned int GetVSAmplitudeValue(int index, int * value);
%
% SEE ALSO : GetVSAmplitudeFromString GetVSAmplitudeString 
[ret, value] = atmcdmex('GetVSAmplitudeValue', index);

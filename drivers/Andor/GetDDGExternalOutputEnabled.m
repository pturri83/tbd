function [ret, puiEnabled] = GetDDGExternalOutputEnabled(uiIndex)
% GetDDGExternalOutputEnabled This function gets the current state of a selected external output.
%
% SYNOPSIS : [ret, puiEnabled] = GetDDGExternalOutputEnabled(uiIndex)
%
% INPUT uiIndex: index of external output.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with card.
%          DRV_P2INVALID - Invalid external output index.
%        puiEnabled: current state of external output (0 - Off, 1 - On).
% REMARKS : C++ Equiv : unsigned int GetDDGExternalOutputEnabled(at_u32 uiIndex, at_u32 * puiEnabled);
%
% SEE ALSO : GetCapabilities SetDDGExternalOutputEnabled SetDDGGateStep 
[ret, puiEnabled] = atmcdmex('GetDDGExternalOutputEnabled', uiIndex);

function [ret] = SetDDGIntelligate(state)
% SetDDGIntelligate This function controls the MCP gating. Not available when the fast insertion delay option is selected.
%
% SYNOPSIS : [ret] = SetDDGIntelligate(state)
%
% INPUT state: ON/OFF switch for the MCP gating.
%         0 - to switch MCP gating OFF.
%         1 - to switch MCP gating ON.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - intelligate option accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with system.
% REMARKS : C++ Equiv : unsigned int SetDDGIntelligate(int state);
%
% SEE ALSO : GetCapabilities SetDDGInsertionDelay 
[ret] = atmcdmex('SetDDGIntelligate', state);

function [ret, iXshift, iYShift] = GetCYMGShift()
% GetCYMGShift THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, iXshift, iYShift] = GetCYMGShift()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        iXshift: 
%        iYShift: 
% REMARKS : C++ Equiv : unsigned int GetCYMGShift(int * iXshift, int * iYShift);
%
% SEE ALSO : 
[ret, iXshift, iYShift] = atmcdmex('GetCYMGShift');

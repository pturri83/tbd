function [ret, mode] = GetRegisterDump()
% GetRegisterDump THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, mode] = GetRegisterDump()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        mode: 
% REMARKS : C++ Equiv : unsigned int GetRegisterDump(int * mode);
%
% SEE ALSO : 
[ret, mode] = atmcdmex('GetRegisterDump');

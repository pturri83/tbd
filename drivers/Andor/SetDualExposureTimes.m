function [ret] = SetDualExposureTimes(expTime1, expTime2)
% SetDualExposureTimes This function configures the two exposure times used in dual exposure mode.  This mode is only available for certain sensors in run till abort mode, external trigger, full image.
%
% SYNOPSIS : [ret] = SetDualExposureTimes(expTime1, expTime2)
%
% INPUT expTime1: the exposure time in seconds for each odd numbered frame.
%       expTime2: the exposure time in seconds for each even numbered frame.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Dual exposure mode not supported on this camera.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - First exposure out of range.
%          DRV_P2INVALID - Second exposure out of range.
% REMARKS : C++ Equiv : unsigned int SetDualExposureTimes(float expTime1, float expTime2);
%
% SEE ALSO : GetCapabilities SetDualExposureMode GetDualExposureTimes 
[ret] = atmcdmex('SetDualExposureTimes', expTime1, expTime2);

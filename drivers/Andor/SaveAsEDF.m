function [ret] = SaveAsEDF(szPath, iMode)
% SaveAsEDF This function saves the last acquisition in the European Synchotron Radiation Facility Data Format (*.edf).
%
% SYNOPSIS : [ret] = SaveAsEDF(szPath, iMode)
%
% INPUT szPath: the filename to save to.
%       iMode: option to save to multiple files.
%         0 - Save to 1 file
%         1 - Save kinetic series to multiple files
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data successfully saved.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Path invalid.
%          DRV_P2INVALID - Invalid mode
%          DRV_ERROR_PAGELOCK - File too large to be generated in memory.
% REMARKS : C++ Equiv : unsigned int SaveAsEDF(char * szPath, int iMode);
%
% SEE ALSO : SaveAsSif SaveAsFITS SaveAsRaw SaveAsSPC SaveAsTiff SaveAsBmp 
[ret] = atmcdmex('SaveAsEDF', szPath, iMode);

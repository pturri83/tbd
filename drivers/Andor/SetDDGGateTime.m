function [ret] = SetDDGGateTime(uiDelay, uiWidth)
% SetDDGGateTime This function can be used to set the gate timings for a USB iStar.
%
% SYNOPSIS : [ret] = SetDDGGateTime(uiDelay, uiWidth)
%
% INPUT uiDelay: gate delay time in picoseconds.
%       uiWidth: gate width time in picoseconds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timings set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - USB iStar not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - Invalid delay.
% REMARKS : C++ Equiv : unsigned int SetDDGGateTime(at_u64 uiDelay, at_u64 uiWidth);
%
% SEE ALSO : GetCapabilities GetDDGGateTime 
[ret] = atmcdmex('SetDDGGateTime', uiDelay, uiWidth);

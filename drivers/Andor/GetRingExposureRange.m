function [ret, fpMin, fpMax] = GetRingExposureRange()
% GetRingExposureRange With the Ring Of Exposure feature there may be a case when not all exposures can be met. The ring of exposure feature will guarantee that the highest exposure will be met but this may mean that the lower exposures may not be. If the lower exposures are too low they will be increased to the lowest value possible. This function will return these upper and lower values.
%
% SYNOPSIS : [ret, fpMin, fpMax] = GetRingExposureRange()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Min and max returned
%          DRV_NOT_INITIALIZED - System not initialize
%          DRV_INVALID_MODE - Trigger mode is not available
%        fpMin: Minimum exposure
%        fpMax: Maximum exposure.
% REMARKS : C++ Equiv : unsigned int GetRingExposureRange(float * fpMin, float * fpMax);
%
% SEE ALSO : GetCapabilities GetNumberRingExposureTimes IsTriggerModeAvailable SetRingExposureTimes 
[ret, fpMin, fpMax] = atmcdmex('GetRingExposureRange');

function [ret] = SetMultiTrackHBin(bin)
% SetMultiTrackHBin This function sets the horizontal binning used when acquiring in Multi-Track read mode.
%
% SYNOPSIS : [ret] = SetMultiTrackHBin(bin)
%
% INPUT bin: Binning size.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Binning set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid binning size.
% REMARKS : C++ Equiv : unsigned int SetMultiTrackHBin(int bin);
%
% SEE ALSO : SetReadMode SetMultiTrack SetReadMode 
[ret] = atmcdmex('SetMultiTrackHBin', bin);

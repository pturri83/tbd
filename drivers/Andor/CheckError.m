function CheckError(code)
% Converts the code returned by the SDK into an error message
% CheckError(code)
% Arguments
%     code: the return code from the SDK
% Returns:
%     None

  switch(code)
    case 20001
      error('atmcd SDK returned Error: DRV_ERROR_CODES [20001]');
    case 20002
      return
    case 20003
      error('atmcd SDK returned Error: DRV_VXDNOTINSTALLED [20003]');
    case 20004
      error('atmcd SDK returned Error: DRV_ERROR_SCAN [20004]');
    case 20005
      error('atmcd SDK returned Error: DRV_ERROR_CHECK_SUM [20005]');
    case 20006
      error('atmcd SDK returned Error: DRV_ERROR_FILELOAD [20006]');
    case 20007
      error('atmcd SDK returned Error: DRV_UNKNOWN_FUNCTION [20007]');
    case 20008
      error('atmcd SDK returned Error: DRV_ERROR_VXD_INIT [20008]');
    case 20009
      error('atmcd SDK returned Error: DRV_ERROR_ADDRESS [20009]');
    case 20010
      error('atmcd SDK returned Error: DRV_ERROR_PAGELOCK [20010]');
    case 20011
      error('atmcd SDK returned Error: DRV_ERROR_PAGEUNLOCK [20011]');
    case 20012
      error('atmcd SDK returned Error: DRV_ERROR_BOARDTEST [20012]');
    case 20013
      error('atmcd SDK returned Error: DRV_ERROR_ACK [20013]');
    case 20014
      error('atmcd SDK returned Error: DRV_ERROR_UP_FIFO [20014]');
    case 20015
      error('atmcd SDK returned Error: DRV_ERROR_PATTERN [20015]');
    case 20017
      error('atmcd SDK returned Error: DRV_ACQUISITION_ERRORS [20017]');
    case 20018
      error('atmcd SDK returned Error: DRV_ACQ_BUFFER [20018]');
    case 20019
      error('atmcd SDK returned Error: DRV_ACQ_DOWNFIFO_FULL [20019]');
    case 20020
      error('atmcd SDK returned Error: DRV_PROC_UNKONWN_INSTRUCTION [20020]');
    case 20021
      error('atmcd SDK returned Error: DRV_ILLEGAL_OP_CODE [20021]');
    case 20022
      error('atmcd SDK returned Error: DRV_KINETIC_TIME_NOT_MET [20022]');
    case 20023
      error('atmcd SDK returned Error: DRV_ACCUM_TIME_NOT_MET [20023]');
    case 20024
      error('atmcd SDK returned Error: DRV_NO_NEW_DATA [20024]');
    case 20025
      error('atmcd SDK returned Error: DRV_PCI_DMA_FAIL [20025]');
    case 20026
      error('atmcd SDK returned Error: DRV_SPOOLERROR [20026]');
    case 20027
      error('atmcd SDK returned Error: DRV_SPOOLSETUPERROR [20027]');
    case 20028
      error('atmcd SDK returned Error: DRV_FILESIZELIMITERROR [20028]');
    case 20029
      error('atmcd SDK returned Error: DRV_ERROR_FILESAVE [20029]');
    case 20033
      error('atmcd SDK returned Error: DRV_TEMPERATURE_CODES [20033]');
    case 20034
      error('atmcd SDK returned Error: DRV_TEMPERATURE_OFF [20034]');
    case 20035
      error('atmcd SDK returned Error: DRV_TEMPERATURE_NOT_STABILIZED [20035]');
    case 20036
      error('atmcd SDK returned Error: DRV_TEMPERATURE_STABILIZED [20036]');
    case 20037
      error('atmcd SDK returned Error: DRV_TEMPERATURE_NOT_REACHED [20037]');
    case 20038
      error('atmcd SDK returned Error: DRV_TEMPERATURE_OUT_RANGE [20038]');
    case 20039
      error('atmcd SDK returned Error: DRV_TEMPERATURE_NOT_SUPPORTED [20039]');
    case 20040
      error('atmcd SDK returned Error: DRV_TEMPERATURE_DRIFT [20040]');
    case 20033
      error('atmcd SDK returned Error: DRV_TEMP_CODES [20033]');
    case 20034
      error('atmcd SDK returned Error: DRV_TEMP_OFF [20034]');
    case 20035
      error('atmcd SDK returned Error: DRV_TEMP_NOT_STABILIZED [20035]');
    case 20036
      error('atmcd SDK returned Error: DRV_TEMP_STABILIZED [20036]');
    case 20037
      error('atmcd SDK returned Error: DRV_TEMP_NOT_REACHED [20037]');
    case 20038
      error('atmcd SDK returned Error: DRV_TEMP_OUT_RANGE [20038]');
    case 20039
      error('atmcd SDK returned Error: DRV_TEMP_NOT_SUPPORTED [20039]');
    case 20040
      error('atmcd SDK returned Error: DRV_TEMP_DRIFT [20040]');
    case 20049
      error('atmcd SDK returned Error: DRV_GENERAL_ERRORS [20049]');
    case 20050
      error('atmcd SDK returned Error: DRV_INVALID_AUX [20050]');
    case 20051
      error('atmcd SDK returned Error: DRV_COF_NOTLOADED [20051]');
    case 20052
      error('atmcd SDK returned Error: DRV_FPGAPROG [20052]');
    case 20053
      error('atmcd SDK returned Error: DRV_FLEXERROR [20053]');
    case 20054
      error('atmcd SDK returned Error: DRV_GPIBERROR [20054]');
    case 20055
      error('atmcd SDK returned Error: DRV_EEPROMVERSIONERROR [20055]');
    case 20064
      error('atmcd SDK returned Error: DRV_DATATYPE [20064]');
    case 20065
      error('atmcd SDK returned Error: DRV_DRIVER_ERRORS [20065]');
    case 20066
      error('atmcd SDK returned Error: DRV_P1INVALID [20066]');
    case 20067
      error('atmcd SDK returned Error: DRV_P2INVALID [20067]');
    case 20068
      error('atmcd SDK returned Error: DRV_P3INVALID [20068]');
    case 20069
      error('atmcd SDK returned Error: DRV_P4INVALID [20069]');
    case 20070
      error('atmcd SDK returned Error: DRV_INIERROR [20070]');
    case 20071
      error('atmcd SDK returned Error: DRV_COFERROR [20071]');
    case 20072
      error('atmcd SDK returned Error: DRV_ACQUIRING [20072]');
    case 20073
      error('atmcd SDK returned Error: DRV_IDLE [20073]');
    case 20074
      error('atmcd SDK returned Error: DRV_TEMPCYCLE [20074]');
    case 20075
      error('atmcd SDK returned Error: DRV_NOT_INITIALIZED [20075]');
    case 20076
      error('atmcd SDK returned Error: DRV_P5INVALID [20076]');
    case 20077
      error('atmcd SDK returned Error: DRV_P6INVALID [20077]');
    case 20078
      error('atmcd SDK returned Error: DRV_INVALID_MODE [20078]');
    case 20079
      error('atmcd SDK returned Error: DRV_INVALID_FILTER [20079]');
    case 20080
      error('atmcd SDK returned Error: DRV_I2CERRORS [20080]');
    case 20081
      error('atmcd SDK returned Error: DRV_I2CDEVNOTFOUND [20081]');
    case 20082
      error('atmcd SDK returned Error: DRV_I2CTIMEOUT [20082]');
    case 20083
      error('atmcd SDK returned Error: DRV_P7INVALID [20083]');
    case 20084
      error('atmcd SDK returned Error: DRV_P8INVALID [20084]');
    case 20085
      error('atmcd SDK returned Error: DRV_P9INVALID [20085]');
    case 20086
      error('atmcd SDK returned Error: DRV_P10INVALID [20086]');
    case 20087
      error('atmcd SDK returned Error: DRV_P11INVALID [20087]');
    case 20089
      error('atmcd SDK returned Error: DRV_USBERROR [20089]');
    case 20090
      error('atmcd SDK returned Error: DRV_IOCERROR [20090]');
    case 20091
      error('atmcd SDK returned Error: DRV_VRMVERSIONERROR [20091]');
    case 20092
      error('atmcd SDK returned Error: DRV_GATESTEPERROR [20092]');
    case 20093
      error('atmcd SDK returned Error: DRV_USB_INTERRUPT_ENDPOINT_ERROR [20093]');
    case 20094
      error('atmcd SDK returned Error: DRV_RANDOM_TRACK_ERROR [20094]');
    case 20095
      error('atmcd SDK returned Error: DRV_INVALID_TRIGGER_MODE [20095]');
    case 20096
      error('atmcd SDK returned Error: DRV_LOAD_FIRMWARE_ERROR [20096]');
    case 20097
      error('atmcd SDK returned Error: DRV_DIVIDE_BY_ZERO_ERROR [20097]');
    case 20098
      error('atmcd SDK returned Error: DRV_INVALID_RINGEXPOSURES [20098]');
    case 20099
      error('atmcd SDK returned Error: DRV_BINNING_ERROR [20099]');
    case 20100
      error('atmcd SDK returned Error: DRV_INVALID_AMPLIFIER [20100]');
    case 20101
      error('atmcd SDK returned Error: DRV_INVALID_COUNTCONVERT_MODE [20101]');
    case 20990
      error('atmcd SDK returned Error: DRV_ERROR_NOCAMERA [20990]');
    case 20991
      error('atmcd SDK returned Error: DRV_NOT_SUPPORTED [20991]');
    case 20992
      error('atmcd SDK returned Error: DRV_NOT_AVAILABLE [20992]');
    case 20115
      error('atmcd SDK returned Error: DRV_ERROR_MAP [20115]');
    case 20116
      error('atmcd SDK returned Error: DRV_ERROR_UNMAP [20116]');
    case 20117
      error('atmcd SDK returned Error: DRV_ERROR_MDL [20117]');
    case 20118
      error('atmcd SDK returned Error: DRV_ERROR_UNMDL [20118]');
    case 20119
      error('atmcd SDK returned Error: DRV_ERROR_BUFFSIZE [20119]');
    case 20121
      error('atmcd SDK returned Error: DRV_ERROR_NOHANDLE [20121]');
    case 20130
      error('atmcd SDK returned Error: DRV_GATING_NOT_AVAILABLE [20130]');
    case 20131
      error('atmcd SDK returned Error: DRV_FPGA_VOLTAGE_ERROR [20131]');
    case 20150
      error('atmcd SDK returned Error: DRV_OW_CMD_FAIL [20150]');
    case 20151
      error('atmcd SDK returned Error: DRV_OWMEMORY_BAD_ADDR [20151]');
    case 20152
      error('atmcd SDK returned Error: DRV_OWCMD_NOT_AVAILABLE [20152]');
    case 20153
      error('atmcd SDK returned Error: DRV_OW_NO_SLAVES [20153]');
    case 20154
      error('atmcd SDK returned Error: DRV_OW_NOT_INITIALIZED [20154]');
    case 20155
      error('atmcd SDK returned Error: DRV_OW_ERROR_SLAVE_NUM [20155]');
    case 20156
      error('atmcd SDK returned Error: DRV_MSTIMINGS_ERROR [20156]');
    case 20173
      error('atmcd SDK returned Error: DRV_OA_NULL_ERROR [20173]');
    case 20174
      error('atmcd SDK returned Error: DRV_OA_PARSE_DTD_ERROR [20174]');
    case 20175
      error('atmcd SDK returned Error: DRV_OA_DTD_VALIDATE_ERROR [20175]');
    case 20176
      error('atmcd SDK returned Error: DRV_OA_FILE_ACCESS_ERROR [20176]');
    case 20177
      error('atmcd SDK returned Error: DRV_OA_FILE_DOES_NOT_EXIST [20177]');
    case 20178
      error('atmcd SDK returned Error: DRV_OA_XML_INVALID_OR_NOT_FOUND_ERROR [20178]');
    case 20179
      error('atmcd SDK returned Error: DRV_OA_PRESET_FILE_NOT_LOADED [20179]');
    case 20180
      error('atmcd SDK returned Error: DRV_OA_USER_FILE_NOT_LOADED [20180]');
    case 20181
      error('atmcd SDK returned Error: DRV_OA_PRESET_AND_USER_FILE_NOT_LOADED [20181]');
    case 20182
      error('atmcd SDK returned Error: DRV_OA_INVALID_FILE [20182]');
    case 20183
      error('atmcd SDK returned Error: DRV_OA_FILE_HAS_BEEN_MODIFIED [20183]');
    case 20184
      error('atmcd SDK returned Error: DRV_OA_BUFFER_FULL [20184]');
    case 20185
      error('atmcd SDK returned Error: DRV_OA_INVALID_STRING_LENGTH [20185]');
    case 20186
      error('atmcd SDK returned Error: DRV_OA_INVALID_CHARS_IN_NAME [20186]');
    case 20187
      error('atmcd SDK returned Error: DRV_OA_INVALID_NAMING [20187]');
    case 20188
      error('atmcd SDK returned Error: DRV_OA_GET_CAMERA_ERROR [20188]');
    case 20189
      error('atmcd SDK returned Error: DRV_OA_MODE_ALREADY_EXISTS [20189]');
    case 20190
      error('atmcd SDK returned Error: DRV_OA_STRINGS_NOT_EQUAL [20190]');
    case 20191
      error('atmcd SDK returned Error: DRV_OA_NO_USER_DATA [20191]');
    case 20192
      error('atmcd SDK returned Error: DRV_OA_VALUE_NOT_SUPPORTED [20192]');
    case 20193
      error('atmcd SDK returned Error: DRV_OA_MODE_DOES_NOT_EXIST [20193]');
    case 20194
      error('atmcd SDK returned Error: DRV_OA_CAMERA_NOT_SUPPORTED [20194]');
    case 20195
      error('atmcd SDK returned Error: DRV_OA_FAILED_TO_GET_MODE [20195]');
    case 20211
      error('atmcd SDK returned Error: DRV_PROCESSING_FAILED [20211]');
    otherwise
      warning('Andor SDK Returned Error: UNKNOWN ERROR [%d]',code);
  end
end

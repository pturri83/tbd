function [ret, pcListOfParams] = OA_GetModeAcqParams(pcModeName)
% OA_GetModeAcqParams This function will return all acquisition parameters associated with the specified mode.  The mode specified by the user must be in either the Preset file or the User defined file.  The user must allocate enough memory for all of the acquisition parameters.
%
% SYNOPSIS : [ret, pcListOfParams] = OA_GetModeAcqParams(pcModeName)
%
% INPUT pcModeName: The mode for which all acquisition parameters must be returned.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null mode name.
%          DRV_P2INVALID - Null mode parameter.
%          DRV_OA_NO_USER_DATA - No data for selected mode.
%        pcListOfParams: A user allocated array of characters for storage of the acquisition parameters.  Parameters will be delimited by a ','.
% REMARKS : C++ Equiv : unsigned int OA_GetModeAcqParams(const char * pcModeName, char * pcListOfParams);
%
% SEE ALSO : OA_GetNumberOfAcqParams 
[ret, pcListOfParams] = atmcdmex('OA_GetModeAcqParams', pcModeName);

function [ret, control] = GetDDGLiteControlByte(channel)
% GetDDGLiteControlByte THIS FUNCTION IS RESERVED
%
% SYNOPSIS : [ret, control] = GetDDGLiteControlByte(channel)
%
% INPUT channel: 
% OUTPUT ret: Return Code: 
%        control: 
% REMARKS : C++ Equiv : unsigned int GetDDGLiteControlByte(AT_DDGLiteChannelId channel, unsigned char * control);
%
% SEE ALSO : 
[ret, control] = atmcdmex('GetDDGLiteControlByte', channel);

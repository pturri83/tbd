function [ret] = AndorShutDown()
% ShutDown This function will close the AndorMCD system down.
%
% SYNOPSIS : [ret] = ShutDown()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - System shut down.
% REMARKS : C++ Equiv : unsigned int ShutDown(void);
%
% SEE ALSO : CoolerOFF CoolerON SetTemperature GetTemperature 
[ret] = atmcdmex('ShutDown');

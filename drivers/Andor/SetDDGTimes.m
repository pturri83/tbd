function [ret] = SetDDGTimes(t0, t1, t2)
% SetDDGTimes This function sets the properties of the gate pulse. t0 has a resolution of 16 nanoseconds whilst t1 and t2 have a resolution of 25 picoseconds.
%
% SYNOPSIS : [ret] = SetDDGTimes(t0, t1, t2)
%
% INPUT t0: output A delay in nanoseconds.
%       t1: gate delay in picoseconds.
%       t2: pulse width in picoseconds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Values for gate pulse accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid output A delay.
%          DRV_P2INVALID - Invalid gate delay.
%          DRV_P3INVALID - Invalid pulse width.
% REMARKS : C++ Equiv : unsigned int SetDDGTimes(double t0, double t1, double t2);
%
% SEE ALSO : SetDDGGateStep 
[ret] = atmcdmex('SetDDGTimes', t0, t1, t2);

function [ret, speed] = GetHSSpeed(channel, typ, index)
% GetHSSpeed As your Andor system is capable of operating at more than one horizontal shift speed this function will return the actual speeds available. The value returned is in MHz.
%
% SYNOPSIS : [ret, speed] = GetHSSpeed(channel, typ, index)
%
% INPUT channel: the AD channel.
%       typ: output amplification.
%         0 - electron multiplication/Conventional(clara).
%         1 - conventional/Extended NIR Mode(clara).
%       index: speed required Valid values: 0 to NumberSpeeds-1, where NumberSpeeds is value returned in first parameter after a call to GetNumberHSSpeedsGetNumberHSSpeeds().
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Speed returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Invalid channel.
%          DRV_P2INVALID - Invalid horizontal read mode
%          DRV_P3INVALID - Invalid index
%        speed: speed in in MHz.
% REMARKS : C++ Equiv : unsigned int GetHSSpeed(int channel, int typ, int index, float * speed);
%
% SEE ALSO : GetNumberHSSpeeds SetHSSpeed 
[ret, speed] = atmcdmex('GetHSSpeed', channel, typ, index);

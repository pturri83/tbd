function [ret] = SetDDGLiteControlByte(channel, control)
% SetDDGLiteControlByte THIS FUNCTION IS RESERVED
%
% SYNOPSIS : [ret] = SetDDGLiteControlByte(channel, control)
%
% INPUT channel: 
%       control: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDDGLiteControlByte(AT_DDGLiteChannelId channel, char control);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGLiteControlByte', channel, control);

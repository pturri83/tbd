function [ret, minval, maxval] = GetCountConvertWavelengthRange()
% GetCountConvertWavelengthRange This function returns the valid wavelength range available in Count Convert mode.
%
% SYNOPSIS : [ret, minval, maxval] = GetCountConvertWavelengthRange()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Count Convert wavelength set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Count Convert not supported on this camera
%        minval: minimum wavelength permited.
%        maxval: maximum wavelength permited.
% REMARKS : C++ Equiv : unsigned int GetCountConvertWavelengthRange(float * minval, float * maxval);
%
% SEE ALSO : GetCapabilities SetCountConvertMode SetCountConvertWavelength 
[ret, minval, maxval] = atmcdmex('GetCountConvertWavelengthRange');

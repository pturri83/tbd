function [ret, iVoltage] = GetMCPVoltage()
% GetMCPVoltage This function will retrieve the current Micro Channel Plate voltage.
%
% SYNOPSIS : [ret, iVoltage] = GetMCPVoltage()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Voltage returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_AVAILABLE - Not a USB iStar.
%          DRV_GENERAL_ERRORS - EEPROM not valid
%        iVoltage: Will contain voltage on return. The unit is in Volts and should be between the range 600 - 1100 Volts.
% REMARKS : C++ Equiv : unsigned int GetMCPVoltage(int * iVoltage);
%
% SEE ALSO : GetMCPGain 
[ret, iVoltage] = atmcdmex('GetMCPVoltage');

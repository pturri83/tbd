function [ret] = SaveEEPROMToFile(cFileName)
% SaveEEPROMToFile THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SaveEEPROMToFile(cFileName)
%
% INPUT cFileName: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SaveEEPROMToFile(char * cFileName);
%
% SEE ALSO : 
[ret] = atmcdmex('SaveEEPROMToFile', cFileName);

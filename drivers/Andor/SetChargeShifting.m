function [ret] = SetChargeShifting(NumberRows, NumberRepeats)
% SetChargeShifting Use this function in External Charge Shifting trigger mode to configure how many rows to shift and how many times for each frame of data.  The number of repeats must be a multiple of 2.
%
% SYNOPSIS : [ret] = SetChargeShifting(NumberRows, NumberRepeats)
%
% INPUT NumberRows: number of rows to shift after each external trigger
%       NumberRepeats: number of times to shift rows
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Success
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Trigger mode not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Number of rows invalid.
%          DRV_P2INVALID - Number of repeats invalid.
% REMARKS : C++ Equiv : unsigned int SetChargeShifting(unsigned int NumberRows, unsigned int NumberRepeats);
%
% SEE ALSO : SetTriggerMode GetCapabilities 
[ret] = atmcdmex('SetChargeShifting', NumberRows, NumberRepeats);

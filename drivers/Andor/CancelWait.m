function [ret] = CancelWait()
% CancelWait This function restarts a thread which is sleeping within the WaitForAcquisitionWaitForAcquisition function. The sleeping thread will return from WaitForAcquisition with a value not equal to DRV_SUCCESS.
%
% SYNOPSIS : [ret] = CancelWait()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Thread restarted successfully.
% REMARKS : C++ Equiv : unsigned int CancelWait(void);
%
% SEE ALSO : WaitForAcquisition 
[ret] = atmcdmex('CancelWait');

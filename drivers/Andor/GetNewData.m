function [ret, arr] = GetNewData(size)
% GetNewData Deprecated see Note:
% This function will update the data array to hold data acquired so far. The data are returned as long integers (32-bit signed integers). The array must be large enough to hold the complete data set. When used in conjunction with the SetDriverEventSetDriverEvent and GetAcquisitonProgressGetAcquisitionProgress functions, the data from each scan in a kinetic series can be processed while the acquisition is taking place.
%
% SYNOPSIS : [ret, arr] = GetNewData(size)
%
% INPUT size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data copied.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Array size is incorrect.
%          DRV_NO_NEW_DATA - There is no new data yet.
%        arr: data storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int GetNewData(at_32 * arr, long size); // deprecated
%
% SEE ALSO : SetDriverEvent GetAcquisitionProgress SetAcquisitionMode SetAcGetNewData8 GetNewData16 
[ret, arr] = atmcdmex('GetNewData', size);

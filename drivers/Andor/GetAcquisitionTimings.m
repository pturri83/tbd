function [ret, exposure, accumulate, kinetic] = GetAcquisitionTimings()
% GetAcquisitionTimings This function will return the current "valid" acquisition timing information. This function should be used after all the acquisitions settings have been set, e.g. SetExposureTimeSetExposureTime, SetKineticCycleTimeSetKineticCycleTime and SetReadModeSetReadMode etc. The values returned are the actual times used in subsequent acquisitions.
% This function is required as it is possible to set the exposure time to 20ms, accumulate cycle time to 30ms and then set the readout mode to full image. As it can take 250ms to read out an image it is not possible to have a cycle time of 30ms.
%
% SYNOPSIS : [ret, exposure, accumulate, kinetic] = GetAcquisitionTimings()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timing information returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_INVALID_MODE - Acquisition or readout mode is not available.
%        exposure: valid exposure time in seconds
%        accumulate: valid accumulate cycle time in seconds
%        kinetic: valid kinetic cycle time in seconds
% REMARKS : C++ Equiv : unsigned int GetAcquisitionTimings(float * exposure, float * accumulate, float * kinetic);
%
% SEE ALSO : SetAccumulationCycleTime SetAcquisitionMode SetExposureTime SetHSSpeed SetKineticCycleTime SetMultiTrack SetNumberAccumulations SetNumberKinetics SetReadMode SetSingleTrack SetTriggerMode SetVSSpeed 
[ret, exposure, accumulate, kinetic] = atmcdmex('GetAcquisitionTimings');

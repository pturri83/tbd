function [ret] = SetFastKinetics(exposedRows, seriesLength, time, mode, hbin, vbin)
% SetFastKinetics This function will set the parameters to be used when taking a fast kinetics acquisition.
%
% SYNOPSIS : [ret] = SetFastKinetics(exposedRows, seriesLength, time, mode, hbin, vbin)
%
% INPUT exposedRows: sub-area height in rows.
%       seriesLength: number in series.
%       time: exposure time in seconds.
%       mode: binning mode (0 - FVB , 4 - Image).
%       hbin: horizontal binning.
%       vbin: vertical binning (only used when in image mode).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - System not initialized.
%          DRV_NOT_INITIALIZED - Acquisition in progress.
%          DRV_ACQUIRING - Invalid height.
%          DRV_P1INVALID - Invalid number in series.
%          DRV_P2INVALID DRV_P3INVALID - Exposure time must be greater than 0.
%          DRV_P4INVALID DRV_P5INVALID - Mode must be equal to 0 or 4.
%          DRV_P6INVALID - Horizontal binning.
%          All parameters accepted. - Vertical binning.
% REMARKS : C++ Equiv : unsigned int SetFastKinetics(int exposedRows, int seriesLength, float time, int mode, int hbin, int vbin);
%
% SEE ALSO : SetFKVShiftSpeed SetFastKineticsEx SetFKVShiftSpeed 
[ret] = atmcdmex('SetFastKinetics', exposedRows, seriesLength, time, mode, hbin, vbin);

function [ret, SensorTemp, TargetTemp, AmbientTemp, CoolerVolts] = GetTemperatureStatus()
% GetTemperatureStatus THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, SensorTemp, TargetTemp, AmbientTemp, CoolerVolts] = GetTemperatureStatus()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        SensorTemp: 
%        TargetTemp: 
%        AmbientTemp: 
%        CoolerVolts: 
% REMARKS : C++ Equiv : unsigned int GetTemperatureStatus(float * SensorTemp, float * TargetTemp, float * AmbientTemp, float * CoolerVolts);
%
% SEE ALSO : 
[ret, SensorTemp, TargetTemp, AmbientTemp, CoolerVolts] = atmcdmex('GetTemperatureStatus');

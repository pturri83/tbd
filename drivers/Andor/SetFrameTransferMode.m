function [ret] = SetFrameTransferMode(mode)
% SetFrameTransferMode This function will set whether an acquisition will readout in Frame Transfer Mode. If the acquisition mode is Single Scan or Fast Kinetics this call will have no affect.
%
% SYNOPSIS : [ret] = SetFrameTransferMode(mode)
%
% INPUT mode: mode
%         0 - OFF
%         1 - ON
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Frame transfer mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid parameter.
% REMARKS : C++ Equiv : unsigned int SetFrameTransferMode(int mode);
%
% SEE ALSO : SetAcquisitionMode 
[ret] = atmcdmex('SetFrameTransferMode', mode);

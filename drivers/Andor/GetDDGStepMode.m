function [ret, mode] = GetDDGStepMode()
% GetDDGStepMode This function will return the current gate step mode.
%
% SYNOPSIS : [ret, mode] = GetDDGStepMode()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gate step mode returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Gate step not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - mode has invalid memory address.
%        mode: the gate step mode.
%          0 - constant.
%          1 - exponential.
%          2 - logarithmic.
%          3 - linear.
%          100 - off.
% REMARKS : C++ Equiv : unsigned int GetDDGStepMode(at_u32 * mode);
%
% SEE ALSO : StartAcquisition SetDDGStepMode SetDDGStepCoefficients GetDDGStepCoefficients 
[ret, mode] = atmcdmex('GetDDGStepMode');

function [ret, number] = GetNumberFKVShiftSpeeds()
% GetNumberFKVShiftSpeeds As your Andor SDK system is capable of operating at more than one fast kinetics vertical shift speed this function will return the actual number of speeds available.
%
% SYNOPSIS : [ret, number] = GetNumberFKVShiftSpeeds()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of speeds returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%        number: number of allowed speeds
% REMARKS : C++ Equiv : unsigned int GetNumberFKVShiftSpeeds(int * number);
%
% SEE ALSO : GetFKVShiftSpeedF SetFKVShiftSpeed 
[ret, number] = atmcdmex('GetNumberFKVShiftSpeeds');

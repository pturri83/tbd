function [ret, piGain, pfPhotoepc] = GetMCPGainTable(iNum)
% GetMCPGainTable THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, piGain, pfPhotoepc] = GetMCPGainTable(iNum)
%
% INPUT iNum: 
% OUTPUT ret: Return Code: 
%        piGain: 
%        pfPhotoepc: 
% REMARKS : C++ Equiv : unsigned int GetMCPGainTable(int iNum, int * piGain, float * pfPhotoepc);
%
% SEE ALSO : 
[ret, piGain, pfPhotoepc] = atmcdmex('GetMCPGainTable', iNum);

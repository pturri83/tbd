function [ret, camStatus] = GetCameraEventStatus()
% GetCameraEventStatus This function will return if the system is exposing or not.
%
% SYNOPSIS : [ret, camStatus] = GetCameraEventStatus()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Status returned
%          DRV_NOT_INITIALIZED - System not initialized
%        camStatus: The status of the firepulse will be returned that the firepulse is low
%          0 - Fire pulse low
%          1 - Fire pulse high
% REMARKS : C++ Equiv : unsigned int GetCameraEventStatus(DWORD * camStatus);
%
% SEE ALSO : SetAcqStatusEvent SetPCIMode 
[ret, camStatus] = atmcdmex('GetCameraEventStatus');

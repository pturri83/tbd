function [ret] = Filter_SetMode(mode)
% Filter_SetMode Set the Noise Filter to use.
%
% SYNOPSIS : [ret] = Filter_SetMode(mode)
%
% INPUT mode: Filter mode to use.
%         0 - No Filter
%         1 - Median Filter
%         2 - Level Above Filter
%         3 - interquartile Range Filter
%         4 - Noise Threshold Filter
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Filter set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Noise Filter processing not available for this camera.
%          DRV_P1INVALID - Invalid mode.
% REMARKS : C++ Equiv : unsigned int Filter_SetMode(int mode);
%
% SEE ALSO : Filter_GetMode 
[ret] = atmcdmex('Filter_SetMode', mode);

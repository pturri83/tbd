function [ret, arr, coeff, output, start, step_Renamed] = Merge(nOrder, nPoint, nPixel, fit, hbin)
% Merge THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, arr, coeff, output, start, step_Renamed] = Merge(nOrder, nPoint, nPixel, fit, hbin)
%
% INPUT nOrder: 
%       nPoint: 
%       nPixel: 
%       fit: 
%       hbin: 
% OUTPUT ret: Return Code: 
%        arr: 
%        coeff: 
%        output: 
%        start: 
%        step_Renamed: 
% REMARKS : C++ Equiv : unsigned int Merge(const at_32 * arr, long nOrder, long nPoint, long nPixel, float * coeff, long fit, long hbin, at_32 * output, float * start, float * step_Renamed);
%
% SEE ALSO : 
[ret, arr, coeff, output, start, step_Renamed] = atmcdmex('Merge', nOrder, nPoint, nPixel, fit, hbin);

function [ret] = SetMultiTrackHRange(iStart, iEnd)
% SetMultiTrackHRange This function sets the horizontal range used when acquiring in Multi Track read mode.
%
% SYNOPSIS : [ret] = SetMultiTrackHRange(iStart, iEnd)
%
% INPUT iStart: First horizontal pixel in multi track mode.
%       iEnd: iEndLast horizontal pixel in multi track mode.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Range set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_AVAILABLE - Feature not available for this camera.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid start position.
%          DRV_P2INVALID - Invalid end position.
% REMARKS : C++ Equiv : unsigned int SetMultiTrackHRange(int iStart, int iEnd);
%
% SEE ALSO : SetReadMode SetMultiTrack SetReadMode 
[ret] = atmcdmex('SetMultiTrackHRange', iStart, iEnd);

function [ret] = CoolerOFF()
% CoolerOFF Switches OFF the cooling. The rate of temperature change is controlled in some models until the temperature reaches 0°C. Control is returned immediately to the calling application.
%
% SYNOPSIS : [ret] = CoolerOFF()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Temperature controller switched OFF.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_NOT_SUPPORTED - Camera does not support switching cooler off.
% REMARKS : C++ Equiv : unsigned int CoolerOFF(void);
%
% SEE ALSO : CoolerON SetTemperature GetTemperature GetTemperatureF GetTemperatureRange GetStatus 
[ret] = atmcdmex('CoolerOFF');

function [ret] = SaveAsTiffEx(path, palette, position, typ, mode)
% SaveAsTiffEx This function saves the last acquisition as a tiff file, which can be loaded into an imaging package. This is an extended version of the SaveAsTiff function. The palette parameter specifies the location of a .PAL file, which describes the colors to use in the tiff. This file consists of 256 lines of ASCII text; each line containing three numbers separated by spaces indicating the red, green and blue component of the respective color value. The parameter position can be changed to export different scans in a kinetic series. If the acquisition is any other mode, position should be set to 1. The parameter typ can be set to 0, 1 or 2 which correspond to 8-bit, 16-bit and color, respectively. The mode parameter specifies the mode of output. Data can be output scaled from the min and max count values across the entire range of values (mode 0) or can remain unchanged (mode 1).Of course if the count value is higher or lower than the output data range then even in mode 1 data will be scaled.
%
% SYNOPSIS : [ret] = SaveAsTiffEx(path, palette, position, typ, mode)
%
% INPUT path: The filename of the tiff.
%       palette: The filename of a palette file (.PAL) for applying color to the tiff.
%       position: The number in the series, should be 1 for a single scan.
%       typ: The type of tiff file to create.
%       mode: The output mode
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data successfully saved as tiff
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Path invalid.
%          DRV_P2INVALID - Invalid palette file
%          DRV_P3INVALID - position out of range
%          DRV_P4INVALID - type not valid
%          DRV_P5INVALID - mode not valid
%          DRV_ERROR_PAGELOCK - File too large to be generated in memory
% REMARKS : C++ Equiv : unsigned int SaveAsTiffEx(char * path, char * palette, int position, int typ, int mode);
%
% SEE ALSO : SaveAsSif SaveAsEDF SaveAsFITS SaveAsRaw SaveAsSPC SaveAsTiff SaveAsBmp 
[ret] = atmcdmex('SaveAsTiffEx', path, palette, position, typ, mode);

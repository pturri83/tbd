function [ret, text] = GPIBReceive(id, address, size)
% GPIBReceive This function reads data from a device until a byte is received with the EOI line asserted or until size bytes have been read.
%
% SYNOPSIS : [ret, text] = GPIBReceive(id, address, size)
%
% INPUT id: The interface board number
%         short - address: Address of device to send data
%       address: The address to send the data to
%       size: Number of characters to read
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data received.
%          DRV_P3INVALID - Invalid pointer (e.g. NULL).  .Other errors may be returned by the GPIB device. Consult the help documentation supplied with these devices.
%        text: The data to be sent
% REMARKS : C++ Equiv : unsigned int GPIBReceive(int id, short address, char * text, int size);
%
% SEE ALSO : GPIBSend 
[ret, text] = atmcdmex('GPIBReceive', id, address, size);

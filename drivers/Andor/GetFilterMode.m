function [ret, mode] = GetFilterMode()
% GetFilterMode This function returns the current state of the cosmic ray filtering mode.
%
% SYNOPSIS : [ret, mode] = GetFilterMode()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Filter mode returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%        mode: current state of filter
%          0 - OFF
%          2 - ON
% REMARKS : C++ Equiv : unsigned int GetFilterMode(int * mode);
%
% SEE ALSO : SetFilterMode 
[ret, mode] = atmcdmex('GetFilterMode');

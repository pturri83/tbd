function [ret, iLow, iHigh] = GetMCPGainRange()
% GetMCPGainRange Returns the minimum and maximum values of the SetMCPGain function.
%
% SYNOPSIS : [ret, iLow, iHigh] = GetMCPGainRange()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gain range returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        iLow: lowest gain setting
%        iHigh: highest gain setting
% REMARKS : C++ Equiv : unsigned int GetMCPGainRange(int * iLow, int * iHigh);
%
% SEE ALSO : SetMCPGain 
[ret, iLow, iHigh] = atmcdmex('GetMCPGainRange');

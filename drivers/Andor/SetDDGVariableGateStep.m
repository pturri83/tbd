function [ret] = SetDDGVariableGateStep(mode, p1, p2)
% SetDDGVariableGateStep This function will set a varying value for the gate step in a kinetic series. The lowest available resolution is 25 picoseconds and the maximum permitted value is 25 seconds.
%
% SYNOPSIS : [ret] = SetDDGVariableGateStep(mode, p1, p2)
%
% INPUT mode: the gate step mode.
%         1 - Exponential (p1*exp(p2*n))
%         2 - Logarithmic (p1*log(p2*n))
%         3 - Linear (p1 + p2*n)
%         n - = 1, 2, ..., number in kinetic series
%       p1: 
%       p2: 
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gate step mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Gate step mode invalid.
% REMARKS : C++ Equiv : unsigned int SetDDGVariableGateStep(int mode, double p1, double p2);
%
% SEE ALSO : StartAcquisition 
[ret] = atmcdmex('SetDDGVariableGateStep', mode, p1, p2);

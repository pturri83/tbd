function [ret] = InitializeDevice(dir)
% InitializeDevice THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = InitializeDevice(dir)
%
% INPUT dir: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int InitializeDevice(char * dir);
%
% SEE ALSO : 
[ret] = atmcdmex('InitializeDevice', dir);

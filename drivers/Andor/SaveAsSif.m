function [ret] = SaveAsSif(path)
% SaveAsSif This function will save the data from the last acquisition into a file, which can be read in by the main application. User text can be added to sif files using the SaveAsCommentedSif and SetSifComment functions.
%
% SYNOPSIS : [ret] = SaveAsSif(path)
%
% INPUT path: a filename specified by the user.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data saved.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid filename.
%          DRV_ERROR_PAGELOCK - File too large to be generated in memory.
% REMARKS : C++ Equiv : unsigned int SaveAsSif(char * path);
%
% SEE ALSO : SaveAsEDF SaveAsFITS SaveAsRaw SaveAsSPC SaveAsTiff SaveAsBmp SetSifComment SaveAsCommentedSif 
[ret] = atmcdmex('SaveAsSif', path);

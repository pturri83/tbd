function [ret, noOfDivisions] = GetNumberPhotonCountingDivisions()
% GetNumberPhotonCountingDivisions Available in some systems is photon counting mode. This function gets the number of photon counting divisions available. The functions SetPhotonCounting and SetPhotonCountingThreshold can be used to specify which of these divisions is to be used.
%
% SYNOPSIS : [ret, noOfDivisions] = GetNumberPhotonCountingDivisions()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of photon counting divisions returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Invalid parameter.
%          DRV_NOT_AVAILABLE - Photon Counting not available
%        noOfDivisions: number of allowed photon counting divisions
% REMARKS : C++ Equiv : unsigned int GetNumberPhotonCountingDivisions(at_u32 * noOfDivisions);
%
% SEE ALSO : SetPhotonCounting IsPreAmpGainAvailable SetPhotonCountingThresholdGetPreAmpGain GetCapabilities 
[ret, noOfDivisions] = atmcdmex('GetNumberPhotonCountingDivisions');

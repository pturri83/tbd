function [ret, piState] = GetDDGInsertionDelay()
% GetDDGInsertionDelay This function gets the current state of the insertion delay.
%
% SYNOPSIS : [ret, piState] = GetDDGInsertionDelay()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Insertion delay state returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_SUPPORTED - Insertion delay not supported.
%          DRV_ERROR_ACK - Unable to communicate with system.
%        piState: current state of the insertion delay option (0 - Normal, 1 - Ultra Fast).
% REMARKS : C++ Equiv : unsigned int GetDDGInsertionDelay(int * piState);
%
% SEE ALSO : GetCapabilities SetDDGInsertionDelay SetDDGIntelligate 
[ret, piState] = atmcdmex('GetDDGInsertionDelay');

function [ret] = SetTriggerMode(mode)
% SetTriggerMode This function will set the trigger mode that the camera will operate in.
%
% SYNOPSIS : [ret] = SetTriggerMode(mode)
%
% INPUT mode: trigger mode
%         0 - internal
%         1 - External
%         6 - External Start
%         7 - External Exposure (Bulb)
%         9 - External FVB EM (only valid for EM Newton models in FVB mode)	10.	Software Trigger
%         12 - External Charge Shifting
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Trigger mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Trigger mode invalid.
% REMARKS : C++ Equiv : unsigned int SetTriggerMode(int mode);
%
% SEE ALSO : Trigger Modes SetFastExtTrigger 
[ret] = atmcdmex('SetTriggerMode', mode);

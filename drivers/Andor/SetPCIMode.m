function [ret] = SetPCIMode(mode, value)
% SetPCIMode With the CCI23 card, events can be sent when the camera is starting to expose and when it has finished exposing. This function will control whether those events happen or not.
%
% SYNOPSIS : [ret] = SetPCIMode(mode, value)
%
% INPUT mode: currently must be set to 1
%       value: 0 to disable the events, 1 to enable
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Acquisition Mode invalid
% REMARKS : C++ Equiv : unsigned int SetPCIMode(int mode, int value);
%
% SEE ALSO : SetAcqStatusEvent SetCameraStatusEnable 
[ret] = atmcdmex('SetPCIMode', mode, value);

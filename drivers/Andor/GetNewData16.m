function [ret, arr] = GetNewData16(size)
% GetNewData16 Deprecated see Note:
% 16-bit version of the GetNewDataGetNewData function.
%
% SYNOPSIS : [ret, arr] = GetNewData16(size)
%
% INPUT size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data copied.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Array size is incorrect.
%          DRV_NO_NEW_DATA - There is no new data yet.
%        arr: data storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int GetNewData16(WORD * arr, long size); // deprecated
%
% SEE ALSO : 
[ret, arr] = atmcdmex('GetNewData16', size);

function [ret, TimeOfStart, pfDifferences] = GetMSTimingsData(inoOfImages)
% GetMSTimingsData THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, TimeOfStart, pfDifferences] = GetMSTimingsData(inoOfImages)
%
% INPUT inoOfImages: 
% OUTPUT ret: Return Code: 
%        TimeOfStart: 
%        pfDifferences: 
% REMARKS : C++ Equiv : unsigned int GetMSTimingsData(SYSTEMTIME * TimeOfStart, float * pfDifferences, int inoOfImages);
%
% SEE ALSO : 
[ret, TimeOfStart, pfDifferences] = atmcdmex('GetMSTimingsData', inoOfImages);

function [ret] = SetFKVShiftSpeed(index)
% SetFKVShiftSpeed This function will set the fast kinetics vertical shift speed to one of the possible speeds of the system. It will be used for subsequent acquisitions.
%
% SYNOPSIS : [ret] = SetFKVShiftSpeed(index)
%
% INPUT index: the speed to be used
%         0 - to GetNumberFKVShiftSpeedsGetNumberFKVShiftSpeeds-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Fast kinetics vertical shift speed set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Index is out off range.
% REMARKS : C++ Equiv : unsigned int SetFKVShiftSpeed(int index);
%
% SEE ALSO : GetNumberFKVShiftSpeeds GetFKVShiftSpeedF 
[ret] = atmcdmex('SetFKVShiftSpeed', index);

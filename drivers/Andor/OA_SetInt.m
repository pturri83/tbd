function [ret] = OA_SetInt(pcModeName, pcModeParam, iintValue)
% OA_SetInt This function is used to set values for integer type acquisition parameters where the
% new values are stored in memory.  To commit changes to file call WriteToFile().
%
% SYNOPSIS : [ret] = OA_SetInt(pcModeName, pcModeParam, iintValue)
%
% INPUT pcModeName: The name of the mode for which an acquisition parameter will be edited.
%       pcModeParam: The name of the acquisition parameter to be edited.
%       iintValue: The value to assign to the acquisition parameter.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null mode name.
%          DRV_P2INVALID - Null mode parameter.
%          DRV_OA_INVALID_STRING_LENGTH - One or more of the string parameters has an invalid length, i.e. > 255.
%          DRV_OA_MODE_DOES_NOT_EXIST - The Mode does not exist.
% REMARKS : C++ Equiv : unsigned int OA_SetInt(const char * pcModeName, const char * pcModeParam, const int iintValue);
%
% SEE ALSO : OA_GetInt OA_EnableMode OA_WriteToFile 
[ret] = atmcdmex('OA_SetInt', pcModeName, pcModeParam, iintValue);

function [ret, puiDelay, puiWidth] = GetDDGExternalOutputTime(uiIndex)
% GetDDGExternalOutputTime This function can be used to find the actual timings for a particular external output.
%
% SYNOPSIS : [ret, puiDelay, puiWidth] = GetDDGExternalOutputTime(uiIndex)
%
% INPUT uiIndex: index of external output.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timings returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - Invalid external output index.
%          DRV_P3INVALID - Delay has invalid memory address.
%        puiDelay: actual external output delay time in picoseconds.
%        puiWidth: actual external output width time in picoseconds.
% REMARKS : C++ Equiv : unsigned int GetDDGExternalOutputTime(at_u32 uiIndex, at_u64 * puiDelay, at_u64 * puiWidth);
%
% SEE ALSO : GetCapabilities GetDDGExternalOutputEnabled SetDDGExternalOutputTime SetDDGGateStep 
[ret, puiDelay, puiWidth] = atmcdmex('GetDDGExternalOutputTime', uiIndex);

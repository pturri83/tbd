function [ret] = SaveToClipBoard(palette)
% SaveToClipBoard THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SaveToClipBoard(palette)
%
% INPUT palette: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SaveToClipBoard(char * palette);
%
% SEE ALSO : 
[ret] = atmcdmex('SaveToClipBoard', palette);

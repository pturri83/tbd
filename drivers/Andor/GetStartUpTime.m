function [ret, time] = GetStartUpTime()
% GetStartUpTime THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, time] = GetStartUpTime()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        time: 
% REMARKS : C++ Equiv : unsigned int GetStartUpTime(float * time);
%
% SEE ALSO : 
[ret, time] = atmcdmex('GetStartUpTime');

function [ret] = I2CWrite(deviceID, intAddress, data)
% I2CWrite This function will write a single byte to the chosen device.
%
% SYNOPSIS : [ret] = I2CWrite(deviceID, intAddress, data)
%
% INPUT deviceID: The device to write to.
%       intAddress: The internal address of the device to write to.
%       data: The byte to be written to the device.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Write successful.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_INIERROR - Unable to load DETECTOR.INI.
%          DRV_COFERROR - Unable to load *.COF.
%          DRV_FLEXERROR - Unable to load *.RBF.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_I2CDEVNOTFOUND - Could not find the specified device.
%          DRV_I2CTIMEOUT - Timed out reading from device.
%          DRV_UNKNOWN_FUNC - Unknown function, incorrect cof file.
% REMARKS : C++ Equiv : unsigned int I2CWrite(BYTE deviceID, BYTE intAddress, BYTE data);
%
% SEE ALSO : I2CBurstWrite I2CBurstRead I2CRead I2CReset 
[ret] = atmcdmex('I2CWrite', deviceID, intAddress, data);

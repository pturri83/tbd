function [ret] = SetNumberKinetics(number)
% SetNumberKinetics This function will set the number of scans (possibly accumulated scans) to be taken during a single acquisition sequence. This will only take effect if the acquisition mode is Kinetic Series.
%
% SYNOPSIS : [ret] = SetNumberKinetics(number)
%
% INPUT number: number of scans to store
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Series length set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Number in series invalid.
% REMARKS : C++ Equiv : unsigned int SetNumberKinetics(int number);
%
% SEE ALSO : GetAcquisitionTimings SetAccumulationCycleTime SetAcquisitionMode SetExposureTime SetKineticCycleTime 
[ret] = atmcdmex('SetNumberKinetics', number);

function [ret, arr] = GetNewData8(size)
% GetNewData8 Deprecated see Note:
% 8-bit version of the GetNewDataGetNewData function. This function will return the data in the lower 8 bits of the acquired data.
%
% SYNOPSIS : [ret, arr] = GetNewData8(size)
%
% INPUT size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data copied.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Array size is incorrect.
%          DRV_NO_NEW_DATA - There is no new data yet.
%        arr: data storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int GetNewData8(unsigned char * arr, long size);
%
% SEE ALSO : 
[ret, arr] = atmcdmex('GetNewData8', size);

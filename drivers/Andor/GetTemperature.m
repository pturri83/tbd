function [ret, temperature] = GetTemperature()
% GetTemperature This function returns the temperature of the detector to the nearest degree. It also gives the status of cooling process.
%
% SYNOPSIS : [ret, temperature] = GetTemperature()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_TEMP_OFF - Temperature is OFF.
%          DRV_TEMP_STABILIZED - Temperature has stabilized at set point.
%          DRV_TEMP_NOT_REACHED - Temperature has not reached set point.
%          DRV_TEMP_DRIFT - Temperature had stabilized but has since drifted
%          DRV_TEMP_NOT_STABILIZED - Temperature reached but not stabilized
%        temperature: temperature of the detector
% REMARKS : C++ Equiv : unsigned int GetTemperature(int * temperature);
%
% SEE ALSO : GetTemperatureF SetTemperature CoolerON CoolerOFF GetTemperatureRange 
[ret, temperature] = atmcdmex('GetTemperature');

function [ret] = SaveAsTiff(path, palette, position, typ)
% SaveAsTiff This function saves the last acquisition as a tiff file, which can be loaded into an imaging package. The palette parameter specifies the location of a .PAL file, which describes the colors to use in the tiff. This file consists of 256 lines of ASCII text; each line containing three numbers separated by spaces indicating the red, green and blue component of the respective color value.
% The parameter position can be changed to export different scans in a kinetic series. If the acquisition is any other mode, position should be set to 1. The parameter typ can be set to 0, 1 or 2 which correspond to 8-bit, 16-bit and color, respectively
%
% SYNOPSIS : [ret] = SaveAsTiff(path, palette, position, typ)
%
% INPUT path: The filename of the tiff.
%       palette: The filename of a palette file (.PAL) for applying color to the tiff.
%       position: The number in the series, should be 1 for a single scan.
%       typ: The type of tiff file to create.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data successfully saved as tiff.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Path invalid.
%          DRV_P2INVALID - Invalid palette file
%          DRV_P3INVALID - position out of range
%          DRV_P4INVALID - type not valid
%          DRV_ERROR_PAGELOCK - File too large to be generated in memory.
% REMARKS : C++ Equiv : unsigned int SaveAsTiff(char * path, char * palette, int position, int typ);
%
% SEE ALSO : SaveAsSif SaveAsEDF SaveAsFITS SaveAsRaw SaveAsSPC SaveAsBmp SaveAsTiffEx SaveAsBmp 
[ret] = atmcdmex('SaveAsTiff', path, palette, position, typ);

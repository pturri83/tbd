function [ret] = EnableKeepCleans(mode)
% EnableKeepCleans This function is only available on certain cameras operating in FVB external trigger mode.  It determines if the camera keep clean cycle will run between acquisitions.
% When keep cleans are disabled in this way the exposure time is effectively the exposure time between triggers.
% The Keep Clean cycle is enabled by default.
% The feature capability AC_FEATURES_KEEPCLEANCONTROL determines if this function can be called for the camera.
%
% SYNOPSIS : [ret] = EnableKeepCleans(mode)
%
% INPUT mode: The keep clean mode.
%         0 - OFF
%         1 - ON
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Keep clean cycle mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_AVAILABLE - Feature not available.
% REMARKS : C++ Equiv : unsigned int EnableKeepCleans(int mode);
%
% SEE ALSO : GetCapabilities 
[ret] = atmcdmex('EnableKeepCleans', mode);

function [ret, puiNumberOfParams] = OA_GetNumberOfAcqParams(pcModeName)
% OA_GetNumberOfAcqParams This function will return the parameters associated with a specified mode.  The mode must be present in either the Preset file or the User defined file.
%
% SYNOPSIS : [ret, puiNumberOfParams] = OA_GetNumberOfAcqParams(pcModeName)
%
% INPUT pcModeName: The mode to search for a list of acquisition parameters.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null mode name.
%          DRV_P2INVALID - Null number of parameters.
%          DRV_OA_NULL_ERROR - Invalid pointer.
%        puiNumberOfParams: The number of acquisition parameters for the specified mode.
% REMARKS : C++ Equiv : unsigned int OA_GetNumberOfAcqParams(const char * pcModeName, unsigned int * puiNumberOfParams);
%
% SEE ALSO : OA_GetModeAcqParams 
[ret, puiNumberOfParams] = atmcdmex('OA_GetNumberOfAcqParams', pcModeName);

function [ret, iNumber] = GetNumberIO()
% GetNumberIO Available in some systems are a number of IOs that can be configured to be inputs or outputs. This function gets the number of these IOs available. The functions GetIODirection, GetIOLevel, SetIODirection and SetIOLevel can be used to specify the configuration.
%
% SYNOPSIS : [ret, iNumber] = GetNumberIO()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of  IOs returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid parameter.
%          DRV_NOT_AVAILABLE - Feature not available.
%        iNumber: number of allowed IOs
% REMARKS : C++ Equiv : unsigned int GetNumberIO(int * iNumber);
%
% SEE ALSO : GetIOLevel GetIODirection SetIODirection SetIOLevel 
[ret, iNumber] = atmcdmex('GetNumberIO');

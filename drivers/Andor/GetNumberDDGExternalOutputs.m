function [ret, puiCount] = GetNumberDDGExternalOutputs()
% GetNumberDDGExternalOutputs This function gets the number of available external outputs.
%
% SYNOPSIS : [ret, puiCount] = GetNumberDDGExternalOutputs()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%        puiCount: number of available external outputs.
% REMARKS : C++ Equiv : unsigned int GetNumberDDGExternalOutputs(at_u32 * puiCount);
%
% SEE ALSO : GetCapabilities SetDDGExternalOutputEnabled SetDDGGateStep 
[ret, puiCount] = atmcdmex('GetNumberDDGExternalOutputs');

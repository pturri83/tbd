function [ret, number] = GetMinimumNumberInSeries()
% GetMinimumNumberInSeries THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, number] = GetMinimumNumberInSeries()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        number: 
% REMARKS : C++ Equiv : unsigned int GetMinimumNumberInSeries(int * number);
%
% SEE ALSO : 
[ret, number] = atmcdmex('GetMinimumNumberInSeries');

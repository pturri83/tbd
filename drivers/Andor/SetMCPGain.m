function [ret] = SetMCPGain(gain)
% SetMCPGain Allows the user to control the voltage across the microchannel plate. Increasing the gain increases the voltage and so amplifies the signal. The gain range can be returned using GetMCPGainRange.
%
% SYNOPSIS : [ret] = SetMCPGain(gain)
%
% INPUT gain: amount of gain applied.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Value for gain accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with device.
%          DRV_P1INVALID - Gain value invalid.
% REMARKS : C++ Equiv : unsigned int SetMCPGain(int gain);
%
% SEE ALSO : GetMCPGainRange SetGateMode SetMCPGating 
[ret] = atmcdmex('SetMCPGain', gain);

function [ret] = SetCoolerMode(mode)
% SetCoolerMode This function determines whether the cooler is switched off when the camera is shut down.
%
% SYNOPSIS : [ret] = SetCoolerMode(mode)
%
% INPUT mode: 
%         0 - Returns to ambient temperature on ShutDown
%         1 - Temperature is maintained on ShutDown
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - State parameter was not zero or one.
%          DRV_NOT_SUPPORTED - Camera does not support
% REMARKS : C++ Equiv : unsigned int SetCoolerMode(int mode);
%
% SEE ALSO : 
[ret] = atmcdmex('SetCoolerMode', mode);

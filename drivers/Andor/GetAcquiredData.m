function [ret, arr] = GetAcquiredData(size)
% GetAcquiredData This function will return the data from the last acquisition. The data are returned as long integers (32-bit signed integers). The array must be large enough to hold the complete data set.
%
% SYNOPSIS : [ret, arr] = GetAcquiredData(size)
%
% INPUT size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data copied.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Array size is incorrect.
%          DRV_NO_NEW_DATA - No acquisition has taken place
%        arr: data storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int GetAcquiredData(at_32 * arr, unsigned long size);
%
% SEE ALSO : GetStatus StartAcquisition GetAcquiredData16 
[ret, arr] = atmcdmex('GetAcquiredData', size);

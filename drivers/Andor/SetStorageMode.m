function [ret] = SetStorageMode(mode)
% SetStorageMode THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetStorageMode(mode)
%
% INPUT mode: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetStorageMode(long mode);
%
% SEE ALSO : 
[ret] = atmcdmex('SetStorageMode', mode);

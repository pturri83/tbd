function [ret] = SetCurrentCamera(cameraHandle)
% SetCurrentCamera When multiple Andor cameras are installed this function allows the user to select which camera is currently active.  Once a camera has been selected the other functions can be called as normal but they will only apply to the selected camera.  If only 1 camera is installed calling this function is not required since that camera will be selected by default.
%
% SYNOPSIS : [ret] = SetCurrentCamera(cameraHandle)
%
% INPUT cameraHandle: Selects the active camera
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Camera successfully selected.
%          DRV_P1INVALID - Invalid camera handle.
% REMARKS : C++ Equiv : unsigned int SetCurrentCamera(long cameraHandle);
%
% SEE ALSO : GetCurrentCamera GetAvailableCameras GetCameraHandle 
[ret] = atmcdmex('SetCurrentCamera', cameraHandle);

function [ret] = SetIODirection(index, iDirection)
% SetIODirection Available in some systems are a number of IOs that can be configured to be inputs or outputs. This function sets the current state of a particular IO.
%
% SYNOPSIS : [ret] = SetIODirection(index, iDirection)
%
% INPUT index: IO index
%         0 - to GetNumberIO() - 1
%       iDirection: requested direction for this index.
%         0 - 0 Output
%         1 - 1 Input
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IO direction set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid index.
%          DRV_P2INVALID - Invalid direction.
%          DRV_NOT_AVAILABLE - Feature not available.
% REMARKS : C++ Equiv : unsigned int SetIODirection(int index, int iDirection);
%
% SEE ALSO : GetNumberIO GetIOLevel GetIODirection SetIOLevel 
[ret] = atmcdmex('SetIODirection', index, iDirection);

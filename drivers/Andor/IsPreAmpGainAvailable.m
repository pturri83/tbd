function [ret, status] = IsPreAmpGainAvailable(channel, amplifier, index, pa)
% IsPreAmpGainAvailable This function checks that the AD channel exists, and that the amplifier, speed and gain are available for the AD channel.
%
% SYNOPSIS : [ret, status] = IsPreAmpGainAvailable(channel, amplifier, index, pa)
%
% INPUT channel: AD channel index.
%       amplifier: Type of output amplifier.
%       index: Channel speed index.
%       pa: PreAmpGain index.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - PreAmpGain status returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid channel.
%          DRV_P2INVALID - Invalid amplifier.
%          DRV_P3INVALID - Invalid speed index.
%          DRV_P4INVALID - Invalid gain.
%        status: PreAmpGain Status
%          0 - PreAmpGain not available.
%          1 - PreAmpGain available.
% REMARKS : C++ Equiv : unsigned int IsPreAmpGainAvailable(int channel, int amplifier, int index, int pa, int * status);
%
% SEE ALSO : GetNumberPreAmpGains GetPreAmpGain SetPreAmpGain 
[ret, status] = atmcdmex('IsPreAmpGainAvailable', channel, amplifier, index, pa);

function [ret] = SetHSSpeed(typ, index)
% SetHSSpeed This function will set the speed at which the pixels are shifted into the output node during the readout phase of an acquisition. Typically your camera will be capable of operating at several horizontal shift speeds. To get the actual speed that an index corresponds to use the GetHSSpeed function. Ensure the desired A/D channel has been set with SetADChannel before calling SetHSSpeed.
%
% SYNOPSIS : [ret] = SetHSSpeed(typ, index)
%
% INPUT typ: output amplification.
%         0 - electron multiplication/Conventional(clara).
%         1 - conventional/Extended NIR mode(clara).
%       index: the horizontal speed to be used
%         0 - to GetNumberHSSpeeds()GetNumberHSSpeeds-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Horizontal speed set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Mode is invalid.
%          DRV_P2INVALID - Index is out off range.
% REMARKS : C++ Equiv : unsigned int SetHSSpeed(int typ, int index);
%
% SEE ALSO : GetNumberHSSpeeds GetHSSpeed GetNumberAmp 
[ret] = atmcdmex('SetHSSpeed', typ, index);

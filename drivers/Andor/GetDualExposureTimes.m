function [ret, exposure1, exposure2] = GetDualExposureTimes()
% GetDualExposureTimes This function will return the current valid acquisition timing information for dual exposure mode.  This mode is only available for certain sensors in run till abort mode, external trigger, full image.
%
% SYNOPSIS : [ret, exposure1, exposure2] = GetDualExposureTimes()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized. .
%          DRV_NOT_SUPPORTED - Dual exposure mode not supported on this camera.
%          DRV_NOT_AVAILABLE - Dual exposure mode not configured correctly.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - exposure1 has invalid memory address.
%          DRV_P2INVALID - exposure2 has invalid memory address.
%        exposure1: valid exposure time in seconds for each odd numbered frame.
%        exposure2: valid exposure time in seconds for each even numbered frame.
% REMARKS : C++ Equiv : unsigned int GetDualExposureTimes(float * exposure1, float * exposure2);
%
% SEE ALSO : GetCapabilities SetDualExposureMode SetDualExposureTimes 
[ret, exposure1, exposure2] = atmcdmex('GetDualExposureTimes');

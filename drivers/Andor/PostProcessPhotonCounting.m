function [ret, pInputImage, pOutputImage, pfThreshold] = PostProcessPhotonCounting(iOutputBufferSize, iNumImages, iNumframes, iNumberOfThresholds, iHeight, iWidth)
% PostProcessPhotonCounting This function will convert the input image data to photons and return the processed image in the output buffer.
%
% SYNOPSIS : [ret, pInputImage, pOutputImage, pfThreshold] = PostProcessPhotonCounting(iOutputBufferSize, iNumImages, iNumframes, iNumberOfThresholds, iHeight, iWidth)
%
% INPUT iOutputBufferSize: The number of images if a kinetic series is supplied as the input
%         data - data
%       iNumImages: The number of frames per output image.
%       iNumframes: The number of thresholds provided by the user.
%       iNumberOfThresholds: The Thresholds used to define a photon.
%       iHeight: The width of the image.
%       iWidth: 
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS DRV_NOT_INITIALIZED - Acquisition prepared.
%          DRV_ACQUIRING - System not initialized.
%          DRV_P1INVALID - Acquisition in progress.
%          DRV_P2INVALID - Invalid pointer (i.e. NULL).
%          DRV_P4INVALID - Invalid pointer (i.e. NULL).
%          DRV_P5INVALID - Number of images less than zero.
%          DRV_P6INVALID - Invalid Number of Frames requested.
%          DRV_P7INVALID - Invalid number of thresholds.
%          DRV_P8INVALID - Invalid pointer (i.e. NULL).
%          DRV_P9INVALID - Height less than zero.
%          DRV_ERROR_BUFFSIZE - Width less than zero.
%        pInputImage: The input image data to be processed.
%          at32 - * pOutputImage:	The output buffer to return the processed image.
%        pOutputImage: The size of the output buffer.
%        pfThreshold: The height of the image.
% REMARKS : C++ Equiv : unsigned int PostProcessPhotonCounting(at_32 * pInputImage, at_32 * pOutputImage, int iOutputBufferSize, int iNumImages, int iNumframes, int iNumberOfThresholds, float * pfThreshold, int iHeight, int iWidth);
%
% SEE ALSO : 
[ret, pInputImage, pOutputImage, pfThreshold] = atmcdmex('PostProcessPhotonCounting', iOutputBufferSize, iNumImages, iNumframes, iNumberOfThresholds, iHeight, iWidth);

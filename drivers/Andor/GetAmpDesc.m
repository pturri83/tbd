function [ret, name] = GetAmpDesc(index, length)
% GetAmpDesc This function will return a string with an amplifier description. The amplifier is selected using the index. The SDK has a string associated with each of its amplifiers. The maximum number of characters needed to store the amplifier descriptions is 21. The user has to specify the number of characters they wish to have returned to them from this function.
%
% SYNOPSIS : [ret, name] = GetAmpDesc(index, length)
%
% INPUT index: The amplifier index.
%       length: The length of the user allocated character array.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Description returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - The amplifier index is not valid.
%          DRV_P2INVALID - The desc pointer is null.
%          DRV_P3INVALID - The length parameter is invalid (less than 1)
%        name: A user allocated array of characters for storage of the description.
% REMARKS : C++ Equiv : unsigned int GetAmpDesc(int index, char * name, int length);
%
% SEE ALSO : GetNumberAmp 
[ret, name] = atmcdmex('GetAmpDesc', index, length);

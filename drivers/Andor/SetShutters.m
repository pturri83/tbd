function [ret] = SetShutters(typ, mode, closingtime, openingtime, exttype, extmode, dummy1, dummy2)
% SetShutters THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetShutters(typ, mode, closingtime, openingtime, exttype, extmode, dummy1, dummy2)
%
% INPUT typ: 
%       mode: 
%       closingtime: 
%       openingtime: 
%       exttype: 
%       extmode: 
%       dummy1: 
%       dummy2: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetShutters(int typ, int mode, int closingtime, int openingtime, int exttype, int extmode, int dummy1, int dummy2);
%
% SEE ALSO : 
[ret] = atmcdmex('SetShutters', typ, mode, closingtime, openingtime, exttype, extmode, dummy1, dummy2);

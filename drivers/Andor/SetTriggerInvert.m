function [ret] = SetTriggerInvert(mode)
% SetTriggerInvert This function will set whether an acquisition will be triggered on a rising or falling edge external trigger.
%
% SYNOPSIS : [ret] = SetTriggerInvert(mode)
%
% INPUT mode: trigger mode
%         0 - Rising Edge
%         1 - Falling Edge
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Trigger mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Trigger mode invalid.
%          DRV_NOT_AVAILABLE - Feature not available.
% REMARKS : C++ Equiv : unsigned int SetTriggerInvert(int mode);
%
% SEE ALSO : Trigger Modes SetTriggerMode SetFastExtTrigger 
[ret] = atmcdmex('SetTriggerInvert', mode);

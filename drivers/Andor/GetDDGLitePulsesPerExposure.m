function [ret, ui32Pulses] = GetDDGLitePulsesPerExposure(channel)
% GetDDGLitePulsesPerExposure THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, ui32Pulses] = GetDDGLitePulsesPerExposure(channel)
%
% INPUT channel: 
% OUTPUT ret: Return Code: 
%        ui32Pulses: 
% REMARKS : C++ Equiv : unsigned int GetDDGLitePulsesPerExposure(AT_DDGLiteChannelId channel, at_u32 * ui32Pulses);
%
% SEE ALSO : 
[ret, ui32Pulses] = atmcdmex('GetDDGLitePulsesPerExposure', channel);

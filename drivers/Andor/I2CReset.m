function [ret] = I2CReset()
% I2CReset This function will reset the I2C data bus.
%
% SYNOPSIS : [ret] = I2CReset()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Reset successful.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_INIERROR - Unable to load DETECTOR.INI.
%          DRV_COFERROR - Unable to load *.COF.
%          DRV_FLEXERROR - Unable to load *.RBF.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_I2CTIMEOUT - Timed out reading from device.
%          DRV_UNKNOWN_FUNC - Unknown function, incorrect cof file.
% REMARKS : C++ Equiv : unsigned int I2CReset(void);
%
% SEE ALSO : I2CBurstWrite I2CBurstRead I2CWrite 
[ret] = atmcdmex('I2CReset');

function [ret, control] = GetDDGLiteGlobalControlByte()
% GetDDGLiteGlobalControlByte THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, control] = GetDDGLiteGlobalControlByte()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        control: 
% REMARKS : C++ Equiv : unsigned int GetDDGLiteGlobalControlByte(unsigned char * control);
%
% SEE ALSO : 
[ret, control] = atmcdmex('GetDDGLiteGlobalControlByte');

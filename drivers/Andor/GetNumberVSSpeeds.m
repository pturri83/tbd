function [ret, speeds] = GetNumberVSSpeeds()
% GetNumberVSSpeeds 
%
% SYNOPSIS : [ret, speeds] = GetNumberVSSpeeds()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        speeds: 
% REMARKS : C++ Equiv : unsigned int GetNumberVSSpeeds(int * speeds);
%
% SEE ALSO : 
[ret, speeds] = atmcdmex('GetNumberVSSpeeds');

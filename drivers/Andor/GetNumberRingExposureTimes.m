function [ret, ipnumTimes] = GetNumberRingExposureTimes()
% GetNumberRingExposureTimes Gets the number of exposures in the ring at this moment.
%
% SYNOPSIS : [ret, ipnumTimes] = GetNumberRingExposureTimes()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Success
%          DRV_NOT_INITIALIZED - System not initialized
%        ipnumTimes: Numberof exposure times.
% REMARKS : C++ Equiv : unsigned int GetNumberRingExposureTimes(int * ipnumTimes);
%
% SEE ALSO : SetRingExposureTimes 
[ret, ipnumTimes] = atmcdmex('GetNumberRingExposureTimes');

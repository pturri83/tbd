function [ret] = SetImage(hbin, vbin, hstart, hend, vstart, vend)
% SetImage This function will set the horizontal and vertical binning to be used when taking a full resolution image.
%
% SYNOPSIS : [ret] = SetImage(hbin, vbin, hstart, hend, vstart, vend)
%
% INPUT hbin: number of pixels to bin horizontally.
%       vbin: number of pixels to bin vertically.
%       hstart: Start column (inclusive).
%       hend: End column (inclusive).
%       vstart: Start row (inclusive).
%       vend: End row (inclusive).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Binning parameters invalid.
%          DRV_P2INVALID - Binning parameters invalid.
%          DRV_P3INVALID - Sub-area co-ordinate is invalid.
%          DRV_P4INVALID - Sub-area co-ordinate is invalid.
%          DRV_P5INVALID - Sub-area co-ordinate is invalid.
%          DRV_P6INVALID - Sub-area co-ordinate is invalid.
% REMARKS : C++ Equiv : unsigned int SetImage(int hbin, int vbin, int hstart, int hend, int vstart, int vend);
%
% SEE ALSO : SetReadMode 
[ret] = atmcdmex('SetImage', hbin, vbin, hstart, hend, vstart, vend);

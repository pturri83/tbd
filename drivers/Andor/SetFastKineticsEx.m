function [ret] = SetFastKineticsEx(exposedRows, seriesLength, time, mode, hbin, vbin, offset)
% SetFastKineticsEx This function is the same as SetFastKinetics with the addition of an Offset parameter, which will inform the SDK of the first row to be used.
%
% SYNOPSIS : [ret] = SetFastKineticsEx(exposedRows, seriesLength, time, mode, hbin, vbin, offset)
%
% INPUT exposedRows: sub-area height in rows.
%       seriesLength: number in series.
%       time: exposure time in seconds.
%       mode: binning mode (0 - FVB , 4 - Image).
%       hbin: horizontal binning.
%       vbin: vertical binning (only used when in image mode).
%       offset: offset of first row to be used in Fast Kinetics from the bottom of the CCD.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid height.
%          DRV_P2INVALID - Invalid number in series.
%          DRV_P3INVALID - Exposure time must be greater than 0.
%          DRV_P4INVALID - Mode must be equal to 0 or 4.
%          DRV_P5INVALID - Horizontal binning.
%          DRV_P6INVALID - Vertical binning.
%          DRV_P7INVALID - Offset not within CCD limits
% REMARKS : C++ Equiv : unsigned int SetFastKineticsEx(int exposedRows, int seriesLength, float time, int mode, int hbin, int vbin, int offset);
%
% SEE ALSO : SetFKVShiftSpeed SetFastKinetics SetFKVShiftSpeed 
[ret] = atmcdmex('SetFastKineticsEx', exposedRows, seriesLength, time, mode, hbin, vbin, offset);

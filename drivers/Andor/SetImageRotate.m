function [ret] = SetImageRotate(iRotate)
% SetImageRotate This function will cause data output from the SDK to be rotated on one or both axes. This rotate is not done in the camera, it occurs after the data is retrieved and will increase processing overhead. If the rotation could be implemented by the user more efficiently then use of this function is not recomended. E.g writing to file or displaying on screen.
%
% SYNOPSIS : [ret] = SetImageRotate(iRotate)
%
% INPUT iRotate: Rotation setting
%         0 - No rotation.
%         1 - Rotate 90 degrees clockwise.
%         2 - Rotate 90 degrees anti-clockwise.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Rotate parameter invalid.
% REMARKS : C++ Equiv : unsigned int SetImageRotate(int iRotate);
%
% SEE ALSO : SetImageFlip 
[ret] = atmcdmex('SetImageRotate', iRotate);

function [ret, mode] = Filter_GetDataAveragingMode()
% Filter_GetDataAveragingMode Returns the current averaging mode.
%
% SYNOPSIS : [ret, mode] = Filter_GetDataAveragingMode()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Averaging mode returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid threshold (i.e. NULL pointer).
%        mode: The current averaging mode.
% REMARKS : C++ Equiv : unsigned int Filter_GetDataAveragingMode(int * mode);
%
% SEE ALSO : Filter_SetDataAveragingMode 
[ret, mode] = atmcdmex('Filter_GetDataAveragingMode');

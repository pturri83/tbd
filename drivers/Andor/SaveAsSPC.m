function [ret] = SaveAsSPC(path)
% SaveAsSPC This function saves the last acquisition in the GRAMS .spc file format
%
% SYNOPSIS : [ret] = SaveAsSPC(path)
%
% INPUT path: the filename to save too.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data successfully saved.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Path invalid.
%          DRV_ERROR_PAGELOCK - File too large to be generated in memory.
% REMARKS : C++ Equiv : unsigned int SaveAsSPC(char * path);
%
% SEE ALSO : SaveAsSif SaveAsEDF SaveAsFITS SaveAsRaw SaveAsTiff SaveAsBmp 
[ret] = atmcdmex('SaveAsSPC', path);

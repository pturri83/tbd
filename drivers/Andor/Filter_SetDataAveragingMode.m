function [ret] = Filter_SetDataAveragingMode(mode)
% Filter_SetDataAveragingMode Sets the current data averaging mode.
%
% SYNOPSIS : [ret] = Filter_SetDataAveragingMode(mode)
%
% INPUT mode: The averaging  factor mode to use.
%         0 - No Averaging Filter
%         5 - Recursive Averaging Filter
%         6 - Frame Averaging Filter
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Averaging mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid mode.
% REMARKS : C++ Equiv : unsigned int Filter_SetDataAveragingMode(int mode);
%
% SEE ALSO : Filter_GetDataAveragingMode 
[ret] = atmcdmex('Filter_SetDataAveragingMode', mode);

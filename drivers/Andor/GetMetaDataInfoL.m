function [ret, TimeOfStart_wYear, TimeOfStart_wMonth, TimeOfStart_wDayOfWeek, TimeOfStart_wDay, TimeOfStart_wHour, TimeOfStart_wMinute, TimeOfStart_wSecond, TimeOfStart_wMilliseconds, pfTimeFromStart] = GetMetaDataInfo(index)
%  Legacy Wrapper for GetMetaDataInfo This function will return the time of the initial frame and the time in milliseconds of further frames from this point.
%
% SYNOPSIS : [ret, TimeOfStart_wYear, TimeOfStart_wMonth, TimeOfStart_wDayOfWeek, TimeOfStart_wDay, TimeOfStart_wHour, TimeOfStart_wMinute, TimeOfStart_wSecond, TimeOfStart_wMilliseconds, pfTimeFromStart] = GetMetaDataInfo(index)
%
% INPUT index: frame for which time is required.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timings returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_MSTIMINGS_ERROR - Invalid timing request
%        TimeOfStart: Structure with start time details.
%        pfTimeFromStart: time in milliseconds for a particular frame from time of start.
% REMARKS : C++ Equiv : unsigned int GetMetaDataInfo(SYSTEMTIME * TimeOfStart, float * pfTimeFromStart, int index);
%
% SEE ALSO : SetMetaData 
[ret, TimeOfStart, pfTimeFromStart] = atmcdmex('GetMetaDataInfo', index);
TimeOfStart_wYear = TimeOfStart.wYear;
TimeOfStart_wMonth = TimeOfStart.wMonth;
TimeOfStart_wDayOfWeek = TimeOfStart.wDayOfWeek;
TimeOfStart_wDay = TimeOfStart.wDay;
TimeOfStart_wHour = TimeOfStart.wHour;
TimeOfStart_wMinute = TimeOfStart.wMinute;
TimeOfStart_wSecond = TimeOfStart.wSecond;
TimeOfStart_wMilliseconds = TimeOfStart.wMilliseconds;

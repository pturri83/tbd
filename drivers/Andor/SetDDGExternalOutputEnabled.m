function [ret] = SetDDGExternalOutputEnabled(uiIndex, uiEnabled)
% SetDDGExternalOutputEnabled This function sets the state of a selected external output.
%
% SYNOPSIS : [ret] = SetDDGExternalOutputEnabled(uiIndex, uiEnabled)
%
% INPUT uiIndex: index of external output.
%       uiEnabled: state of external output (0 - Off,1 - On).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - Invalid external output index.
% REMARKS : C++ Equiv : unsigned int SetDDGExternalOutputEnabled(at_u32 uiIndex, at_u32 uiEnabled);
%
% SEE ALSO : GetCapabilities GetDDGExternalOutputEnabled 
[ret] = atmcdmex('SetDDGExternalOutputEnabled', uiIndex, uiEnabled);

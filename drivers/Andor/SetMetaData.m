function [ret] = SetMetaData(state)
% SetMetaData This function activates the meta data option.
%
% SYNOPSIS : [ret] = SetMetaData(state)
%
% INPUT state: ON/OFF switch for the meta data option.
%         0 - to switch meta data OFF.
%         1 - to switch meta data ON.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Meta data option accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid state.
%          DRV_NOT_AVAILABLE - Feature not available.
% REMARKS : C++ Equiv : unsigned int SetMetaData(int state);
%
% SEE ALSO : GetMetaDataInfo 
[ret] = atmcdmex('SetMetaData', state);

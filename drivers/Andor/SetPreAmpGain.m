function [ret] = SetPreAmpGain(index)
% SetPreAmpGain This function will set the pre amp gain to be used for subsequent acquisitions. The actual gain factor that will be applied can be found through a call to the GetPreAmpGain function.
% The number of Pre Amp Gains available is found by calling the GetNumberPreAmpGains function.
%
% SYNOPSIS : [ret] = SetPreAmpGain(index)
%
% INPUT index: index pre amp gain table
%         0 - to GetNumberPreAmpGainsGetNumberPreAmpGains-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Pre amp gain set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Index out of range.
% REMARKS : C++ Equiv : unsigned int SetPreAmpGain(int index);
%
% SEE ALSO : IsPreAmpGainAvailable GetNumberPreAmpGains GetPreAmpGain 
[ret] = atmcdmex('SetPreAmpGain', index);

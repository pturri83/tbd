function [ret] = SetNumberPrescans(iNumber)
% SetNumberPrescans This function will set the number of scans acquired before data is to be retrieved. This will only take effect if the acquisition mode is Kinetic Series.
%
% SYNOPSIS : [ret] = SetNumberPrescans(iNumber)
%
% INPUT iNumber: number of scans to ignore
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Prescans set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Number of prescans invalid.
% REMARKS : C++ Equiv : unsigned int SetNumberPrescans(int iNumber);
%
% SEE ALSO : GetAcquisitionTimings SetAcquisitionMode SetKineticCycleTime SetNumberKinetics 
[ret] = atmcdmex('SetNumberPrescans', iNumber);

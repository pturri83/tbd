function [ret] = SaveAsBmp(path, palette, ymin, ymax)
% SaveAsBmp This function saves the last acquisition as a bitmap file, which can be loaded into an imaging package. The palette parameter specifies the location of a .PAL file, which describes the colors to use in the bitmap. This file consists of 256 lines of ASCII text; each line containing three numbers separated by spaces indicating the red, green and blue component of the respective color value.
% The ymin and ymax parameters indicate which data values will map to the first and last colors in the palette:
% * All data values below or equal to ymin will be colored with the first color.
% * All values above or equal to ymax will be colored with the last color
% * All other palette colors will be scaled across values between these limits.
%
% SYNOPSIS : [ret] = SaveAsBmp(path, palette, ymin, ymax)
%
% INPUT path: The filename of the bitmap.
%       palette: The filename of a palette file (.PAL) for applying color to the bitmap.
%       ymin: Min data value that palette will be scaled across. If ymin = 0 and ymax = 0 the palette will scale across the full range of values.
%       ymax: Max data value that palette will be scaled across. If ymin = 0 and ymax = 0 the palette will scale across the full range of values.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data successfully saved as bitmap.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Path invalid.
%          DRV_ERROR_PAGELOCK - File too large to be generated in memory.
% REMARKS : C++ Equiv : unsigned int SaveAsBmp(const char * path, const char * palette, long ymin, long ymax);
%
% SEE ALSO : SaveAsSif SaveAsEDF SaveAsFITS SaveAsRaw SaveAsSPC SaveAsTiff 
[ret] = atmcdmex('SaveAsBmp', path, palette, ymin, ymax);

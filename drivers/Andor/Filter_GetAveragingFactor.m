function [ret, averagingFactor] = Filter_GetAveragingFactor()
% Filter_GetAveragingFactor Returns the current averaging factor value.
%
% SYNOPSIS : [ret, averagingFactor] = Filter_GetAveragingFactor()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Frame count returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid averagingFactor (i.e. NULL pointer).
%        averagingFactor: The current averaging factor value.
% REMARKS : C++ Equiv : unsigned int Filter_GetAveragingFactor(int * averagingFactor);
%
% SEE ALSO : Filter_SetAveragingFactor 
[ret, averagingFactor] = atmcdmex('Filter_GetAveragingFactor');

function [ret, data] = I2CBurstRead(i2cAddress, nBytes)
% I2CBurstRead This function will read a specified number of bytes from a chosen device attached to the I2C data bus.
%
% SYNOPSIS : [ret, data] = I2CBurstRead(i2cAddress, nBytes)
%
% INPUT i2cAddress: The address of the device to read from.
%       nBytes: The number of bytes to read from the device.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Read successful.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_INIERROR - Unable to load DETECTOR.INI.
%          DRV_COFERROR - Unable to load *.COF.
%          DRV_FLEXERROR - Unable to load *.RBF.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_I2CDEVNOTFOUND - Could not find the specified device.
%          DRV_I2CTIMEOUT - Timed out reading from device.
%          DRV_UNKNOWN_FUNC - Unknown function, incorrect cof file.
%        data: The data read from the device.
% REMARKS : C++ Equiv : unsigned int I2CBurstRead(BYTE i2cAddress, long nBytes, BYTE * data);
%
% SEE ALSO : I2CBurstWrite I2CRead I2CWrite I2CReset 
[ret, data] = atmcdmex('I2CBurstRead', i2cAddress, nBytes);

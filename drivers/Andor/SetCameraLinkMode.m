function [ret] = SetCameraLinkMode(mode)
% SetCameraLinkMode This function allows the user to enable or disable the Camera Link functionality for the camera. Enabling this functionality will start to stream all acquired data through the camera link interface.
%
% SYNOPSIS : [ret] = SetCameraLinkMode(mode)
%
% INPUT mode: Enables/Disables Camera Link mode
%         1 - Enable Camera Link
%         0 - Disable Camera Link
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ACQUIRING - Acquisition in progress
%          DRV_NOT_SUPPORTED - Camera Link not supported by this Camera
%          DRV_P1INVALID - Mode was not zero or one.
% REMARKS : C++ Equiv : unsigned int SetCameraLinkMode(int mode);
%
% SEE ALSO : 
[ret] = atmcdmex('SetCameraLinkMode', mode);

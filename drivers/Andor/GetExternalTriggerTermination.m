function [ret, puiTermination] = GetExternalTriggerTermination()
% GetExternalTriggerTermination This function can be used to get the current external trigger termination mode.
%
% SYNOPSIS : [ret, puiTermination] = GetExternalTriggerTermination()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Termination returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Trigger termination not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%        puiTermination: trigger termination option.
%          0 - 50 ohm.
%          1 - hi-Z.
% REMARKS : C++ Equiv : unsigned int GetExternalTriggerTermination(at_u32 * puiTermination);
%
% SEE ALSO : GetCapabilities SetExternalTriggerTermination 
[ret, puiTermination] = atmcdmex('GetExternalTriggerTermination');

function [ret, index] = GetTotalNumberImagesAcquired()
% GetTotalNumberImagesAcquired This function will return the total number of images acquired since the current acquisition started. If the camera is idle the value returned is the number of images acquired during the last acquisition.
%
% SYNOPSIS : [ret, index] = GetTotalNumberImagesAcquired()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of acquired images returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        index: returns the total number of images acquired since the acquisition started.
% REMARKS : C++ Equiv : unsigned int GetTotalNumberImagesAcquired(long * index);
%
% SEE ALSO : 
[ret, index] = atmcdmex('GetTotalNumberImagesAcquired');

function [ret, arr] = GetAcquiredData16(size)
% GetAcquiredData16 16-bit version of the GetAcquiredDataGetAcquiredData function. The array must be large enough to hold the complete data set.
%
% SYNOPSIS : [ret, arr] = GetAcquiredData16(size)
%
% INPUT size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data copied.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Array size isincorrect.
%          DRV_NO_NEW_DATA - No acquisition has taken place
%        arr: data storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int GetAcquiredData16(WORD * arr, unsigned long size);
%
% SEE ALSO : GetStatus StartAcquisition GetAcquiredData 
[ret, arr] = atmcdmex('GetAcquiredData16', size);

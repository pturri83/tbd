function [ret] = GetMSTimingsEnabled()
% GetMSTimingsEnabled THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = GetMSTimingsEnabled()
%
% INPUT none
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int GetMSTimingsEnabled(void);
%
% SEE ALSO : 
[ret] = atmcdmex('GetMSTimingsEnabled');

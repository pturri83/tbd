function [ret] = SetDDGStepMode(mode)
% SetDDGStepMode This function will set the current gate step mode.
%
% SYNOPSIS : [ret] = SetDDGStepMode(mode)
%
% INPUT mode: the gate step mode.
%         0 - constant.
%         1 - exponential.
%         2 - logarithmic.
%         3 - linear.
%         100 - off.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gate step mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Gate step not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - Invalid gate step mode.
% REMARKS : C++ Equiv : unsigned int SetDDGStepMode(at_u32 mode);
%
% SEE ALSO : StartAcquisition GetDDGStepMode SetDDGStepCoefficients GetDDGStepCoefficients 
[ret] = atmcdmex('SetDDGStepMode', mode);

function [ret, iintValue] = OA_GetInt(pcModeName, pcModeParam)
% OA_GetInt This function is used to get the values for integer type acquisition parameters. Values  are retrieved from memory for the specified mode name. 	
% 
%
% SYNOPSIS : [ret, iintValue] = OA_GetInt(pcModeName, pcModeParam)
%
% INPUT pcModeName: The name of the mode for which an acquisition parameter
%       pcModeParam: The name of the acquisition parameter for which a value
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null mode name.
%          DRV_P2INVALID - Null mode parameter.
%          DRV_P3INVALID - Null integer value.
%        iintValue: The buffer to return the value of the acquisition.
% REMARKS : C++ Equiv : unsigned int OA_GetInt(const char * pcModeName, const char * pcModeParam, int * iintValue);
%
% SEE ALSO : OA_SetInt 
[ret, iintValue] = atmcdmex('OA_GetInt', pcModeName, pcModeParam);

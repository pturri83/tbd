function [ret, data] = SetNextAddress(lowAdd, highAdd, length, physical)
% SetNextAddress THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, data] = SetNextAddress(lowAdd, highAdd, length, physical)
%
% INPUT lowAdd: 
%       highAdd: 
%       length: 
%       physical: 
% OUTPUT ret: Return Code: 
%        data: 
% REMARKS : C++ Equiv : unsigned int SetNextAddress(at_32 * data, long lowAdd, long highAdd, long length, long physical);
%
% SEE ALSO : 
[ret, data] = atmcdmex('SetNextAddress', lowAdd, highAdd, length, physical);

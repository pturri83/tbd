function [ret] = SetDualExposureMode(mode)
% SetDualExposureMode This function turns on and off the option to acquire 2 frames for each external trigger pulse.  This mode is only available for certain sensors in run till abort mode, external trigger, full image.
%
% SYNOPSIS : [ret] = SetDualExposureMode(mode)
%
% INPUT mode: Enables/Disables dual exposure mode
%         1 - Enable mode
%         0 - Disable mode
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Dual exposure mode not supported on this camera.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Mode parameter was not zero or one.
% REMARKS : C++ Equiv : unsigned int SetDualExposureMode(int mode);
%
% SEE ALSO : GetCapabilities SetDualExposureTimes GetDualExposureTimes 
[ret] = atmcdmex('SetDualExposureMode', mode);

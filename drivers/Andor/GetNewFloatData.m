function [ret, arr] = GetNewFloatData(size)
% GetNewFloatData THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, arr] = GetNewFloatData(size)
%
% INPUT size: 
% OUTPUT ret: Return Code: 
%        arr: 
% REMARKS : C++ Equiv : unsigned int GetNewFloatData(float * arr, long size);
%
% SEE ALSO : 
[ret, arr] = atmcdmex('GetNewFloatData', size);

function [ret, internalShutter] = IsInternalMechanicalShutter()
% IsInternalMechanicalShutter This function checks if an iXon camera has a mechanical shutter installed. 	
% 
%
% SYNOPSIS : [ret, internalShutter] = IsInternalMechanicalShutter()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Internal shutter state returned
%          DRV_NOT_AVAILABLE - Not an iXon Camera
%          DRV_P1INVALID - Parameter is NULL
%        internalShutter: Status of internal shutter
%          0 - Mechanical shutter not installed.
%          1 - Mechanical shutter installed.
% REMARKS : C++ Equiv : unsigned int IsInternalMechanicalShutter(int * internalShutter);
%
% SEE ALSO : 
[ret, internalShutter] = atmcdmex('IsInternalMechanicalShutter');

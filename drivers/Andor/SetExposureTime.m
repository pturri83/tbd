function [ret] = SetExposureTime(time)
% SetExposureTime This function will set the exposure time to the nearest valid value not less than the given value. The actual exposure time used is obtained by GetAcquisitionTimingsGetAcquisitionTimings. Please refer to SECTION 5 - ACQUISITION MODES for further information.
%
% SYNOPSIS : [ret] = SetExposureTime(time)
%
% INPUT time: the exposure time in seconds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Exposure time accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Exposure Time invalid.
% REMARKS : C++ Equiv : unsigned int SetExposureTime(float time);
%
% SEE ALSO : GetAcquisitionTimings 
[ret] = atmcdmex('SetExposureTime', time);

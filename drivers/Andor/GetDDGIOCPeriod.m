function [ret, period] = GetDDGIOCPeriod()
% GetDDGIOCPeriod This function can be used to return the actual IOC period that will be triggered. It should only be called once all the conditions of the experiment have been defined.
%
% SYNOPSIS : [ret, period] = GetDDGIOCPeriod()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IOC period returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - IOC not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - period has invalid memory address.
%        period: the period of integrate on chip pulses triggered within the fire pulse.
% REMARKS : C++ Equiv : unsigned int GetDDGIOCPeriod(at_u64 * period);
%
% SEE ALSO : GetCapabilities SetDDGIOC SetDDGIOCPeriod SetDDGIOCFrequency 
[ret, period] = atmcdmex('GetDDGIOCPeriod');

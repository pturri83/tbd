function [ret, arr] = GetRelativeImageTimes(first, last, size)
% GetRelativeImageTimes This function will return an array of the start times in nanoseconds of a user defined number of frames relative to the initial frame.
%
% SYNOPSIS : [ret, arr] = GetRelativeImageTimes(first, last, size)
%
% INPUT first: Index of first frame in array.
%       last: Index of last frame in array.
%       size: number of frames for which start time is required.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timings returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_MSTIMINGS_ERROR - Invalid timing request
%        arr: array of times in nanoseconds for each frame from time of start.
% REMARKS : C++ Equiv : unsigned int GetRelativeImageTimes(int first, int last, at_u64 * arr, int size);
%
% SEE ALSO : GetCapabilities SetMetaData 
[ret, arr] = atmcdmex('GetRelativeImageTimes', first, last, size);

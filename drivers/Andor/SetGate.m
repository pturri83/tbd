function [ret] = SetGate(delay, width, stepRenamed)
% SetGate This function sets the Gater parameters for an ICCD system. The image intensifier of the Andor ICCD acts as a shutter on nanosecond time-scales using a process known as gating.
%
% SYNOPSIS : [ret] = SetGate(delay, width, stepRenamed)
%
% INPUT delay: Sets the delay(>=0) between the T0 and C outputs on the SRS box to delay nanoseconds.
%       width: Sets the width(>=0) of the gate in nanoseconds
%       stepRenamed: Sets the amount(<>0, in nanoseconds) by which the gate position is moved in time after each scan in a kinetic series.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gater parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_GPIBERROR - Error communicating with GPIB card.
%          DRV_P1INVALID - Invalid delay
%          DRV_P2INVALID - Invalid width.
%          DRV_P3INVALID - Invalid step.
% REMARKS : C++ Equiv : unsigned int SetGate(float delay, float width, float stepRenamed);
%
% SEE ALSO : SetDelayGenerator 
[ret] = atmcdmex('SetGate', delay, width, stepRenamed);

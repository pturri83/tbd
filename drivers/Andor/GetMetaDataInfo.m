function [ret, TimeOfStart, pfTimeFromStart] = GetMetaDataInfo(index)
% GetMetaDataInfo This function will return the time of the initial frame and the time in milliseconds of further frames from this point.
%
% SYNOPSIS : [ret, TimeOfStart, pfTimeFromStart] = GetMetaDataInfo(index)
%
% INPUT index: frame for which time is required.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timings returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_MSTIMINGS_ERROR - Invalid timing request
%        TimeOfStart: Structure with start time details.
%        pfTimeFromStart: time in milliseconds for a particular frame from time of start.
% REMARKS : C++ Equiv : unsigned int GetMetaDataInfo(SYSTEMTIME * TimeOfStart, float * pfTimeFromStart, int index);
%
% SEE ALSO : SetMetaData 
[ret, TimeOfStart, pfTimeFromStart] = atmcdmex('GetMetaDataInfo', index);

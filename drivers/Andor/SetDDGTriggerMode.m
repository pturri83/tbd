function [ret] = SetDDGTriggerMode(mode)
% SetDDGTriggerMode This function will set the trigger mode of the internal delay generator to either internal or External
%
% SYNOPSIS : [ret] = SetDDGTriggerMode(mode)
%
% INPUT mode: trigger mode
%         0 - internal
%         1 - External
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Trigger mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Trigger mode invalid.
% REMARKS : C++ Equiv : unsigned int SetDDGTriggerMode(int mode);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGTriggerMode', mode);

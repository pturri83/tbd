function [ret, first, last] = GetNumberAvailableImages()
% GetNumberAvailableImages This function will return information on the number of available images in the circular buffer. This information can be used with GetImages to retrieve a series of images. If any images are overwritten in the circular buffer they no longer can be retrieved and the information returned will treat overwritten images as not available.
%
% SYNOPSIS : [ret, first, last] = GetNumberAvailableImages()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of acquired images returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ERROR_ACK - Unable to communicate with card
%          DRV_NO_NEW_DATA - There is no new data yet
%        first: returns the index of the first available image in the circular buffer.
%        last: returns the index of the last available image in the circular buffer.
% REMARKS : C++ Equiv : unsigned int GetNumberAvailableImages(at_32 * first, at_32 * last);
%
% SEE ALSO : GetImages GetImages16 GetNumberNewImages 
[ret, first, last] = atmcdmex('GetNumberAvailableImages');

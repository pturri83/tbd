function [ret] = SetVerticalSpeed(index)
% SetVerticalSpeed Deprecated see Note:
% This function will set the vertical speed to be used for subsequent acquisitions
%
% SYNOPSIS : [ret] = SetVerticalSpeed(index)
%
% INPUT index: index into the vertical speed table
%         0 - to GetNumberVerticalSpeedsGetNumberVerticalSpeeds-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Vertical speed set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Index out of range.
% REMARKS : C++ Equiv : unsigned int SetVerticalSpeed(int index); // deprecated
%
% SEE ALSO : GetNumberVerticalSpeeds GetVerticalSpeed 
[ret] = atmcdmex('SetVerticalSpeed', index);

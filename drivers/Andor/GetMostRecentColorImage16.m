function [ret, red, green, blue] = GetMostRecentColorImage16(size, algorithm)
% GetMostRecentColorImage16 For colour sensors only.
% Color version of the GetMostRecentImage16 function. The CCD is sensitive to Cyan, Yellow, Magenta and Green (CYMG). The Red, Green and Blue (RGB) are calculated and Data is stored in 3 planes/images, one for each basic color.
%
% SYNOPSIS : [ret, red, green, blue] = GetMostRecentColorImage16(size, algorithm)
%
% INPUT size: total number of pixels.
%       algorithm: algorithm used to extract the RGB from the original CYMG CCD.
%         0 - 0 basic algorithm combining Cyan, Yellow and Magenta.
%         1 - 1 algorithm combining Cyan, Yellow, Magenta and Green.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Image RGB has been copied into arrays.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Arrays size is incorrect.
%          DRV_P2INVALID - Invalid algorithm.
%          DRV_P3INVALID - Invalid red pointer (i.e. NULL)..
%          DRV_P4INVALID - Invalid green pointer (i.e. NULL)..
%          DRV_P5INVALID - Invalid bluepointer (i.e. NULL)..
%          DRV_NO_NEW_DATA - There is no new data yet.
%        red: red data storage allocated by the user.
%        green: red data storage allocated by the user.
%        blue: red data storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int GetMostRecentColorImage16(unsigned long size, int algorithm, WORD * red, WORD * green, WORD * blue);
%
% SEE ALSO : GetMostRecentImage16 DemosaicImage WhiteBalance 
[ret, red, green, blue] = atmcdmex('GetMostRecentColorImage16', size, algorithm);

function [ret, speeds] = GetNumberHSSpeeds(channel, typ)
% GetNumberHSSpeeds As your Andor SDK system is capable of operating at more than one horizontal shift speed this function will return the actual number of speeds available.
%
% SYNOPSIS : [ret, speeds] = GetNumberHSSpeeds(channel, typ)
%
% INPUT channel: the AD channel.
%       typ: output amplification.
%         0 - electron multiplication.
%         1 - conventional.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of speeds returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Invalid channel.
%          DRV_P2INVALID - Invalid horizontal read mode
%        speeds: number of allowed horizontal speeds
% REMARKS : C++ Equiv : unsigned int GetNumberHSSpeeds(int channel, int typ, int * speeds);
%
% SEE ALSO : GetHSSpeed SetHSSpeed GetNumberADChannel 
[ret, speeds] = atmcdmex('GetNumberHSSpeeds', channel, typ);

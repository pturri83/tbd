function [ret] = SetFastExtTrigger(mode)
% SetFastExtTrigger This function will enable fast external triggering. When fast external triggering is enabled the system will NOT wait until a Keep Clean cycle has been completed before accepting the next trigger. This setting will only have an effect if the trigger mode has been set to External via SetTriggerModeSetTriggerMode.
%
% SYNOPSIS : [ret] = SetFastExtTrigger(mode)
%
% INPUT mode: 0	Disabled
%         1 - Enabled
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters accepted.
% REMARKS : C++ Equiv : unsigned int SetFastExtTrigger(int mode);
%
% SEE ALSO : SetTriggerMode 
[ret] = atmcdmex('SetFastExtTrigger', mode);

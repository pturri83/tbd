function [ret] = SetDataType(typ)
% SetDataType THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetDataType(typ)
%
% INPUT typ: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDataType(int typ);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDataType', typ);

function [ret, iLevel] = GetIOLevel(index)
% GetIOLevel Available in some systems are a number of IOs that can be configured to be inputs or outputs. This function gets the current state of a particular IO.
%
% SYNOPSIS : [ret, iLevel] = GetIOLevel(index)
%
% INPUT index: IO index
%         0 - toGetNumberIO() - 1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IO level returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid index.
%          DRV_P2INVALID - Invalid parameter.
%          DRV_NOT_AVAILABLE - Feature not available.
%        iLevel: current level for this index.
%          0 - 0 Low
%          1 - 1 High
% REMARKS : C++ Equiv : unsigned int GetIOLevel(int index, int * iLevel);
%
% SEE ALSO : GetNumberIO GetIODirection SetIODirection SetIOLevel 
[ret, iLevel] = atmcdmex('GetIOLevel', index);

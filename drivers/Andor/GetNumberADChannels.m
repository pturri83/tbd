function [ret, channels] = GetNumberADChannels()
% GetNumberADChannels As your Andor SDK system may be capable of operating with more than one A-D converter, this function will tell you the number available.
%
% SYNOPSIS : [ret, channels] = GetNumberADChannels()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of channels returned.
%        channels: number of allowed channels
% REMARKS : C++ Equiv : unsigned int GetNumberADChannels(int * channels);
%
% SEE ALSO : SetADChannel 
[ret, channels] = atmcdmex('GetNumberADChannels');

function [ret] = StartAcquisition()
% StartAcquisition This function starts an acquisition. The status of the acquisition can be monitored via GetStatus().
%
% SYNOPSIS : [ret] = StartAcquisition()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition started.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_INIERROR - Error reading DETECTOR.INI.
%          DRV_ACQERROR - Acquisition settings invalid.
%          DRV_ERROR_PAGELOCK - Unable to allocate memory.
%          DRV_INVALID_FILTER - Filter not available for current acquisition.
%          DRV_BINNING_ERROR - Range not multiple of horizontal binning.
%          DRV_SPOOLSETUPERROR - Error with spool settings.
% REMARKS : C++ Equiv : unsigned int StartAcquisition(void);
%
% SEE ALSO : GetStatus GetAcquisitionTimings SetAccumulationCycleTime SetAcquisitionMode SetExposureTime SetHSSpeed SetKineticCycleTime SetMultiTrack SetNumberAccumulations SetNumberKinetics SetReadMode SetSingleTrack SetTriggerMode SetVSSpeed 
[ret] = atmcdmex('StartAcquisition');

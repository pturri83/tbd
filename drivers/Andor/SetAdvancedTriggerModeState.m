function [ret] = SetAdvancedTriggerModeState(iState)
% SetAdvancedTriggerModeState This function will set the state for the iCam functionality that some cameras are capable of. There may be some cases where we wish to prevent the software using the new functionality and just do it the way it was previously done.
%
% SYNOPSIS : [ret] = SetAdvancedTriggerModeState(iState)
%
% INPUT iState: 0: turn off iCam
%         1 - 1 Enable iCam.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State set
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_P1INVALID - state invalid
% REMARKS : C++ Equiv : unsigned int SetAdvancedTriggerModeState(int iState);
%
% SEE ALSO : iCam 
[ret] = atmcdmex('SetAdvancedTriggerModeState', iState);

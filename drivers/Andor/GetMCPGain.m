function [ret, gain] = GetMCPGain()
% GetMCPGain This function will retrieve the set value for the MCP Gain.
%
% SYNOPSIS : [ret, gain] = GetMCPGain()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Table returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ACQUIRING - Acquisition in progress
%          DRV_P1INVALID - Invalid pointer (i.e. NULL)
%          DRV_NOT_AVAILABLE - Not a USB iStar
%        gain: Returned gain value.
% REMARKS : C++ Equiv : unsigned int GetMCPGain(int * gain);
%
% SEE ALSO : SetMCPGain 
[ret, gain] = atmcdmex('GetMCPGain');

function [ret] = SetUSGenomics(width, height)
% SetUSGenomics THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetUSGenomics(width, height)
%
% INPUT width: 
%       height: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetUSGenomics(long width, long height);
%
% SEE ALSO : 
[ret] = atmcdmex('SetUSGenomics', width, height);

function [ret, iRotate] = GetImageRotate()
% GetImageRotate This function will obtain whether the acquired data output is rotated in any direction.
%
% SYNOPSIS : [ret, iRotate] = GetImageRotate()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Rotate parameter invalid.
%        iRotate: Rotation setting
%          0 - - No rotation
%          1 - - Rotate 90 degrees clockwise
%          2 - - Rotate 90 degrees anti-clockwise
% REMARKS : C++ Equiv : unsigned int GetImageRotate(int * iRotate);
%
% SEE ALSO : SetImageFlip SetImageRotate SetReadMode 
[ret, iRotate] = atmcdmex('GetImageRotate');

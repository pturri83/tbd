function [ret, images] = GetImagesPerDMA()
% GetImagesPerDMA This function will return the maximum number of images that can be transferred during a single DMA transaction.
%
% SYNOPSIS : [ret, images] = GetImagesPerDMA()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of images per DMA returned.
%        images: The maximum number of images that can be transferred during a single DMA transaction
% REMARKS : C++ Equiv : unsigned int GetImagesPerDMA(unsigned long * images);
%
% SEE ALSO : 
[ret, images] = atmcdmex('GetImagesPerDMA');

function [ret] = SetRandomTracks(numTracks, areas)
% SetRandomTracks This function will set the Random-Track parameters. The positions of the tracks are validated to ensure that the tracks are in increasing order and do not overlap. The horizontal binning is set via the SetCustomTrackHBin function. The vertical binning is set to the height of each track.
% Some cameras need to have at least 1 row in between specified tracks. Ixon+ and the USB cameras allow tracks with no gaps in between.
% Example:
% Tracks specified as 20 30 31 40 tells the SDK that the first track starts at row 20 in the CCD and finishes at row 30. The next track starts at row 31 (no gap between tracks) and ends at row 40.
%
% SYNOPSIS : [ret] = SetRandomTracks(numTracks, areas)
%
% INPUT numTracks: number tracks
%         1 - to number of vertical pixels/2
%       areas: an array of track positions. The array has the form
%         bottom1 - bottom1 top1, bottom2, top2 ... bottomN, topN
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Number of tracks invalid.
%          DRV_P2INVALID - Track positions invalid.
%          DRV_RANDOM_TRACK_ERROR - Invalid combination of tracks, out of memory or mode not available.
% REMARKS : C++ Equiv : unsigned int SetRandomTracks(int numTracks, int * areas);
%
% SEE ALSO : SetCustomTrackHBin SetReadMode StartAcquisition SetComplexImage 
[ret] = atmcdmex('SetRandomTracks', numTracks, areas);

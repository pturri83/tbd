function [ret] = SetDACOutput(iOption, iResolution, iValue)
% SetDACOutput Clara offers 2 configurable precision 16-bit DAC outputs.  This function should be used to set the required voltage.
%
% SYNOPSIS : [ret] = SetDACOutput(iOption, iResolution, iValue)
%
% INPUT iOption: DAC Output  DAC Pin 1 or 2 (1/2).
%       iResolution: resolution of DAC can be set from 2 to 16-bit in steps of 2
%       iValue: requested DAC value (for particular resolution)
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - DAC Scale option accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_AVAILABLE - Feature not available.
%          DRV_P1INVALID - DAC range value invalid.
%          DRV_P2INVALID - Resolution unavailable.
%          DRV_P3INVALID - Requested value not within DAC range.
% REMARKS : C++ Equiv : unsigned int SetDACOutput(int iOption, int iResolution, int iValue);
%
% SEE ALSO : SetDACOutputScale 
[ret] = atmcdmex('SetDACOutput', iOption, iResolution, iValue);

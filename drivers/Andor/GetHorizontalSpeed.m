function [ret, speed] = GetHorizontalSpeed(index)
% GetHorizontalSpeed Deprecated see Note:
% As your Andor system is capable of operating at more than one horizontal shift speed this function will return the actual speeds available. The value returned is in microseconds per pixel shift.
%
% SYNOPSIS : [ret, speed] = GetHorizontalSpeed(index)
%
% INPUT index: speed required, 0 to NumberSpeeds-1, where NumberSpeeds is the parameter returned by GetNumberHorizontalSpeedsGetNumberHorizontalSpeeds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Speed returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid index.
%        speed: speed in micro-seconds per pixel shift
% REMARKS : C++ Equiv : unsigned int GetHorizontalSpeed(int index, int * speed); // deprecated
%
% SEE ALSO : GetNumberHorizontalSpeeds SetHorizontalSpeed 
[ret, speed] = atmcdmex('GetHorizontalSpeed', index);

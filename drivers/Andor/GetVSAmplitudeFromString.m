function [ret, index] = GetVSAmplitudeFromString(text)
% GetVSAmplitudeFromString This Function is used to get the index of the Vertical Clock Amplitude that corresponds to the string passed in.
%
% SYNOPSIS : [ret, index] = GetVSAmplitudeFromString(text)
%
% INPUT text: String to test "Normal" , "+1" , "+2" , "+3" , "+4"
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Vertical Clock Amplitude string Index returned
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Invalid text.
%          DRV_P2INVALID - Invalid index pointer.
%        index: Returns the Index of the VSAmplitude that matches string passed in
% REMARKS : C++ Equiv : unsigned int GetVSAmplitudeFromString(char * text, int * index);
%
% SEE ALSO : GetVSAmplitudeString GetVSAmplitudeValue 
[ret, index] = atmcdmex('GetVSAmplitudeFromString', text);

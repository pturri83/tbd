function [ret] = SetKineticCycleTime(time)
% SetKineticCycleTime This function will set the kinetic cycle time to the nearest valid value not less than the given value. The actual time used is obtained by GetAcquisitionTimingsGetAcquisitionTimings. . Please refer to SECTION 5 - ACQUISITION MODES for further information.
%
% SYNOPSIS : [ret] = SetKineticCycleTime(time)
%
% INPUT time: the kinetic cycle time in seconds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Cycle time accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Time invalid.
% REMARKS : C++ Equiv : unsigned int SetKineticCycleTime(float time);
%
% SEE ALSO : SetNumberKinetics 
[ret] = atmcdmex('SetKineticCycleTime', time);

function [ret, time] = GetFKExposureTime()
% GetFKExposureTime This function will return the current "valid" exposure time for a fast kinetics acquisition. This function should be used after all the acquisitions settings have been set, i.e. SetFastKineticsSetFastKinetics and SetFKVShiftSpeedSetFKVShiftSpeed. The value returned is the actual time used in subsequent acquisitions.
%
% SYNOPSIS : [ret, time] = GetFKExposureTime()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timing information returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_INVALID_MODE - Fast kinetics is not available.
%        time: valid exposure time in seconds
% REMARKS : C++ Equiv : unsigned int GetFKExposureTime(float * time);
%
% SEE ALSO : SetFastKinetics SetFKVShiftSpeed 
[ret, time] = atmcdmex('GetFKExposureTime');

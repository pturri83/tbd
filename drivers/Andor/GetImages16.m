function [ret, arr, validfirst, validlast] = GetImages16(first, last, size)
% GetImages16 16-bit version of the GetImagesGetImages function.
%
% SYNOPSIS : [ret, arr, validfirst, validlast] = GetImages16(first, last, size)
%
% INPUT first: index of first image in buffer to retrieve.
%       last: index of last image in buffer to retrieve.
%       size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Images have been copied into array.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_GENERAL_ERRORS - The series is out of range.
%          DRV_P3INVALID - Invalid pointer (i.e. NULL).
%          DRV_P4INVALID - Array size is incorrect.
%          DRV_NO_NEW_DATA - There is no new data yet.
%        arr: data storage allocated by the user.
%        validfirst: index of the first valid image.
%        validlast: index of the last valid image.
% REMARKS : C++ Equiv : unsigned int GetImages16(long first, long last, WORD * arr, long size, long * validfirst, long * validlast);
%
% SEE ALSO : GetImages GetNumberNewImages 
[ret, arr, validfirst, validlast] = atmcdmex('GetImages16', first, last, size);

function [ret, name] = GetHeadModel()
% GetHeadModel This function will retrieve the type of CCD attached to your system.
%
% SYNOPSIS : [ret, name] = GetHeadModel()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Name returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        name: A user allocated array of characters for storage of the Head Model. This should be declared as size MAX_PATH.
% REMARKS : C++ Equiv : unsigned int GetHeadModel(char * name);
%
% SEE ALSO : 
[ret, name] = atmcdmex('GetHeadModel');

function [ret, first, last] = GetNumberNewImages()
% GetNumberNewImages This function will return information on the number of new images (i.e. images which have not yet been retrieved) in the circular buffer. This information can be used with GetImages to retrieve a series of the latest images. If any images are overwritten in the circular buffer they can no longer be retrieved and the information returned will treat overwritten images as having been retrieved.
%
% SYNOPSIS : [ret, first, last] = GetNumberNewImages()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of acquired images returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_NO_NEW_DATA - There is no new data yet.
%        first: returns the index of the first available image in the circular buffer.
%        last: returns the index of the last available image in the circular buffer.
% REMARKS : C++ Equiv : unsigned int GetNumberNewImages(long * first, long * last);
%
% SEE ALSO : GetImages GetImages16 GetNumberAvailableImages 
[ret, first, last] = atmcdmex('GetNumberNewImages');

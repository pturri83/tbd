function [ret] = SetDDGGateStep(step)
% SetDDGGateStep This function will set a constant value for the gate step in a kinetic series. The lowest available resolution is 25 picoseconds and the maximum permitted value is 25 seconds.
%
% SYNOPSIS : [ret] = SetDDGGateStep(step)
%
% INPUT step: gate step in picoseconds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gate step set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Gate step invalid.
% REMARKS : C++ Equiv : unsigned int SetDDGGateStep(double step);
%
% SEE ALSO : SetDDGTimes SetDDGVariableGateStep 
[ret] = atmcdmex('SetDDGGateStep', step);

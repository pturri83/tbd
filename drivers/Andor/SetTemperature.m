function [ret] = SetTemperature(temperature)
% SetTemperature This function will set the desired temperature of the detector. To turn the cooling ON and OFF use the CoolerONCoolerON and CoolerOFFCoolerOFF function respectively.
%
% SYNOPSIS : [ret] = SetTemperature(temperature)
%
% INPUT temperature: the temperature in Centigrade.
%         Valid - range is given by GetTemperatureRangeGetTemperatureRange
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Temperature set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Temperature invalid.
%          DRV_NOT_SUPPORTED - The camera does not support setting the temperature.
% REMARKS : C++ Equiv : unsigned int SetTemperature(int temperature);
%
% SEE ALSO : CoolerOFF CoolerON GetTemperature GetTemperatureF GetTemperatureRange 
[ret] = atmcdmex('SetTemperature', temperature);

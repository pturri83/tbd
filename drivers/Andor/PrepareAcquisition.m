function [ret] = PrepareAcquisition()
% PrepareAcquisition This function reads the current acquisition setup and allocates and configures any memory that will be used during the acquisition. The function call is not required as it will be called automatically by the StartAcquisition function if it has not already been called externally.
% However for long kinetic series acquisitions the time to allocate and configure any memory can be quite long which can result in a long delay between calling StartAcquisition and the acquisition actually commencing. For iDus, there is an additional delay caused by the camera being set-up with any new acquisition parameters. Calling PrepareAcquisition first will reduce this delay in the StartAcquisition call.
%
% SYNOPSIS : [ret] = PrepareAcquisition()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition prepared.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_INIERROR - Error reading DETECTOR.INI.
%          DRV_ACQERROR - Acquisition settings invalid.
%          DRV_ERROR_PAGELOCK - Unable to allocate memory.
%          DRV_INVALID_FILTER - Filter not available for current acquisition.
%          DRV_IOCERROR - integrate On Chip setup error.
%          DRV_BINNING_ERROR - Range not multiple of horizontal binning.
%          DRV_SPOOLSETUPERROR - Error with spool settings.
% REMARKS : C++ Equiv : unsigned int PrepareAcquisition(void);
%
% SEE ALSO : StartAcquisition FreeInternalMemory 
[ret] = atmcdmex('PrepareAcquisition');

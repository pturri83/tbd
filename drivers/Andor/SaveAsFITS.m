function [ret] = SaveAsFITS(szFileTitle, typ)
% SaveAsFITS This function saves the last acquisition in the FITS (Flexible Image Transport System) Data Format (*.fits) endorsed by NASA.
%
% SYNOPSIS : [ret] = SaveAsFITS(szFileTitle, typ)
%
% INPUT szFileTitle: the filename to save to.
%       typ: Data type
%         0 - Unsigned 16
%         1 - Unsigned 32
%         2 - Signed 16
%         3 - Signed 32
%         4 - Float
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data successfully saved.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Path invalid.
%          DRV_P2INVALID - Invalid mode
%          DRV_ERROR_PAGELOCK - File too large to be generated in memory.
% REMARKS : C++ Equiv : unsigned int SaveAsFITS(char * szFileTitle, int typ);
%
% SEE ALSO : SaveAsSif SaveAsEDF SaveAsRaw SaveAsSPC SaveAsTiff SaveAsBmp 
[ret] = atmcdmex('SaveAsFITS', szFileTitle, typ);

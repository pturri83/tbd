function [ret] = SetCountConvertMode(Mode)
% SetCountConvertMode This function configures the Count Convert mode.
%
% SYNOPSIS : [ret] = SetCountConvertMode(Mode)
%
% INPUT Mode: 
%         0 - Data in Counts
%         1 - Data in Electrons
%         2 - Data in Photons
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Count Convert mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_SUPPORTED - Count Convert not available for this camera
%          DRV_NOT_AVAILABLE - Count Convert mode not available with current settings
%          DRV_P1INVALID - Mode parameter was out of range.
% REMARKS : C++ Equiv : unsigned int SetCountConvertMode(int Mode);
%
% SEE ALSO : GetCapabilities SetCountConvertWavelength 
[ret] = atmcdmex('SetCountConvertMode', Mode);

function [ret, name] = GetPreAmpGainText(index, length)
% GetPreAmpGainText This function will return a string with a pre amp gain description. The pre amp gain is selected using the index. The SDK has a string associated with each of its pre amp gains. The maximum number of characters needed to store the pre amp gain descriptions is 30. The user has to specify the number of characters they wish to have returned to them from this function.
%
% SYNOPSIS : [ret, name] = GetPreAmpGainText(index, length)
%
% INPUT index: gain index 0 to GetNumberPreAmpGainsGetNumberPreAmpGains()-1
%       length: The length of the user allocated character array.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Description returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Invalid index.
%          DRV_P2INVALID - Array size is incorrect
%          DRV_NOT_SUPPORTED - Function not supported with this camera
%        name: nameA user allocated array of characters for storage of the description.
% REMARKS : C++ Equiv : unsigned int GetPreAmpGainText(int index, char * name, int length);
%
% SEE ALSO : IsPreAmpGainAvailable GetNumberPreAmpGains SetPreAmpGain GetCapabilities 
[ret, name] = atmcdmex('GetPreAmpGainText', index, length);

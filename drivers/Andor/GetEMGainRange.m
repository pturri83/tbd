function [ret, low, high] = GetEMGainRange()
% GetEMGainRange Returns the minimum and maximum values of the current selected EM Gain mode and temperature of the sensor.
%
% SYNOPSIS : [ret, low, high] = GetEMGainRange()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gain range returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        low: lowest gain setting
%        high: highest gain setting
% REMARKS : C++ Equiv : unsigned int GetEMGainRange(int * low, int * high);
%
% SEE ALSO : 
[ret, low, high] = atmcdmex('GetEMGainRange');

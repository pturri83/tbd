function [ret] = UnMapPhysicalAddress()
% UnMapPhysicalAddress THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = UnMapPhysicalAddress()
%
% INPUT none
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int UnMapPhysicalAddress(void);
%
% SEE ALSO : 
[ret] = atmcdmex('UnMapPhysicalAddress');

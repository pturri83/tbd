function [ret] = SetDDGExternalOutputTime(uiIndex, uiDelay, uiWidth)
% SetDDGExternalOutputTime This function can be used to set the timings for a particular external output.
%
% SYNOPSIS : [ret] = SetDDGExternalOutputTime(uiIndex, uiDelay, uiWidth)
%
% INPUT uiIndex: index of external output.
%       uiDelay: external output delay time in picoseconds.
%       uiWidth: external output width time in picoseconds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timings set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with card.
%          DRV_P2INVALID - Invalid external output index.
%          DRV_P3INVALID - Invalid delay.
% REMARKS : C++ Equiv : unsigned int SetDDGExternalOutputTime(at_u32 uiIndex, at_u64 uiDelay, at_u64 uiWidth);
%
% SEE ALSO : GetCapabilities GetDDGExternalOutputEnabled GetDDGExternalOutputTime 
[ret] = atmcdmex('SetDDGExternalOutputTime', uiIndex, uiDelay, uiWidth);

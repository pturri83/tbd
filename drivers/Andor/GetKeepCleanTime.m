function [ret, KeepCleanTime] = GetKeepCleanTime()
% GetKeepCleanTime This function will return the time to perform a keep clean cycle. This function should be used after all the acquisitions settings have been set, e.g. SetExposureTimeSetExposureTime, SetKineticCycleTimeSetKineticCycleTime and SetReadModeSetReadMode etc. The value returned is the actual times used in subsequent acquisitions.
%
% SYNOPSIS : [ret, KeepCleanTime] = GetKeepCleanTime()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timing information returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_CODES - Error communicating with camera.
%        KeepCleanTime: valid readout time in seconds
% REMARKS : C++ Equiv : unsigned int GetKeepCleanTime(float * KeepCleanTime);
%
% SEE ALSO : GetAcquisitionTimings GetReadOutTime 
[ret, KeepCleanTime] = atmcdmex('GetKeepCleanTime');

function [ret] = SetDDGExternalOutputPolarity(uiIndex, uiPolarity)
% SetDDGExternalOutputPolarity This function sets the polarity of a selected external output.
%
% SYNOPSIS : [ret] = SetDDGExternalOutputPolarity(uiIndex, uiPolarity)
%
% INPUT uiIndex: index of external output.
%       uiPolarity: polarity of external output (0 - Positive,1 - Negative).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Polarity set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - Invalid external output index.
% REMARKS : C++ Equiv : unsigned int SetDDGExternalOutputPolarity(at_u32 uiIndex, at_u32 uiPolarity);
%
% SEE ALSO : GetCapabilities GetDDGExternalOutputEnabled GetDDGExternalOutputPolarity 
[ret] = atmcdmex('SetDDGExternalOutputPolarity', uiIndex, uiPolarity);

function [ret, fptimes] = GetAdjustedRingExposureTimes(inumTimes)
% GetAdjustedRingExposureTimes This function will return the actual exposure times that the camera will use. There may be differences between requested exposures and the actual exposures.
%
% SYNOPSIS : [ret, fptimes] = GetAdjustedRingExposureTimes(inumTimes)
%
% INPUT inumTimes: inumTimesNumbers of times requested.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Success.
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_P1INVALID - Invalid number of exposures requested
%        fptimes: fptimesan array large enough to hold _inumTimes floats.
% REMARKS : C++ Equiv : unsigned int GetAdjustedRingExposureTimes(int inumTimes, float * fptimes);
%
% SEE ALSO : GetNumberRingExposureTimes SetRingExposureTimes 
[ret, fptimes] = atmcdmex('GetAdjustedRingExposureTimes', inumTimes);

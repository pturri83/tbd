function [ret, arr] = GetAcquiredFloatData(size)
% GetAcquiredFloatData THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, arr] = GetAcquiredFloatData(size)
%
% INPUT size: 
% OUTPUT ret: Return Code: 
%        arr: 
% REMARKS : C++ Equiv : unsigned int GetAcquiredFloatData(float * arr, unsigned long size);
%
% SEE ALSO : 
[ret, arr] = atmcdmex('GetAcquiredFloatData', size);

function [ret] = SendSoftwareTrigger()
% SendSoftwareTrigger This function sends an event to the camera to take an acquisition when in Software Trigger mode. Not all cameras have this mode available to them. To check if your camera can operate in this mode check the GetCapabilities function for the Trigger Mode AC_TRIGGERMODE_CONTINUOUS. If this mode is physically possible and other settings are suitable (IsTriggerModeAvailable) and the camera is acquiring then this command will take an acquisition.
%
% SYNOPSIS : [ret] = SendSoftwareTrigger()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Trigger sent
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_INVALID_MODE - Not in SoftwareTrigger mode
%          DRV_IDLE - Not Acquiring
%          DRV_ERROR_CODES - Error communicating with camera
%          DRV_ERROR_ACK - Previous acquisition not complete
% REMARKS : C++ Equiv : unsigned int SendSoftwareTrigger(void);
%
% SEE ALSO : GetCapabilities IsTriggerModeAvailable SetAcquisitionMode SetReadMode SetTriggerMode 
[ret] = atmcdmex('SendSoftwareTrigger');

function [ret] = SetPhotonCounting(state)
% SetPhotonCounting This function activates the photon counting option.
%
% SYNOPSIS : [ret] = SetPhotonCounting(state)
%
% INPUT state: ON/OFF switch for the photon counting option.
%         0 - to switch photon counting OFF.
%         1 - to switch photon counting ON.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - photon counting option accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
% REMARKS : C++ Equiv : unsigned int SetPhotonCounting(int state);
%
% SEE ALSO : SetPhotonCountingThreshold 
[ret] = atmcdmex('SetPhotonCounting', state);

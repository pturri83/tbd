function [ret, number] = GetMaximumNumberRingExposureTimes()
% GetMaximumNumberRingExposureTimes This function will return the maximum number of exposures that can be configured in the SetRingExposureTimes SDK function.
%
% SYNOPSIS : [ret, number] = GetMaximumNumberRingExposureTimes()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Success
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_P1INVALID - Invalid number value (ie NULL)
%          DRV_NOTAVAILABLE - System does not support this option
%        number: Will contain the maximum number of exposures on return.
% REMARKS : C++ Equiv : unsigned int GetMaximumNumberRingExposureTimes(int * number);
%
% SEE ALSO : GetCapabilities GetNumberRingExposureTimes GetAdjustedRingExposureTimes GetRingExposureRange IsTriggerModeAvailable SetRingExposureTimes 
[ret, number] = atmcdmex('GetMaximumNumberRingExposureTimes');

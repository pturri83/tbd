function [ret] = I2CBurstWrite(i2cAddress, nBytes, data)
% I2CBurstWrite This function will write a specified number of bytes to a chosen device attached to the I2C data bus.
%
% SYNOPSIS : [ret] = I2CBurstWrite(i2cAddress, nBytes, data)
%
% INPUT i2cAddress: The address of the device to write to.
%       nBytes: The number of bytes to write to the device.
%       data: The data to write to the device.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Write successful.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_INIERROR - Unable to load DETECTOR.INI.
%          DRV_COFERROR - Unable to load *.COF.
%          DRV_FLEXERROR - Unable to load *.RBF.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_I2CDEVNOTFOUND - Could not find the specified device.
%          DRV_I2CTIMEOUT - Timed out reading from device.
%          DRV_UNKNOWN_FUNC - Unknown function, incorrect cof file.
% REMARKS : C++ Equiv : unsigned int I2CBurstWrite(BYTE i2cAddress, long nBytes, BYTE * data);
%
% SEE ALSO : I2CBurstRead I2CRead I2CWrite I2CReset 
[ret] = atmcdmex('I2CBurstWrite', i2cAddress, nBytes, data);

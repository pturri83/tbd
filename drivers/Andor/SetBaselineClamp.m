function [ret] = SetBaselineClamp(state)
% SetBaselineClamp This function turns on and off the baseline clamp functionality. With this feature enabled the baseline level of each scan in a kinetic series will be more consistent across the sequence.
%
% SYNOPSIS : [ret] = SetBaselineClamp(state)
%
% INPUT state: Enables/Disables Baseline clamp functionality
%         1 - Enable Baseline Clamp
%         0 - Disable Baseline Clamp
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_SUPPORTED - Baseline Clamp not supported on this camera
%          DRV_P1INVALID - State parameter was not zero or one.
% REMARKS : C++ Equiv : unsigned int SetBaselineClamp(int state);
%
% SEE ALSO : 
[ret] = atmcdmex('SetBaselineClamp', state);

function [ret] = SetFPDP(state)
% SetFPDP THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetFPDP(state)
%
% INPUT state: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetFPDP(int state);
%
% SEE ALSO : 
[ret] = atmcdmex('SetFPDP', state);

function [ret] = SetDACOutputScale(iScale)
% SetDACOutputScale Clara offers 2 configurable precision 16-bit DAC outputs.  This function should be used to select the active one.
%
% SYNOPSIS : [ret] = SetDACOutputScale(iScale)
%
% INPUT iScale: 5 or 10 volt DAC range (1/2).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - DAC Scale option accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_AVAILABLE - Feature not available
%          DRV_P1INVALID - DAC Scale value invalid.
% REMARKS : C++ Equiv : unsigned int SetDACOutputScale(int iScale);
%
% SEE ALSO : SetDACOutput 
[ret] = atmcdmex('SetDACOutputScale', iScale);

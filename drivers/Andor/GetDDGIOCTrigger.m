function [ret, trigger] = GetDDGIOCTrigger()
% GetDDGIOCTrigger function can be used to retrieve the active IOC trigger.
% at_u32* trigger: active IOC trigger (0 - Fire pulse, 1 - External trigger).
% at_u32 int
% DRV_SUCCESS
% DRV_NOT_INITIALIZED
% DRV_NOT_SUPPORTED
% DRV_ACQUIRING
% DRV_ERROR_ACK
% DRV_P1INVALID
% IOC trigger returned.
% System not initialized.
% IOC not supported.
% Acquisition in progress.
% Unable to communicate with system.
% Invalid trigger.
% See also
% GetCapabilities SetDDGIOC SetDDGIOCFrequency SetDDGIOCTrigger SetTriggerMode
% GetDDGLiteControlByte
% GetDDGLiteControlByte int WINAPI GetDDGLiteControlByte(AT_DDGLiteChannelId channel, unsigned char * control)
% Description
% THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, trigger] = GetDDGIOCTrigger()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        trigger: 
% REMARKS : C++ Equiv : unsigned int GetDDGIOCTrigger(at_u32 * trigger);
%
% SEE ALSO : 
[ret, trigger] = atmcdmex('GetDDGIOCTrigger');

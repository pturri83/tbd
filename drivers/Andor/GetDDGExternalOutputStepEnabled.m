function [ret, puiEnabled] = GetDDGExternalOutputStepEnabled(uiIndex)
% GetDDGExternalOutputStepEnabled Each external output has the option to track the gate step applied to the gater.  This function can be used to determine if this option is currently active.
%
% SYNOPSIS : [ret, puiEnabled] = GetDDGExternalOutputStepEnabled(uiIndex)
%
% INPUT uiIndex: index of external output.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - Invalid external output index.
%        puiEnabled: current state of external output track step (0 - Off, 1 - On).
% REMARKS : C++ Equiv : unsigned int GetDDGExternalOutputStepEnabled(at_u32 uiIndex, at_u32 * puiEnabled);
%
% SEE ALSO : GetCapabilities GetDDGExternalOutputEnabled SetDDGExternalOutputStepEnabled SetDDGGateStep 
[ret, puiEnabled] = atmcdmex('GetDDGExternalOutputStepEnabled', uiIndex);

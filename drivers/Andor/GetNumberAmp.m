function [ret, amp] = GetNumberAmp()
% GetNumberAmp As your Andor SDK system may be capable of operating with more than one output amplifier, this function will tell you the number available.
%
% SYNOPSIS : [ret, amp] = GetNumberAmp()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of output amplifiers returned.
%        amp: number of allowed channels
% REMARKS : C++ Equiv : unsigned int GetNumberAmp(int * amp);
%
% SEE ALSO : SetOutputAmplifier 
[ret, amp] = atmcdmex('GetNumberAmp');

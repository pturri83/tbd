function [ret] = FreeInternalMemory()
% FreeInternalMemory The FreeinternalMemory function will deallocate any memory used internally to store the previously acquired data. Note that once this function has been called, data from last acquisition cannot be retrieved.
%
% SYNOPSIS : [ret] = FreeInternalMemory()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Memory freed.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
% REMARKS : C++ Equiv : unsigned int FreeInternalMemory(void);
%
% SEE ALSO : GetImages PrepareAcquisition 
[ret] = atmcdmex('FreeInternalMemory');

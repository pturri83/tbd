function [ret, temperature] = GetTemperatureF()
% GetTemperatureF This function returns the temperature in degrees of the detector. It also gives the status of cooling process.
%
% SYNOPSIS : [ret, temperature] = GetTemperatureF()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_TEMP_OFF - Temperature is OFF.
%          DRV_TEMP_STABILIZED - Temperature has stabilized at set point.
%          DRV_TEMP_NOT_REACHED - Temperature has not reached set point.
%          DRV_TEMP_DRIFT - Temperature had stabilised but has since drifted
%          DRV_TEMP_NOT_STABILIZED - Temperature reached but not stabilized
%        temperature: temperature of the detector
% REMARKS : C++ Equiv : unsigned int GetTemperatureF(float * temperature);
%
% SEE ALSO : GetTemperature SetTemperature CoolerON CoolerOFF GetTemperatureRange 
[ret, temperature] = atmcdmex('GetTemperatureF');

function [ret, number] = GetNumberVSAmplitudes()
% GetNumberVSAmplitudes This function will normally return the number of vertical clock voltage amplitues that the camera has.
%
% SYNOPSIS : [ret, number] = GetNumberVSAmplitudes()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters
%          DRV_NOT_INITIALIZED - int* speeds: number of allowed vertical speeds
%          DRV_NOT_AVAILABLE - Return
%          Number returned - Return int
%          System not initialized - DRV_SUCCESS
%          Your system does not support this feature - DRV_NOT_INITIALIZED
%          GetNumberVSSpeeds - DRV_ACQUIRING
%          GetNumberVSSpeeds int WINAPI GetNumberVSSpeeds(int* speeds) - Number of speeds returned.
%          Description - System not initialized.
%          As your Andor system may be capable of operating at more than one vertical shift speed this function will return the actual number of speeds available. - Acquisition in progress.
%        number: Number of vertical clock voltages.
% REMARKS : C++ Equiv : unsigned int GetNumberVSAmplitudes(int * number);
%
% SEE ALSO : GetVSSpeed SetVSSpeed GetFastestRecommendedVSSpeed 
[ret, number] = atmcdmex('GetNumberVSAmplitudes');

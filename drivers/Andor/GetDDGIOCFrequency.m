function [ret, frequency] = GetDDGIOCFrequency()
% GetDDGIOCFrequency This function can be used to return the actual IOC frequency that will be triggered. It should only be called once all the conditions of the experiment have been defined.
%
% SYNOPSIS : [ret, frequency] = GetDDGIOCFrequency()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ERROR_ACK - Unable to communicate with card
%        frequency: the frequency of integrate on chip pulses triggered within the fire pulse.
% REMARKS : C++ Equiv : unsigned int GetDDGIOCFrequency(double * frequency);
%
% SEE ALSO : GetCapabilities SetDDGIOCFrequency SetDDGIOCNumber GetDDGIOCNumber GetDDGIOCPulses SetDDGIOC SetDDGIOCFrequency 
[ret, frequency] = atmcdmex('GetDDGIOCFrequency');

function [ret] = Filter_SetAveragingFactor(averagingFactor)
% Filter_SetAveragingFactor Sets the averaging factor.
%
% SYNOPSIS : [ret] = Filter_SetAveragingFactor(averagingFactor)
%
% INPUT averagingFactor: The averaging factor to use.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Averaging factor set.
%          DRV_NOT_INITIALIZED DRV_ACQUIRING - System not initialized.
%          DRV_P1INVALID - Acquisition in progress.
% REMARKS : C++ Equiv : unsigned int Filter_SetAveragingFactor(int averagingFactor);
%
% SEE ALSO : Filter_GetAveragingFactor 
[ret] = atmcdmex('Filter_SetAveragingFactor', averagingFactor);

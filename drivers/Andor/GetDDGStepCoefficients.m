function [ret, p1, p2] = GetDDGStepCoefficients(mode)
% GetDDGStepCoefficients This function will return the coefficients for a particular gate step mode.
%
% SYNOPSIS : [ret, p1, p2] = GetDDGStepCoefficients(mode)
%
% INPUT mode: the gate step mode.
%         0 - constant.
%         1 - exponential.
%         2 - logarithmic.
%         3 - linear.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gate step coefficients returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Gate step not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - Gate step mode invalid.
%          DRV_P2_INVALID - p1 has invalid memory address.
%          DRV_P3_INVALID - p2 has invalid memory address.
%        p1: First coefficient
%        p2: Second coefficient
% REMARKS : C++ Equiv : unsigned int GetDDGStepCoefficients(at_u32 mode, double * p1, double * p2);
%
% SEE ALSO : StartAcquisition SetDDGStepMode SetDDGStepCoefficients 
[ret, p1, p2] = atmcdmex('GetDDGStepCoefficients', mode);

function [ret, QE] = GetQE(sensor, wavelength, mode)
% GetQE Returns the percentage QE for a particular head model at a user specified wavelengthSetPreAmpGain.
%
% SYNOPSIS : [ret, QE] = GetQE(sensor, wavelength, mode)
%
% INPUT sensor: head model
%       wavelength: wavelength at which QE is required
%       mode: Clara mode (Normal (0) or Extended NIR (1)).  0 for all other systems
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - QE returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        QE: requested QE
% REMARKS : C++ Equiv : unsigned int GetQE(char * sensor, float wavelength, int mode, float * QE);
%
% SEE ALSO : GetHeadModel IsPreAmpGainAvailable SetPreAmpGain GetCapabilities 
[ret, QE] = atmcdmex('GetQE', sensor, wavelength, mode);

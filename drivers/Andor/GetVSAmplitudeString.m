function [ret, text] = GetVSAmplitudeString(index)
% GetVSAmplitudeString This Function is used to get the Vertical Clock Amplitude string that corresponds to the index passed in.
%
% SYNOPSIS : [ret, text] = GetVSAmplitudeString(index)
%
% INPUT index: Index of VS amplitude required
%         0 - to GetNumberVSAmplitudes()-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Vertical Clock Amplitude string returned
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Invalid index.
%          DRV_P2INVALID - Invalid text pointer.
%        text: Returns string value of the VS Amplitude found at the index supplied
% REMARKS : C++ Equiv : unsigned int GetVSAmplitudeString(int index, char * text);
%
% SEE ALSO : GetVSAmplitudeFromString GetVSAmplitudeValue 
[ret, text] = atmcdmex('GetVSAmplitudeString', index);

function [ret, arr] = GetOldestImage16(size)
% GetOldestImage16 16-bit version of the GetOldestImageGetOldestImage function.
%
% SYNOPSIS : [ret, arr] = GetOldestImage16(size)
%
% INPUT size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Image has been copied into array.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Array size is incorrect.
%          DRV_NO_NEW_DATA - There is no new data yet.
%        arr: data storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int GetOldestImage16(WORD * arr, unsigned long size);
%
% SEE ALSO : GetOldestImage GetMostRecentImage16 GetMostRecentImage 
[ret, arr] = atmcdmex('GetOldestImage16', size);

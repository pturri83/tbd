function [ret, pdata] = I2CRead(deviceID, intAddress)
% I2CRead This function will read a single byte from the chosen device.
%
% SYNOPSIS : [ret, pdata] = I2CRead(deviceID, intAddress)
%
% INPUT deviceID: The device to read from.
%       intAddress: The internal address of the device to be read from.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Read successful.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_INIERROR - Unable to load DETECTOR.INI.
%          DRV_COFERROR - Unable to load *.COF.
%          DRV_FLEXERROR - Unable to load *.RBF.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_I2CDEVNOTFOUND - Could not find the specified device.
%          DRV_I2CTIMEOUT - Timed out reading from device.
%          DRV_UNKNOWN_FUNC - Unknown function, incorrect cof file.
%        pdata: The byte read from the device.
% REMARKS : C++ Equiv : unsigned int I2CRead(BYTE deviceID, BYTE intAddress, BYTE * pdata);
%
% SEE ALSO : I2CBurstWrite I2CBurstRead I2CWrite I2CReset 
[ret, pdata] = atmcdmex('I2CRead', deviceID, intAddress);

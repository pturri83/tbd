function [ret] = SetEMClockCompensation(EMClockCompensationFlag)
% SetEMClockCompensation THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetEMClockCompensation(EMClockCompensationFlag)
%
% INPUT EMClockCompensationFlag: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetEMClockCompensation(int EMClockCompensationFlag);
%
% SEE ALSO : 
[ret] = atmcdmex('SetEMClockCompensation', EMClockCompensationFlag);

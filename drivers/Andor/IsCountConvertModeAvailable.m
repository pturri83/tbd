function [ret] = IsCountConvertModeAvailable(mode)
% IsCountConvertModeAvailable This function checks if the hardware and current settings permit the use of the specified Count Convert mode.
%
% SYNOPSIS : [ret] = IsCountConvertModeAvailable(mode)
%
% INPUT mode: Count Convert mode to be checked
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Count Convert mode available.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Count Convert not supported on this camera
%          DRV_INVALID_COUNTCONVERT_MODE - Count Convert mode not available with current acquisition settings
% REMARKS : C++ Equiv : unsigned int IsCountConvertModeAvailable(int mode);
%
% SEE ALSO : GetCapabilities SetCountConvertMode SetCountConvertWavelength 
[ret] = atmcdmex('IsCountConvertModeAvailable', mode);

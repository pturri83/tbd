function [ret, PCB, Decode, dummy1, dummy2, CameraFirmwareVersion, CameraFirmwareBuild] = GetHardwareVersion()
% GetHardwareVersion This function returns the Hardware version information.
%
% SYNOPSIS : [ret, PCB, Decode, dummy1, dummy2, CameraFirmwareVersion, CameraFirmwareBuild] = GetHardwareVersion()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Version information returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%        PCB: Plug-in card version
%        Decode: Flex 10K file version
%        dummy1: 
%        dummy2: 
%        CameraFirmwareVersion: Version number of camera firmware
%        CameraFirmwareBuild: Build number of camera firmware
% REMARKS : C++ Equiv : unsigned int GetHardwareVersion(unsigned int * PCB, unsigned int * Decode, unsigned int * dummy1, unsigned int * dummy2, unsigned int * CameraFirmwareVersion, unsigned int * CameraFirmwareBuild);
%
% SEE ALSO : 
[ret, PCB, Decode, dummy1, dummy2, CameraFirmwareVersion, CameraFirmwareBuild] = atmcdmex('GetHardwareVersion');

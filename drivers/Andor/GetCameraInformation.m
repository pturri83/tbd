function [ret, information] = GetCameraInformation(index)
% GetCameraInformation This function will return information on a particular camera denoted by the index.
%
% SYNOPSIS : [ret, information] = GetCameraInformation(index)
%
% INPUT index: (reserved)
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Driver status return
%          DRV_VXDNOTINSTALLED - Driver not installed
%          DRV_USBERROR - USB device error
%        information: current state of camera
%          Bit:1 - USB camera present 
%          Bit:2 - All dlls loaded properly  
%          Bit:3 - Camera Initialized correctly
% REMARKS : C++ Equiv : unsigned int GetCameraInformation(int index, long * information);
%
% SEE ALSO : GetCameraHandle GetHeadModel GetCameraSerialNumber GetCapabilities 
[ret, information] = atmcdmex('GetCameraInformation', index);

function [ret, MinImageLength] = GetMinimumImageLength()
% GetMinimumImageLength This function will return the minimum number of pixels that can be read out from the chip at each exposure. This minimum value arises due the way in which the chip is read out and will limit the possible sub image dimensions and binning sizes that can be applied.
%
% SYNOPSIS : [ret, MinImageLength] = GetMinimumImageLength()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Minimum Number of Pixels returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_P1INVALID - Invalid MinImageLength value (i.e. NULL)
%        MinImageLength: Will contain the minimum number of super pixels on return.
% REMARKS : C++ Equiv : unsigned int GetMinimumImageLength(int * MinImageLength);
%
% SEE ALSO : SetImage 
[ret, MinImageLength] = atmcdmex('GetMinimumImageLength');

function [ret, fDelay] = GetDDGLiteInitialDelay(channel)
% GetDDGLiteInitialDelay THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, fDelay] = GetDDGLiteInitialDelay(channel)
%
% INPUT channel: 
% OUTPUT ret: Return Code: 
%        fDelay: 
% REMARKS : C++ Equiv : unsigned int GetDDGLiteInitialDelay(AT_DDGLiteChannelId channel, float * fDelay);
%
% SEE ALSO : 
[ret, fDelay] = atmcdmex('GetDDGLiteInitialDelay', channel);

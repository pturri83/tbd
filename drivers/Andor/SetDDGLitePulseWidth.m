function [ret] = SetDDGLitePulseWidth(channel, fWidth)
% SetDDGLitePulseWidth THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetDDGLitePulseWidth(channel, fWidth)
%
% INPUT channel: 
%       fWidth: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDDGLitePulseWidth(AT_DDGLiteChannelId channel, float fWidth);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGLitePulseWidth', channel, fWidth);

function [ret, pulses] = GetDDGIOCNumberRequested()
% GetDDGIOCNumberRequested This function can be used to return the number of pulses that were requested by the user.
%
% SYNOPSIS : [ret, pulses] = GetDDGIOCNumberRequested()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - IOC not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - pulses has invalid memory address.
%        pulses: the number of integrate on chip pulses requested.
% REMARKS : C++ Equiv : unsigned int GetDDGIOCNumberRequested(at_u32 * pulses);
%
% SEE ALSO : GetCapabilities SetDDGIOCNumber SetDDGIOC SetDDGIOCFrequency 
[ret, pulses] = atmcdmex('GetDDGIOCNumberRequested');

function [ret] = CoolerON()
% CoolerON Switches ON the cooling. On some systems the rate of temperature change is controlled until the temperature is within 3°C of the set value. Control is returned immediately to the calling application.
%
% SYNOPSIS : [ret] = CoolerON()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Temperature controller switched ON.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
% REMARKS : C++ Equiv : unsigned int CoolerON(void);
%
% SEE ALSO : CoolerOFF SetTemperature GetTemperature GetTemperatureF GetTemperatureRange GetStatus 
[ret] = atmcdmex('CoolerON');

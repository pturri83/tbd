function [ret] = SetMCPGating(gating)
% SetMCPGating This function controls the MCP gating.
%
% SYNOPSIS : [ret] = SetMCPGating(gating)
%
% INPUT gating: ON/OFF switch for the MCP gating.
%         0 - to switch MCP gating OFF.
%         1 - to switch MCP gating ON.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Value for gating accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Value for gating invalid.
% REMARKS : C++ Equiv : unsigned int SetMCPGating(int gating);
%
% SEE ALSO : SetMCPGain SetGateMode 
[ret] = atmcdmex('SetMCPGating', gating);

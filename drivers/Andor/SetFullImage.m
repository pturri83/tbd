function [ret] = SetFullImage(hbin, vbin)
% SetFullImage Deprecated see Note:
% This function will set the horizontal and vertical binning to be used when taking a full resolution image.
%
% SYNOPSIS : [ret] = SetFullImage(hbin, vbin)
%
% INPUT hbin: number of pixels to bin horizontally
%       vbin: number of pixels to bin vertically
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Binning parameters accepted
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ACQUIRING - Acquisition in progress
%          DRV_P1INVALID - Horizontal binning parameter invalid
%          DRV_P2INVALID - Vertical binning parameter invalid
% REMARKS : C++ Equiv : unsigned int SetFullImage(int hbin, int vbin); // deprecated
%
% SEE ALSO : SetReadMode 
[ret] = atmcdmex('SetFullImage', hbin, vbin);

function [ret] = SetDDGGain(gain)
% SetDDGGain Deprecated for SetMCPGain.
%
% SYNOPSIS : [ret] = SetDDGGain(gain)
%
% INPUT gain: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDDGGain(int gain); // deprecated
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGGain', gain);

function [ret] = SetDDGIOCTrigger(trigger)
% SetDDGIOCTrigger This function can be used to select whether to trigger the IOC pulse train with either the rising edge of the fire pulse or an externally supplied trigger.
%
% SYNOPSIS : [ret] = SetDDGIOCTrigger(trigger)
%
% INPUT trigger: IOC Trigger Option
%         0 - Fire pulse
%         1 - External Trigger
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IOC trigger set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - IOC not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - Invalid trigger.
% REMARKS : C++ Equiv : unsigned int SetDDGIOCTrigger(at_u32 trigger);
%
% SEE ALSO : GetCapabilities GetDDGIOCTrigger SetDDGIOC SetTriggerMode 	 
[ret] = atmcdmex('SetDDGIOCTrigger', trigger);

function [ret] = SetNumberAccumulations(number)
% SetNumberAccumulations This function will set the number of scans accumulated in memory. This will only take effect if the acquisition mode is either Accumulate or Kinetic Series.
%
% SYNOPSIS : [ret] = SetNumberAccumulations(number)
%
% INPUT number: number of scans to accumulate
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Accumulations set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Number of accumulates.
% REMARKS : C++ Equiv : unsigned int SetNumberAccumulations(int number);
%
% SEE ALSO : GetAcquisitionTimings SetAccumulationCycleTime SetAcquisitionMode SetExposureTime SetKineticCycleTime SetNumberKinetics 
[ret] = atmcdmex('SetNumberAccumulations', number);

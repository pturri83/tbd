function [ret, xpixels, ypixels] = GetDetector()
% GetDetector This function returns the size of the detector in pixels. The horizontal axis is taken to be the axis parallel to the readout register.
%
% SYNOPSIS : [ret, xpixels, ypixels] = GetDetector()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Detector size returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        xpixels: number of horizontal pixels.
%        ypixels: number of vertical pixels.
% REMARKS : C++ Equiv : unsigned int GetDetector(int * xpixels, int * ypixels);
%
% SEE ALSO : 
[ret, xpixels, ypixels] = atmcdmex('GetDetector');

function [ret] = SetExternalTriggerTermination(uiTermination)
% SetExternalTriggerTermination This function can be used to set the external trigger termination mode.
%
% SYNOPSIS : [ret] = SetExternalTriggerTermination(uiTermination)
%
% INPUT uiTermination: trigger termination option.
%         0 - 50 ohm.
%         1 - hi-Z.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Termination set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Trigger termination not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
% REMARKS : C++ Equiv : unsigned int SetExternalTriggerTermination(at_u32 uiTermination);
%
% SEE ALSO : GetCapabilities GetExternalTriggerTermination 
[ret] = atmcdmex('SetExternalTriggerTermination', uiTermination);

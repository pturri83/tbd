function [ret] = WaitForAcquisitionTimeOut(iTimeOutMs)
% WaitForAcquisitionTimeOut WaitForAcquisitionTimeOut can be called after an acquisition is started using StartAcquisition to put the calling thread to sleep until an Acquisition Event occurs. This can be used as a simple alternative to the functionality provided by the SetDriverEvent function, as all Event creation and handling is performed internally by the SDK library. Like the SetDriverEvent functionality it will use less processor resources than continuously polling with the GetStatus function. If you wish to restart the calling thread without waiting for an Acquisition event, call the function CancelWait. An Acquisition Event occurs each time a new image is acquired during an Accumulation, Kinetic Series or Run-Till-Abort acquisition or at the end of a Single Scan Acquisition. If an Acquisition Event does not occur within _TimeOutMs milliseconds, WaitForAcquisitionTimeOut returns DRV_NO_NEW_DATA
%
% SYNOPSIS : [ret] = WaitForAcquisitionTimeOut(iTimeOutMs)
%
% INPUT iTimeOutMs: Time before returning DRV_NO_NEW_DATA if no Acquisition Event occurs.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition Event occurred.
%          DRV_NO_NEW_DATA - Non-Acquisition Event occurred.(eg CancelWait () called, time out)
% REMARKS : C++ Equiv : unsigned int WaitForAcquisitionTimeOut(int iTimeOutMs);
%
% SEE ALSO : CancelWait StartAcquisition WaitForAcquisition WaitForAcquisitionByHandle WaitForAcquisitionByHandleTimeOut 
[ret] = atmcdmex('WaitForAcquisitionTimeOut', iTimeOutMs);

function [ret, speed] = GetVSSpeed(index)
% GetVSSpeed As your Andor SDK system may be capable of operating at more than one vertical shift speed this function will return the actual speeds available. The value returned is in microseconds.
%
% SYNOPSIS : [ret, speed] = GetVSSpeed(index)
%
% INPUT index: speed required
%         0 - to GetNumberVSSpeedsGetNumberVSSpeeds()-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Speed returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid index.
%        speed: speed in microseconds per pixel shift.
% REMARKS : C++ Equiv : unsigned int GetVSSpeed(int index, float * speed);
%
% SEE ALSO : GetNumberVSSpeeds SetVSSpeed GetFastestRecommendedVSSpeed 
[ret, speed] = atmcdmex('GetVSSpeed', index);

function [ret, iDirection] = GetIODirection(index)
% GetIODirection Available in some systems are a number of IOs that can be configured to be inputs or outputs. This function gets the current state of a particular IO.
%
% SYNOPSIS : [ret, iDirection] = GetIODirection(index)
%
% INPUT index: IO index. Valid values: 0 to GetNumberIO() - 1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IO direction returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid index.
%          DRV_P2INVALID - Invalid parameter.
%          DRV_NOT_AVAILABLE - Feature not available.
%        iDirection: current direction for this index.
%          0 - 0 Output
%          1 - 1 Input
% REMARKS : C++ Equiv : unsigned int GetIODirection(int index, int * iDirection);
%
% SEE ALSO : GetNumberIO GetIOLevel SetIODirection SetIOLevel 
[ret, iDirection] = atmcdmex('GetIODirection', index);

function [ret] = SaveAsCommentedSif(path, comment)
% SaveAsCommentedSif This function will save the data from the last acquisition into a file. The comment text will be added to the user text portion of the Sif file.
%
% SYNOPSIS : [ret] = SaveAsCommentedSif(path, comment)
%
% INPUT path: a filename specified by the user.
%       comment: comment text to add to the sif file
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data saved.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid filename.
% REMARKS : C++ Equiv : unsigned int SaveAsCommentedSif(char * path, char * comment);
%
% SEE ALSO : SetSifComment SaveAsSif SaveAsEDF SaveAsFITS SaveAsRaw SaveAsSPC SaveAsTiff SaveAsBmp SetSifComment 
[ret] = atmcdmex('SaveAsCommentedSif', path, comment);

function [ret, state] = GetDDGIOC()
% GetDDGIOC This function gets the current state of the integrate on chip (IOC) option.
%
% SYNOPSIS : [ret, state] = GetDDGIOC()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IOC state returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - IOC not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - state has invalid memory address.
%        state: current state of the IOC option (0 - Off, 1 - On).
% REMARKS : C++ Equiv : unsigned int GetDDGIOC(int * state);
%
% SEE ALSO : GetCapabilities SetDDGIOC SetDDGIOCFrequency 
[ret, state] = atmcdmex('GetDDGIOC');

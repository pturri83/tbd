function [ret, MaxExp] = GetMaximumExposure()
% GetMaximumExposure This function will return the maximum Exposure Time in seconds that is settable by the SetExposureTime function.
%
% SYNOPSIS : [ret, MaxExp] = GetMaximumExposure()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Maximum Exposure returned.
%          DRV_P1INVALID - Invalid MaxExp value (i.e. NULL)
%        MaxExp: Will contain the Maximum exposure value on return.
% REMARKS : C++ Equiv : unsigned int GetMaximumExposure(float * MaxExp);
%
% SEE ALSO : SetExposureTime 
[ret, MaxExp] = atmcdmex('GetMaximumExposure');

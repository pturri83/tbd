function [ret] = SetDDGExternalOutputStepEnabled(uiIndex, uiEnabled)
% SetDDGExternalOutputStepEnabled Each external output has the option to track the gate step applied to the gater.  This function can be used to set the state of this option.
%
% SYNOPSIS : [ret] = SetDDGExternalOutputStepEnabled(uiIndex, uiEnabled)
%
% INPUT uiIndex: index of external output.
%       uiEnabled: state of external output track step (0 - Off,1 - On).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - External outputs not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - Invalid external output index.
% REMARKS : C++ Equiv : unsigned int SetDDGExternalOutputStepEnabled(at_u32 uiIndex, at_u32 uiEnabled);
%
% SEE ALSO : GetCapabilities GetDDGExternalOutputEnabled GetDDGExternalOutputStepEnabled 
[ret] = atmcdmex('SetDDGExternalOutputStepEnabled', uiIndex, uiEnabled);

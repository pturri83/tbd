function [ret] = SetDDGIOCPeriod(period)
% SetDDGIOCPeriod This function can be used to set the IOC period that will be triggered. It should only be called once all the conditions of the experiment have been defined.
%
% SYNOPSIS : [ret] = SetDDGIOCPeriod(period)
%
% INPUT period: the period of integrate on chip pulses triggered within the fire pulse.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IOC period set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - IOC not supported.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - Invalid period.
% REMARKS : C++ Equiv : unsigned int SetDDGIOCPeriod(at_u64 period);
%
% SEE ALSO : GetCapabilities SetDDGIOC SetDDGIOCFrequency GetDDGIOCPeriod 
[ret] = atmcdmex('SetDDGIOCPeriod', period);

function [ret] = OutAuxPort(port, state)
% OutAuxPort This function sets the TTL Auxiliary Output port (P) on the Andor plug-in card to either ON/HIGH or OFF/LOW.
%
% SYNOPSIS : [ret] = OutAuxPort(port, state)
%
% INPUT port: Number of AUX out port on Andor card
%         1 - to 4
%       state: state to put port in
%         0 - OFF/LOW
%         all - others	ON/HIGH
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - AUX port set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid port id.
% REMARKS : C++ Equiv : unsigned int OutAuxPort(int port, int state);
%
% SEE ALSO : InAuxPort 
[ret] = atmcdmex('OutAuxPort', port, state);

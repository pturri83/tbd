function [ret] = SetPhotonCountingThreshold(min, max)
% SetPhotonCountingThreshold This function sets the minimum and maximum threshold for the photon counting option.
%
% SYNOPSIS : [ret] = SetPhotonCountingThreshold(min, max)
%
% INPUT min: minimum threshold in counts for photon counting.
%       max: maximum threshold in counts for photon counting
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Thresholds accepted.
%          DRV_P1INVALID - Minimum threshold outside valid range (1-65535)
%          DRV_P2INVALID - Maximum threshold outside valid range
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
% REMARKS : C++ Equiv : unsigned int SetPhotonCountingThreshold(long min, long max);
%
% SEE ALSO : SetPhotonCounting 
[ret] = atmcdmex('SetPhotonCountingThreshold', min, max);

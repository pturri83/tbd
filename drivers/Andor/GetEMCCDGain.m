function [ret, gain] = GetEMCCDGain()
% GetEMCCDGain Returns the current gain setting. The meaning of the value returned depends on the EM Gain mode.
%
% SYNOPSIS : [ret, gain] = GetEMCCDGain()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gain returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%        gain: current EM gain setting
% REMARKS : C++ Equiv : unsigned int GetEMCCDGain(int * gain);
%
% SEE ALSO : 
[ret, gain] = atmcdmex('GetEMCCDGain');

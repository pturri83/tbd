function [ret] = SetDDGLiteInterPulseDelay(channel, fDelay)
% SetDDGLiteInterPulseDelay 
%
% SYNOPSIS : [ret] = SetDDGLiteInterPulseDelay(channel, fDelay)
%
% INPUT channel: 
%       fDelay: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDDGLiteInterPulseDelay(AT_DDGLiteChannelId channel, float fDelay);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGLiteInterPulseDelay', channel, fDelay);

function [ret] = SetGateMode(gatemode)
% SetGateMode Allows the user to control the photocathode gating mode.
%
% SYNOPSIS : [ret] = SetGateMode(gatemode)
%
% INPUT gatemode: the gate mode.
%         0 - Fire ANDed with the Gate input.
%         1 - Gating controlled from Fire pulse only.
%         2 - Gating controlled from SMB Gate input only.
%         3 - Gating ON continuously.
%         4 - Gating OFF continuously.
%         5 - Gate using DDG
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gating mode accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Gating mode invalid.
% REMARKS : C++ Equiv : unsigned int SetGateMode(int gatemode);
%
% SEE ALSO : GetCapabilities SetMCPGain SetMCPGating 
[ret] = atmcdmex('SetGateMode', gatemode);

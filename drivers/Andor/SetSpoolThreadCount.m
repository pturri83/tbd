function [ret] = SetSpoolThreadCount(count)
% SetSpoolThreadCount This function sets the number of parallel threads used for writing data to disk when spooling is enabled. Increasing this to a value greater than the default of 1, can sometimes improve the data rate to the hard disk particularly with Solid State hard disks. In other cases increasing this value may actually reduce the rate at which data is written to disk.
%
% SYNOPSIS : [ret] = SetSpoolThreadCount(count)
%
% INPUT count: The number of threads to use.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Thread count is set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid thread count.
% REMARKS : C++ Equiv : unsigned int SetSpoolThreadCount(int count);
%
% SEE ALSO : SetSpool 
[ret] = atmcdmex('SetSpoolThreadCount', count);

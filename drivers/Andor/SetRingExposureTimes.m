function [ret] = SetRingExposureTimes(numTimes, times)
% SetRingExposureTimes This function will send up an array of exposure times to the camera if the hardware supports the feature. See GetCapabilities. Each acquisition will then use the next exposure in the ring looping round to the start again when the end is reached. There can be a maximum of 16 exposures.
%
% SYNOPSIS : [ret] = SetRingExposureTimes(numTimes, times)
%
% INPUT numTimes: The number of exposures
%       times: A predeclared an array of numTimes floats
% OUTPUT ret: Return Code: 
%          Unsigned int - DRV_NOTAVAILABLE
%          DRV_SUCCESS - Success
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_INVALID_MODE - This mode is not available.
%          DRV_P1INVALID - Must be between 1 and 16 exposures inclusive
%          DRV_P2INVALID - The exposures times are invalid.
% REMARKS : C++ Equiv : unsigned int SetRingExposureTimes(int numTimes, float * times);
%
% SEE ALSO : GetCapabilities GetNumberRingExposureTimes GetAdjustedRingExposureTimes GetRingExposureRange IsTriggerModeAvailable 
[ret] = atmcdmex('SetRingExposureTimes', numTimes, times);

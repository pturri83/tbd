function [ret, caps_ulSize, caps_ulAcqModes, caps_ulReadModes, caps_ulTriggerModes, caps_ulCameraType, caps_ulPixelMode, caps_ulSetFunctions, caps_ulGetFunctions, caps_ulFeatures, caps_ulPCICard, caps_ulEMGainCapability, caps_ulFTReadModes] = GetCapabilities()
%  Legacy Wrapper for GetCapabilities This function will fill in an AndorCapabilities structure with the capabilities associated with the connected camera.  Before passing the address of an AndorCapabilites structure to the function the ulSize member of the structure should be set to the size of the structure. In C++  this can be done with the line:
% caps->ulSize = sizeof(AndorCapabilities);
% Individual capabilities are determined by examining certain bits and combinations of bits in the member variables of the AndorCapabilites structure.
%
% SYNOPSIS : [ret, caps_ulSize, caps_ulAcqModes, caps_ulReadModes, caps_ulTriggerModes, caps_ulCameraType, caps_ulPixelMode, caps_ulSetFunctions, caps_ulGetFunctions, caps_ulFeatures, caps_ulPCICard, caps_ulEMGainCapability, caps_ulFTReadModes] = GetCapabilities()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_SUCCESS - Capabilities returned.
%          DRV_P1INVALID - Invalid caps parameter (i.e. NULL).
%        caps: the capabilities structure to be filled in.
% REMARKS : C++ Equiv : unsigned int GetCapabilities(AndorCapabilities * caps);
%
% SEE ALSO : GetCameraHandle GetCameraSerialNumber GetHeadModel GetCameraInformation 
[ret, caps] = atmcdmex('GetCapabilities');
caps_ulSize = caps.ulSize;
caps_ulAcqModes = caps.ulAcqModes;
caps_ulReadModes = caps.ulReadModes;
caps_ulTriggerModes = caps.ulTriggerModes;
caps_ulCameraType = caps.ulCameraType;
caps_ulPixelMode = caps.ulPixelMode;
caps_ulSetFunctions = caps.ulSetFunctions;
caps_ulGetFunctions = caps.ulGetFunctions;
caps_ulFeatures = caps.ulFeatures;
caps_ulPCICard = caps.ulPCICard;
caps_ulEMGainCapability = caps.ulEMGainCapability;
caps_ulFTReadModes = caps.ulFTReadModes;

function [ret, status] = GetStatus()
% GetStatus This function will return the current status of the Andor SDK system. This function should be called before an acquisition is started to ensure that it is IDLE and during an acquisition to monitor the process.
%
% SYNOPSIS : [ret, status] = GetStatus()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Status returned
%          DRV_NOT_INITIALIZED - System not initialized
%        status: current status
%          DRV_IDLE - waiting on instructions.
%          DRV_TEMPCYCLE - Executing temperature cycle.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ACCUM_TIME_NOT_MET - Unable to meet Accumulate cycle time.
%          DRV_KINETIC_TIME_NOT_MET - Unable to meet Kinetic cycle time.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_ACQ_BUFFER - Computer unable to read the data via the ISA slot at the required rate.
%          DRV_SPOOLERROR - Overflow of the spool buffer.
% REMARKS : C++ Equiv : unsigned int GetStatus(int * status);
%
% SEE ALSO : SetTemperature StartAcquisition 
[ret, status] = atmcdmex('GetStatus');

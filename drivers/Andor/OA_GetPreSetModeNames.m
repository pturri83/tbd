function [ret, pcListOfModes] = OA_GetPreSetModeNames()
% OA_GetPreSetModeNames This function will return the available mode names from the Preset file.  The mode and the Preset file must exist.  The user must allocate enough memory for all of the acquisition parameters.
%
% SYNOPSIS : [ret, pcListOfModes] = OA_GetPreSetModeNames()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null list of modes.
%          DRV_OA_NULL_ERROR - Invalid pointer.
%        pcListOfModes: A user allocated array of characters for storage of the mode names.  Mode names will be delimited by a ','.
% REMARKS : C++ Equiv : unsigned int OA_GetPreSetModeNames(char * pcListOfModes);
%
% SEE ALSO : OA_GetNumberOfPreSetModes 
[ret, pcListOfModes] = atmcdmex('OA_GetPreSetModeNames');

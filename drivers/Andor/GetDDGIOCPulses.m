function [ret, pulses] = GetDDGIOCPulses()
% GetDDGIOCPulses This function can be used to calculate the number of pulses that will be triggered with the given exposure time, readout mode, acquisition mode and integrate on chip frequency. It should only be called once all the conditions of the experiment have been defined.
%
% SYNOPSIS : [ret, pulses] = GetDDGIOCPulses()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%        pulses: the number of integrate on chip pulses triggered within the fire pulse.
% REMARKS : C++ Equiv : unsigned int GetDDGIOCPulses(int * pulses);
%
% SEE ALSO : GetCapabilities SetDDGIOCFrequency GetDDGIOCFrequency SetDDGIOCNumber GetDDGIOCNumber SetDDGIOC SetDDGIOCFrequency 
[ret, pulses] = atmcdmex('GetDDGIOCPulses');

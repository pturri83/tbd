function [ret, puiNumberOfModes] = OA_GetNumberOfPreSetModes()
% OA_GetNumberOfPreSetModes This function will return the number of modes defined in the Preset file.  The Preset file must exist.
%
% SYNOPSIS : [ret, puiNumberOfModes] = OA_GetNumberOfPreSetModes()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null number of modes.
%          DRV_OA_NULL_ERROR - Invalid pointer.
%          DRV_OA_BUFFER_FULL - Number of modes exceeds limit.
%        puiNumberOfModes: The number of modes in the Andor file.
% REMARKS : C++ Equiv : unsigned int OA_GetNumberOfPreSetModes(unsigned int * puiNumberOfModes);
%
% SEE ALSO : OA_GetPreSetModeNames 
[ret, puiNumberOfModes] = atmcdmex('OA_GetNumberOfPreSetModes');

function [ret] = SetDDGInsertionDelay(state)
% SetDDGInsertionDelay This function controls the length of the insertion delay.
%
% SYNOPSIS : [ret] = SetDDGInsertionDelay(state)
%
% INPUT state: NORMAL/FAST switch for insertion delay.
%         0 - to set normal insertion delay.
%         1 - to set fast insertion delay.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Value for delay accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with system.
% REMARKS : C++ Equiv : unsigned int SetDDGInsertionDelay(int state);
%
% SEE ALSO : GetCapabilities SetDDGIntelligate 
[ret] = atmcdmex('SetDDGInsertionDelay', state);

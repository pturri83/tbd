function [ret] = OA_DeleteMode(pcModeName, uiModeNameLen)
% OA_DeleteMode This function will remove a mode from memory.  To permanently remove a mode from file, call OA_WriteToFile after OA_DeleteMode.  The Preset file will not be affected.
%
% SYNOPSIS : [ret] = OA_DeleteMode(pcModeName, uiModeNameLen)
%
% INPUT pcModeName: The name of the mode to be removed.
%       uiModeNameLen: Mode name string length.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - DRV_OA_MODE_DOES_NOT_EXIST
%          DRV_P1INVALID - All parameters accepted                                Null mode name.
%          DRV_OA_INVALID_STRING_LENGTH - The mode name parameter has an invalid  length, i.e. > 256.
% REMARKS : C++ Equiv : unsigned int OA_DeleteMode(const char * pcModeName, int uiModeNameLen);
%
% SEE ALSO : OA_AddMode OA_WriteToFile 
[ret] = atmcdmex('OA_DeleteMode', pcModeName, uiModeNameLen);

function [ret, state] = GetBaselineClamp()
% GetBaselineClamp This function returns the status of the baseline clamp functionality. With this feature enabled the baseline level of each scan in a kinetic series will be more consistent across the sequence.
%
% SYNOPSIS : [ret, state] = GetBaselineClamp()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_SUPPORTED - Baseline Clamp not supported on this camera
%          DRV_P1INVALID - State parameter was not zero or one.
%        state: Baseline clamp functionality Enabled/Disabled
%          1 - Baseline Clamp Enabled
%          0 - Baseline Clamp Disabled
% REMARKS : C++ Equiv : unsigned int GetBaselineClamp(int * state);
%
% SEE ALSO : SetBaselineClamp SetBaselineOffset 
[ret, state] = atmcdmex('GetBaselineClamp');

function [ret] = SetDDGWidthStepCoefficients(mode, p1, p2)
% SetDDGWidthStepCoefficients This function will configure the coefficients used in a kinetic series with gate width step active. The lowest available resolution is 25 picoseconds and the maximum permitted value is 25 seconds for a PCI iStar.
% The lowest available resolution is 10 picoseconds and the maximum permitted value is 10 seconds for a USB iStar.
%
% SYNOPSIS : [ret] = SetDDGWidthStepCoefficients(mode, p1, p2)
%
% INPUT mode: the gate step mode.
%         0 - constant  (p1*(n-1)).
%         1 - exponential (p1*exp(p2*n)).
%         2 - logarithmic (p1*log(p2*n)).
%         3 - linear (p1 + p2*n).
%          - n = 1, 2, ..., number in kinetic series
%       p1: The first coefficient
%       p2: The second coefficient
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gate step mode coefficients set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Gate step mode invalid.
% REMARKS : C++ Equiv : unsigned int SetDDGWidthStepCoefficients(at_u32 mode, double p1, double p2);
%
% SEE ALSO : SetDDGWidthStepMode GetDDGWidthStepMode GetDDGWidthStepCoefficients 
[ret] = atmcdmex('SetDDGWidthStepCoefficients', mode, p1, p2);

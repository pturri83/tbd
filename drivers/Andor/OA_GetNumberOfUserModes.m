function [ret, puiNumberOfModes] = OA_GetNumberOfUserModes()
% OA_GetNumberOfUserModes This function will return the number of modes defined in the User file.  The user defined file must exist.
%
% SYNOPSIS : [ret, puiNumberOfModes] = OA_GetNumberOfUserModes()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null number of modes.
%          DRV_OA_NULL_ERROR - Invalid pointer.
%          DRV_OA_BUFFER_FULL - Number of modes exceeds limit.
%        puiNumberOfModes: The number of modes in the user file.
% REMARKS : C++ Equiv : unsigned int OA_GetNumberOfUserModes(unsigned int * puiNumberOfModes);
%
% SEE ALSO : OA_GetUserModeNames 
[ret, puiNumberOfModes] = atmcdmex('OA_GetNumberOfUserModes');

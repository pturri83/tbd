function [ret, fDelay] = GetDDGLiteInterPulseDelay(channel)
% GetDDGLiteInterPulseDelay THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, fDelay] = GetDDGLiteInterPulseDelay(channel)
%
% INPUT channel: 
% OUTPUT ret: Return Code: 
%        fDelay: 
% REMARKS : C++ Equiv : unsigned int GetDDGLiteInterPulseDelay(AT_DDGLiteChannelId channel, float * fDelay);
%
% SEE ALSO : 
[ret, fDelay] = atmcdmex('GetDDGLiteInterPulseDelay', channel);

function [ret, index] = GetSpoolProgress()
% GetSpoolProgress Deprecated see Note:
% This function will return information on the progress of the current spool operation. The value returned is the number of images that have been saved to disk during the current kinetic scan.
%
% SYNOPSIS : [ret, index] = GetSpoolProgress()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Spool progress returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        index: returns the number of files saved to disk in the current kinetic scan.
% REMARKS : C++ Equiv : unsigned int GetSpoolProgress(long * index); // deprecated
%
% SEE ALSO : SetSpool 
[ret, index] = atmcdmex('GetSpoolProgress');

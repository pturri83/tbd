function [ret, index, speed] = GetFastestRecommendedVSSpeed()
% GetFastestRecommendedVSSpeed As your Andor SDK system may be capable of operating at more than one vertical shift speed this function will return the fastest recommended speed available.  The very high readout speeds, may require an increase in the amplitude of the Vertical Clock Voltage using SetVSAmplitudeSetVSAmplitude.  This function returns the fastest speed which does not require the Vertical Clock Voltage to be adjusted.  The values returned are the vertical shift speed index and the actual speed in microseconds per pixel shift.
%
% SYNOPSIS : [ret, index, speed] = GetFastestRecommendedVSSpeed()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Speed returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%        index: index of the fastest recommended vertical shift speed
%        speed: speed in microseconds per pixel shift.
% REMARKS : C++ Equiv : unsigned int GetFastestRecommendedVSSpeed(int * index, float * speed);
%
% SEE ALSO : GetVSSpeed GetNumberVSSpeeds SetVSSpeed 
[ret, index, speed] = atmcdmex('GetFastestRecommendedVSSpeed');

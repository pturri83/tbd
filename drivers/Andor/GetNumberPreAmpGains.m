function [ret, noGains] = GetNumberPreAmpGains()
% GetNumberPreAmpGains Available in some systems are a number of pre amp gains that can be applied to the data as it is read out. This function gets the number of these pre amp gains available. The functions GetPreAmpGain and SetPreAmpGain can be used to specify which of these gains is to be used.
%
% SYNOPSIS : [ret, noGains] = GetNumberPreAmpGains()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of pre amp gains returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%        noGains: number of allowed pre amp gains
% REMARKS : C++ Equiv : unsigned int GetNumberPreAmpGains(int * noGains);
%
% SEE ALSO : IsPreAmpGainAvailable GetPreAmpGain SetPreAmpGain GetCapabilities 
[ret, noGains] = atmcdmex('GetNumberPreAmpGains');

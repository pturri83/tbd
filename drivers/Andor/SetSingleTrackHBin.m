function [ret] = SetSingleTrackHBin(bin)
% SetSingleTrackHBin This function sets the horizontal binning used when acquiring in Single Track read mode.
%
% SYNOPSIS : [ret] = SetSingleTrackHBin(bin)
%
% INPUT bin: Binning size.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Binning set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid binning size.
% REMARKS : C++ Equiv : unsigned int SetSingleTrackHBin(int bin);
%
% SEE ALSO : SetReadMode 
[ret] = atmcdmex('SetSingleTrackHBin', bin);

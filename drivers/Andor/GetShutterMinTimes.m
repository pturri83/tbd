function [ret, minclosingtime, minopeningtime] = GetShutterMinTimes()
% GetShutterMinTimes This function will return the minimum opening and closing times in milliseconds for the shutter on the current camera.
%
% SYNOPSIS : [ret, minclosingtime, minopeningtime] = GetShutterMinTimes()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Minimum times successfully returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Parameter is NULL.
%          DRV_P2INVALID - Parameter is NULL
%        minclosingtime: returns the minimum closing time in milliseconds that the shutter of the camera supports.
%        minopeningtime: returns the minimum opening time in milliseconds that the shutter of the camera supports.
% REMARKS : C++ Equiv : unsigned int GetShutterMinTimes(int * minclosingtime, int * minopeningtime);
%
% SEE ALSO : 
[ret, minclosingtime, minopeningtime] = atmcdmex('GetShutterMinTimes');

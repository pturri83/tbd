function [ret] = GPIBSend(id, address, text)
% GPIBSend This function initializes the GPIB by sending interface clear. Then the device described by address is put in a listen-active state. Finally the string of characters, text, is sent to the device with a newline character and with the EOI line asserted after the final character.
%
% SYNOPSIS : [ret] = GPIBSend(id, address, text)
%
% INPUT id: The interface board number
%         short - address: Address of device to send data
%       address: The GPIB address to send data to
%       text: The data to send
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data sent.
%          DRV_P3INVALID - Invalid pointer (e.g. NULL). The GPIB device may return other errors. Consult the help documentation supplied with these devices.
% REMARKS : C++ Equiv : unsigned int GPIBSend(int id, short address, char * text);
%
% SEE ALSO : GPIBReceive 
[ret] = atmcdmex('GPIBSend', id, address, text);

function [ret] = SetShutter(typ, mode, closingtime, openingtime)
% SetShutter This function controls the behaviour of the shutter.
% The typ parameter allows the user to control the TTL signal output to an external shutter. The mode parameter configures whether the shutter opens & closes automatically (controlled by the camera) or is permanently open or permanently closed.
% The opening and closing time specify the time required to open and close the shutter (this information is required for calculating acquisition timings (see SHUTTER TRANSFER TIME).
%
% SYNOPSIS : [ret] = SetShutter(typ, mode, closingtime, openingtime)
%
% INPUT typ: shutter type
%         1 - Output TTL high signal to open shutter
%         0 - Output TTL low signal to open shutter
%       mode: Shutter mode
%         0 - Automatic
%         1 - Open
%         2 - Close
%       closingtime: Time shutter takes to close (milliseconds)
%       openingtime: Time shutter takes to open (milliseconds)
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Shutter set.
%          DRV_NOT_INITIALIZED DRV_ACQUIRING - System not initialized.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_NOT_SUPPORTED - Unable to communicate with card.
%          DRV_P1INVALID - Camera does not support shutter control.
%          DRV_P2INVALID - Invalid TTL type.
%          DRV_P3INVALID - Invalid mode.
%          DRV_P4INVALID - Invalid time to open.
% REMARKS : C++ Equiv : unsigned int SetShutter(int typ, int mode, int closingtime, int openingtime);
%
% SEE ALSO : SetShutterEx 
[ret] = atmcdmex('SetShutter', typ, mode, closingtime, openingtime);

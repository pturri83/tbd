function [ret, speed] = GetFKVShiftSpeed(index)
% GetFKVShiftSpeed Deprecated see Note:
% As your Andor SDK system is capable of operating at more than one fast kinetics vertical shift speed this function will return the actual speeds available. The value returned is in microseconds per pixel shift.
%
% SYNOPSIS : [ret, speed] = GetFKVShiftSpeed(index)
%
% INPUT index: speed required
%         0 - to GetNumberFKVShiftSpeedsGetNumberFKVShiftSpeeds()-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Speed returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid index.
%        speed: speed in micro-seconds per pixel shift
% REMARKS : C++ Equiv : unsigned int GetFKVShiftSpeed(int index, int * speed); // deprecated
%
% SEE ALSO : GetNumberFKVShiftSpeeds SetFKVShiftSpeed 
[ret, speed] = atmcdmex('GetFKVShiftSpeed', index);

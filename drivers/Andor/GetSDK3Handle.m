function [ret, Handle] = GetSDK3Handle()
% GetSDK3Handle THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, Handle] = GetSDK3Handle()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        Handle: 
% REMARKS : C++ Equiv : unsigned int GetSDK3Handle(int * Handle);
%
% SEE ALSO : 
[ret, Handle] = atmcdmex('GetSDK3Handle');

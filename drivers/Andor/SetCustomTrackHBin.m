function [ret] = SetCustomTrackHBin(bin)
% SetCustomTrackHBin This function sets the horizontal binning value to be used when the readout mode is set to Random Track.
%
% SYNOPSIS : [ret] = SetCustomTrackHBin(bin)
%
% INPUT bin: Binning size.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Binning set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid binning size.
% REMARKS : C++ Equiv : unsigned int SetCustomTrackHBin(int bin);
%
% SEE ALSO : SetReadMode 
[ret] = atmcdmex('SetCustomTrackHBin', bin);

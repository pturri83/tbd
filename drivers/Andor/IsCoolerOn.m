function [ret, iCoolerStatus] = IsCoolerOn()
% IsCoolerOn This function checks the status of the cooler.
%
% SYNOPSIS : [ret, iCoolerStatus] = IsCoolerOn()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Status returned.
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_P1INVALID - Parameter is NULL
%        iCoolerStatus: iCoolerStatus0: Cooler is OFF.
%          1 - 1 Cooler is ON.
% REMARKS : C++ Equiv : unsigned int IsCoolerOn(int * iCoolerStatus);
%
% SEE ALSO : CoolerON CoolerOFF 
[ret, iCoolerStatus] = atmcdmex('IsCoolerOn');

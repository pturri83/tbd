function [ret, piFlag] = GetFrontEndStatus()
% GetFrontEndStatus This function will return if the Front End cooler has overheated.
%
% SYNOPSIS : [ret, piFlag] = GetFrontEndStatus()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Front End cooler not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with card.
%        piFlag: The status of the front end cooler
%          0 - Normal
%          1 - Tripped
% REMARKS : C++ Equiv : unsigned int GetFrontEndStatus(int * piFlag);
%
% SEE ALSO : SetFrontEndEvent 
[ret, piFlag] = atmcdmex('GetFrontEndStatus');

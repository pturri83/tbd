function [ret] = SaveAsRaw(szFileTitle, typ)
% SaveAsRaw This function saves the last acquisition as a raw data file.
%
% SYNOPSIS : [ret] = SaveAsRaw(szFileTitle, typ)
%
% INPUT szFileTitle: the filename to save to.
%       typ: Data type
%         1 - Signed 16
%         2 - Signed 32
%         3 - Float
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Data successfully saved.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Path invalid.
%          DRV_P2INVALID - Invalid mode
%          DRV_ERROR_PAGELOCK - File too large to be generated in memory
% REMARKS : C++ Equiv : unsigned int SaveAsRaw(char * szFileTitle, int typ);
%
% SEE ALSO : SaveAsSif SaveAsEDF SaveAsFITS SaveAsSPC SaveAsTiff SaveAsBmp 
[ret] = atmcdmex('SaveAsRaw', szFileTitle, typ);

function [ret] = SetAcquisitionType(typ)
% SetAcquisitionType THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetAcquisitionType(typ)
%
% INPUT typ: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetAcquisitionType(int typ);
%
% SEE ALSO : 
[ret] = atmcdmex('SetAcquisitionType', typ);

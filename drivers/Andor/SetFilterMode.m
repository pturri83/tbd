function [ret] = SetFilterMode(mode)
% SetFilterMode This function will set the state of the cosmic ray filter mode for future acquisitions. If the filter mode is on, consecutive scans in an accumulation will be compared and any cosmic ray-like features that are only present in one scan will be replaced with a scaled version of the corresponding pixel value in the correct scan.
%
% SYNOPSIS : [ret] = SetFilterMode(mode)
%
% INPUT mode: current state of filter
%         0 - OFF
%         2 - ON
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Filter mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Mode is out off range.
% REMARKS : C++ Equiv : unsigned int SetFilterMode(int mode);
%
% SEE ALSO : GetFilterMode 
[ret] = atmcdmex('SetFilterMode', mode);

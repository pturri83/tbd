function [ret] = SetFilterParameters(width, sensitivity, range, accept, smooth, noise)
% SetFilterParameters THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetFilterParameters(width, sensitivity, range, accept, smooth, noise)
%
% INPUT width: 
%       sensitivity: 
%       range: 
%       accept: 
%       smooth: 
%       noise: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetFilterParameters(int width, float sensitivity, int range, float accept, int smooth, int noise);
%
% SEE ALSO : 
[ret] = atmcdmex('SetFilterParameters', width, sensitivity, range, accept, smooth, noise);

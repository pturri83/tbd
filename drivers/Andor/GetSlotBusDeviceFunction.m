function [ret, dwslot, dwBus, dwDevice, dwFunction] = GetSlotBusDeviceFunction()
% GetSlotBusDeviceFunction THIS FUNCTION IS RESERVED
%
% SYNOPSIS : [ret, dwslot, dwBus, dwDevice, dwFunction] = GetSlotBusDeviceFunction()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        dwslot: 
%        dwBus: 
%        dwDevice: 
%        dwFunction: 
% REMARKS : C++ Equiv : unsigned int GetSlotBusDeviceFunction(DWORD * dwslot, DWORD * dwBus, DWORD * dwDevice, DWORD * dwFunction);
%
% SEE ALSO : 
[ret, dwslot, dwBus, dwDevice, dwFunction] = atmcdmex('GetSlotBusDeviceFunction');

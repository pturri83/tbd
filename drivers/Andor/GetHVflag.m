function [ret, bFlag] = GetHVflag()
% GetHVflag This function will retrieve the High Voltage flag from your USB iStar intensifier. A 0 value indicates that the high voltage is abnormal.
%
% SYNOPSIS : [ret, bFlag] = GetHVflag()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - HV flag returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_AVAILABLE - Not a USB iStar.
%        bFlag: High Voltage flag.
% REMARKS : C++ Equiv : unsigned int GetHVflag(int * bFlag);
%
% SEE ALSO : 
[ret, bFlag] = atmcdmex('GetHVflag');

function [ret] = SetHorizontalSpeed(index)
% SetHorizontalSpeed Deprecated see Note:
% This function will set the horizontal speed to one of the possible speeds of the system. It will be used for subsequent acquisitions.
%
% SYNOPSIS : [ret] = SetHorizontalSpeed(index)
%
% INPUT index: the horizontal speed to be used
%         0 - to GetNumberHorizontalSpeedsGetNumberHorizontalSpeeds-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Horizontal speed set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Index is out off range.
% REMARKS : C++ Equiv : unsigned int SetHorizontalSpeed(int index); // deprecated
%
% SEE ALSO : GetNumberHorizontalSpeeds GetHorizontalSpeed 
[ret] = atmcdmex('SetHorizontalSpeed', index);

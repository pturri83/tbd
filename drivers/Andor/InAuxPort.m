function [ret, state] = InAuxPort(port)
% InAuxPort This function returns the state of the TTL Auxiliary Input Port on the Andor plug-in card.
%
% SYNOPSIS : [ret, state] = InAuxPort(port)
%
% INPUT port: Number of AUX in port on Andor card.  Valid Values: 1 to 4
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - AUX read.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid port id.
%        state: current state of port
%          0 - OFF/LOW
%          all others - ON/HIGH
% REMARKS : C++ Equiv : unsigned int InAuxPort(int port, int * state);
%
% SEE ALSO : OutAuxPort 
[ret, state] = atmcdmex('InAuxPort', port);

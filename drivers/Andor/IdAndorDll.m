function [ret] = IdAndorDll()
% IdAndorDll THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = IdAndorDll()
%
% INPUT none
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int IdAndorDll(void);
%
% SEE ALSO : 
[ret] = atmcdmex('IdAndorDll');

function [ret, eprom, coffile, vxdrev, vxdver, dllrev, dllver] = GetSoftwareVersion()
% GetSoftwareVersion This function returns the Software version information for the microprocessor code and the driver.
%
% SYNOPSIS : [ret, eprom, coffile, vxdrev, vxdver, dllrev, dllver] = GetSoftwareVersion()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Version information returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%        eprom: EPROM version
%        coffile: COF file version
%        vxdrev: Driver revision number
%        vxdver: Driver version number
%        dllrev: DLL revision number
%        dllver: DLL version number
% REMARKS : C++ Equiv : unsigned int GetSoftwareVersion(unsigned int * eprom, unsigned int * coffile, unsigned int * vxdrev, unsigned int * vxdver, unsigned int * dllrev, unsigned int * dllver);
%
% SEE ALSO : 
[ret, eprom, coffile, vxdrev, vxdver, dllrev, dllver] = atmcdmex('GetSoftwareVersion');

function [ret] = SetBaselineOffset(offset)
% SetBaselineOffset This function allows the user to move the baseline level by the amount selected. For example +100 will add approximately 100 counts to the default baseline value. The value entered should be a multiple of 100 between -1000 and +1000 inclusively.
%
% SYNOPSIS : [ret] = SetBaselineOffset(offset)
%
% INPUT offset: Amount to offset baseline by
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_NOT_AVAILABLE - Baseline Clamp not available for this camera
%          DRV_ACQUIRING - Acquisition in progress
%          DRV_P1INVALID - Offset out of range
% REMARKS : C++ Equiv : unsigned int SetBaselineOffset(int offset);
%
% SEE ALSO : 
[ret] = atmcdmex('SetBaselineOffset', offset);

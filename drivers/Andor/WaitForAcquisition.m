function [ret] = WaitForAcquisition()
% WaitForAcquisition WaitForAcquisition can be called after an acquisition is started using StartAcquisitionStartAcquisition to put the calling thread to sleep until an Acquisition Event occurs. This can be used as a simple alternative to the functionality provided by the SetDriverEvent function, as all Event creation and handling is performed internally by the SDK library.
% Like the SetDriverEvent functionality it will use less processor resources than continuously polling with the GetStatus function. If you wish to restart the calling thread without waiting for an Acquisition event, call the function CancelWaitCancelWait.
% An Acquisition Event occurs each time a new image is acquired during an Accumulation, Kinetic Series or Run-Till-Abort acquisition or at the end of a Single Scan Acquisition.
% If a second event occurs before the first one has been acknowledged, the first one will be ignored. Care should be taken in this case, as you may have to use CancelWaitCancelWait to exit the function.
%
% SYNOPSIS : [ret] = WaitForAcquisition()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition Event occurred
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NO_NEW_DATA - Non-Acquisition Event occurred.(e.g. CancelWait () called)
% REMARKS : C++ Equiv : unsigned int WaitForAcquisition(void);
%
% SEE ALSO : StartAcquisition CancelWait 
[ret] = atmcdmex('WaitForAcquisition');

function [ret, iHFlip, iVFlip] = GetImageFlip()
% GetImageFlip This function will obtain whether the acquired data output is flipped in either the horizontal or vertical direction.
%
% SYNOPSIS : [ret, iHFlip, iVFlip] = GetImageFlip()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - HFlip parameter invalid.
%          DRV_P2INVALID - VFlip parameter invalid
%        iHFlip: Gets horizontal flipping.
%        iVFlip: Gets vertical flipping.
%          1 - Flipping Enabled
%          0 - Flipping Disabled
% REMARKS : C++ Equiv : unsigned int GetImageFlip(int * iHFlip, int * iVFlip);
%
% SEE ALSO : SetImageRotate SetImageFlip 
[ret, iHFlip, iVFlip] = atmcdmex('GetImageFlip');

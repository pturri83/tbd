function [ret, xSize, ySize] = GetPixelSize()
% GetPixelSize This function returns the dimension of the pixels in the detector in microns.
%
% SYNOPSIS : [ret, xSize, ySize] = GetPixelSize()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Pixel size returned.
%        xSize: width of pixel.
%        ySize: height of pixel.
% REMARKS : C++ Equiv : unsigned int GetPixelSize(float * xSize, float * ySize);
%
% SEE ALSO : 
[ret, xSize, ySize] = atmcdmex('GetPixelSize');

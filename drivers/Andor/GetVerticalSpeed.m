function [ret, speed] = GetVerticalSpeed(index)
% GetVerticalSpeed Deprecated see Note:
% As your Andor system may be capable of operating at more than one vertical shift speed this function will return the actual speeds available. The value returned is in microseconds per pixel shift.
%
% SYNOPSIS : [ret, speed] = GetVerticalSpeed(index)
%
% INPUT index: speed required
%         0 - to GetNumberVerticalSpeedsGetNumberVerticalSpeeds()-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Speed returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING DRV_P1INVALID - Acquisition in progress.
%        speed: speed in microseconds per pixel shift.
% REMARKS : C++ Equiv : unsigned int GetVerticalSpeed(int index, int * speed); // deprecated
%
% SEE ALSO : GetNumberVerticalSpeeds SetVerticalSpeed 
[ret, speed] = atmcdmex('GetVerticalSpeed', index);

function [ret] = AbortAcquisition()
% AbortAcquisition This function aborts the current acquisition if one is active.
%
% SYNOPSIS : [ret] = AbortAcquisition()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition aborted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_IDLE - The system is not currently acquiring.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_ERROR_ACK - Unable to communicate with card.
% REMARKS : C++ Equiv : unsigned int AbortAcquisition(void);
%
% SEE ALSO : GetStatus StartAcquisition 
[ret] = atmcdmex('AbortAcquisition');

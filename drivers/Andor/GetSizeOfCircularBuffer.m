function [ret, index] = GetSizeOfCircularBuffer()
% GetSizeOfCircularBuffer This function will return the maximum number of images the circular buffer can store based on the current acquisition settings.
%
% SYNOPSIS : [ret, index] = GetSizeOfCircularBuffer()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Maximum number of images returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        index: returns the maximum number of images the circular buffer can store.
% REMARKS : C++ Equiv : unsigned int GetSizeOfCircularBuffer(long * index);
%
% SEE ALSO : 
[ret, index] = atmcdmex('GetSizeOfCircularBuffer');

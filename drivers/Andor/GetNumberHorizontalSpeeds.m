function [ret, number] = GetNumberHorizontalSpeeds()
% GetNumberHorizontalSpeeds Deprecated see Note:
% As your Andor SDK system is capable of operating at more than one horizontal shift speed this function will return the actual number of speeds available.
%
% SYNOPSIS : [ret, number] = GetNumberHorizontalSpeeds()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of speeds returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%        number: number of allowed horizontal speeds
% REMARKS : C++ Equiv : unsigned int GetNumberHorizontalSpeeds(int * number); // deprecated
%
% SEE ALSO : GetHorizontalSpeed SetHorizontalSpeed 
[ret, number] = atmcdmex('GetNumberHorizontalSpeeds');

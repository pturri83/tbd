function [ret, acc, series] = GetAcquisitionProgress()
% GetAcquisitionProgress This function will return information on the progress of the current acquisition. It can be called at any time but is best used in conjunction with SetDriverEventSetDriverEvent.
% The values returned show the number of completed scans in the current acquisition.
% If 0 is returned for both accum and series then either:-
% * No acquisition is currently running
% * The acquisition has just completed
% * The very first scan of an acquisition has just started and not yet completed
% GetStatus can be used to confirm if the first scan has just started, returning
% DRV_ACQUIRING, otherwise it will return DRV_IDLE.
% For example, if [i]accum[/i]=2 and [i]series[/i]=3 then the acquisition has completed 3 in the series and 2 accumulations in the 4 scan of the series.
%
% SYNOPSIS : [ret, acc, series] = GetAcquisitionProgress()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of accumulation and series scans completed.
%          DRV_NOT_INITIALIZED - System not initialized.
%        acc: returns the number of accumulations completed in the current kinetic scan.
%        series: series the number of kinetic scans completed
% REMARKS : C++ Equiv : unsigned int GetAcquisitionProgress(long * acc, long * series);
%
% SEE ALSO : SetAcquisitionMode SetNumberAccumulations SetNumberKinetics SetDriverEvent 
[ret, acc, series] = atmcdmex('GetAcquisitionProgress');

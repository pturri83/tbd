function [ret] = OA_WriteToFile(pcFileName, uiFileNameLen)
% OA_WriteToFile This function will write a User defined list of modes to the User file.  The Preset file will not be affected.
%
% SYNOPSIS : [ret] = OA_WriteToFile(pcFileName, uiFileNameLen)
%
% INPUT pcFileName: The name of the file to be written to.
%       uiFileNameLen: File name string length.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null filename
%          DRV_OA_INVALID_STRING_LENGTH - One or more of the string parameters has an invalid length, i.e. > 255.
%          DRV_OA_INVALID_FILE - Data cannot be written to the Preset Andor file.
%          DRV_ERROR_FILESAVE - Failed to save data to file.
%          DRV_OA_FILE_HAS_BEEN_MODIFIED - File to be written to has been modified since last write, local copy of file may not be the same.
%          DRV_OA_INVALID_CHARS_IN_NAME - File name contains invalid characters.
% REMARKS : C++ Equiv : unsigned int OA_WriteToFile(const char * pcFileName, int uiFileNameLen);
%
% SEE ALSO : OA_AddMode OA_DeleteMode 
[ret] = atmcdmex('OA_WriteToFile', pcFileName, uiFileNameLen);

function [ret] = SetOutputAmplifier(typ)
% SetOutputAmplifier Some EMCCD systems have the capability to use a second output amplifier. This function will set the type of output amplifier to be used when reading data from the head for these systems.
%
% SYNOPSIS : [ret] = SetOutputAmplifier(typ)
%
% INPUT typ: the type of output amplifier.
%         0 - Standard EMCCD gain register (default)/Conventional(clara).
%         1 - Conventional CCD register/Extended NIR mode(clara).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Series length set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Output amplifier type invalid.
% REMARKS : C++ Equiv : unsigned int SetOutputAmplifier(int typ);
%
% SEE ALSO : 
[ret] = atmcdmex('SetOutputAmplifier', typ);

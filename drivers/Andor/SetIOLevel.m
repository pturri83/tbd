function [ret] = SetIOLevel(index, iLevel)
% SetIOLevel Available in some systems are a number of IOs that can be configured to be inputs or outputs. This function sets the current state of a particular IO.
%
% SYNOPSIS : [ret] = SetIOLevel(index, iLevel)
%
% INPUT index: IO index
%         0 - to GetNumberIO() - 1
%       iLevel: current level for this index.
%         0 - 0 Low
%         1 - 1 High
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IO level set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid index.
%          DRV_P2INVALID - Invalid level.
%          DRV_NOT_AVAILABLE - Feature not available.
% REMARKS : C++ Equiv : unsigned int SetIOLevel(int index, int iLevel);
%
% SEE ALSO : GetNumberIO GetIOLevel GetIODirection SetIODirection 
[ret] = atmcdmex('SetIOLevel', index, iLevel);

function [ret] = SetShutterEx(typ, mode, closingtime, openingtime, extmode)
% SetShutterEx This function expands the control offered by SetShutter to allow an external shutter and internal shutter to be controlled independently (only available on some cameras - please consult your Camera User Guide). The typ parameter allows the user to control the TTL signal output to an external shutter. The opening and closing times specify the length of time required to open and close the shutter (this information is required for calculating acquisition timings - see SHUTTER TRANSFER TIME).
% The mode and extmode parameters control the behaviour of the internal and external shutters. To have an external shutter open and close automatically in an experiment, set the mode parameter to Open and set the extmode parameter to Auto. To have an internal shutter open and close automatically in an experiment, set the extmode parameter to Open and set the mode parameter to Auto.
% To not use any shutter in the experiment, set both shutter modes to permanently open.
%
% SYNOPSIS : [ret] = SetShutterEx(typ, mode, closingtime, openingtime, extmode)
%
% INPUT typ: Shutter type
%         0 - Output TTL low signal to open shutter
%         1 - Output TTL high signal to open shutter
%       mode: Internal shutter mode.
%         0 - Auto
%         1 - Open
%         2 - Close
%       closingtime: time shutter takes to close (milliseconds)
%       openingtime: Time shutter takes to open (milliseconds)
%       extmode: External shutter mode.
%         0 - Auto
%         1 - Open
%         2 - Close
% OUTPUT ret: Return Code: 
%          Unsigned int - DRV_P5INVALID
%          DRV_SUCCESS - Shutter set.
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ACQUIRING - Acquisition in progress
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_NOT_SUPPORTED - Camera does not support shutter control.
%          DRV_P1INVALID - Invalid TTL type.
%          DRV_P2INVALID - Invalid internal mode
%          DRV_P3INVALID - Invalid time to open.
%          DRV_P4INVALID - Invalid time to close
% REMARKS : C++ Equiv : unsigned int SetShutterEx(int typ, int mode, int closingtime, int openingtime, int extmode);
%
% SEE ALSO : SetShutter 
[ret] = atmcdmex('SetShutterEx', typ, mode, closingtime, openingtime, extmode);

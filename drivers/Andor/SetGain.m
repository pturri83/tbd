function [ret] = SetGain(gain)
% SetGain Deprecated for SetMCPGain.
%
% SYNOPSIS : [ret] = SetGain(gain)
%
% INPUT gain: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetGain(int gain); // deprecated
%
% SEE ALSO : 
[ret] = atmcdmex('SetGain', gain);

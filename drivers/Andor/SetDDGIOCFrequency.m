function [ret] = SetDDGIOCFrequency(frequency)
% SetDDGIOCFrequency This function sets the frequency of the integrate on chip option. It should be called once the conditions of the experiment have been setup in order for correct operation. The frequency should be limited to 5000Hz when intelligate is activated to prevent damage to the head and 50000Hz otherwise to prevent the gater from overheating. The recommended order is
% ...
% Experiment setup (exposure time, readout mode, gate parameters, ...)
% ...
% SetDDGIOCFrequency (x)
% SetDDGIOCSetDDGIOC(true)
% GetDDGIOCPulses(y)
% StartAcquisitionStartAcquisition()
%
% SYNOPSIS : [ret] = SetDDGIOCFrequency(frequency)
%
% INPUT frequency: frequency of IOC option in Hz.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Value for frequency accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_SUPPORTED - IOC not supported.
%          DRV_ERROR_ACK - Unable to communicate with card.
% REMARKS : C++ Equiv : unsigned int SetDDGIOCFrequency(double frequency);
%
% SEE ALSO : GetDDGIOCFrequency SetDDGIOCNumber GetDDGIOCNumber GetDDGIOCPulses SetDDGIOC 
[ret] = atmcdmex('SetDDGIOCFrequency', frequency);

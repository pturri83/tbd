function [ret] = SetEMAdvanced(state)
% SetEMAdvanced This function turns on and off access to higher EM gain levels within the SDK. Typically, optimal signal to noise ratio and dynamic range is achieved between x1 to x300 EM Gain. Higher gains of > x300 are recommended for single photon counting only. Before using higher levels, you should ensure that light levels do not exceed the regime of tens of photons per pixel, otherwise accelerated ageing of the sensor can occur.
%
% SYNOPSIS : [ret] = SetEMAdvanced(state)
%
% INPUT state: Enables/Disables access to higher EM gain levels
%         1 - Enable access
%         1 - Disable access
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_AVAILABLE - Advanced EM gain not available for this camera.
%          DRV_ACQUIRING. - Acquisition in progress.
%          DRV_P1INVALID - State parameter was not zero or one.
% REMARKS : C++ Equiv : unsigned int SetEMAdvanced(int state);
%
% SEE ALSO : GetCapabilities GetEMCCDGain SetEMCCDGain SetEMGainMode 
[ret] = atmcdmex('SetEMAdvanced', state);

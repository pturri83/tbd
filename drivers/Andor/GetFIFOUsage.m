function [ret, FIFOusage] = GetFIFOUsage()
% GetFIFOUsage THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, FIFOusage] = GetFIFOUsage()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        FIFOusage: 
% REMARKS : C++ Equiv : unsigned int GetFIFOUsage(int * FIFOusage);
%
% SEE ALSO : 
[ret, FIFOusage] = atmcdmex('GetFIFOUsage');

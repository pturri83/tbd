function [ret] = AndorInitialize(dir)
% Initialize This function will initialize the Andor SDK System. As part of the initialization procedure on some cameras (i.e. Classic, iStar and earlier iXion) the DLL will need access to a DETECTOR.INI which contains information relating to the detector head, number pixels, readout speeds etc. If your system has multiple cameras then see the section Controlling multiple cameras
%
% SYNOPSIS : [ret] = Initialize(dir)
%
% INPUT dir: Path to the directory containing the files
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS DRV_VXDNOTINSTALLED - Initialisation successful.
%          DRV_VXDNOTINSTALLED - VxD not loaded.
%          DRV_INIERROR - Unable to load DETECTOR.INI.
%          DRV_COFERROR - Unable to load *.COF.
%          DRV_FLEXERROR - Unable to load *.RBF.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_ERROR_FILELOAD - Unable to load *.COF or *.RBF files.
%          DRV_ERROR_PAGELOCK - Unable to acquire lock on requested memory.
%          DRV_USBERROR - Unable to detect USB device or not USB2.0.
%          DRV_ERROR_NOCAMERA - No camera found
% REMARKS : C++ Equiv : unsigned int Initialize(char * dir);
%
% SEE ALSO : GetAvailableCameras SetCurrentCamera GetCurrentCamera 
[ret] = atmcdmex('Initialize', dir);

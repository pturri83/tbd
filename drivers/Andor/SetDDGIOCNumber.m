function [ret] = SetDDGIOCNumber(numberPulses)
% SetDDGIOCNumber This function allows the user to limit the number of pulses used in the integrate on chip option at a given frequency. It should be called once the conditions of the experiment have been setup in order for correct operation.
%
% SYNOPSIS : [ret] = SetDDGIOCNumber(numberPulses)
%
% INPUT numberPulses: the number of integrate on chip pulses triggered within the fire pulse.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Value for IOC number accepted
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ACQUIRING - Acquisition in progress
%          DRV_NOT_SUPPORTED - IOC not supported
%          DRV_ERROR_ACK - Unable to communicate with card
% REMARKS : C++ Equiv : unsigned int SetDDGIOCNumber(long numberPulses);
%
% SEE ALSO : SetDDGIOCFrequency GetDDGIOCFrequency GetDDGIOCNumber GetDDGIOCPulses SetDDGIOC 
[ret] = atmcdmex('SetDDGIOCNumber', numberPulses);

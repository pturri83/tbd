function [ret] = SetMultiTrackScan(trackHeight, numberTracks, iSIHStart, iSIHEnd, trackHBinning, trackVBinning, trackGap, trackOffset, trackSkip, numberSubFrames)
% SetMultiTrackScan THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetMultiTrackScan(trackHeight, numberTracks, iSIHStart, iSIHEnd, trackHBinning, trackVBinning, trackGap, trackOffset, trackSkip, numberSubFrames)
%
% INPUT trackHeight: 
%       numberTracks: 
%       iSIHStart: 
%       iSIHEnd: 
%       trackHBinning: 
%       trackVBinning: 
%       trackGap: 
%       trackOffset: 
%       trackSkip: 
%       numberSubFrames: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetMultiTrackScan(int trackHeight, int numberTracks, int iSIHStart, int iSIHEnd, int trackHBinning, int trackVBinning, int trackGap, int trackOffset, int trackSkip, int numberSubFrames);
%
% SEE ALSO : 
[ret] = atmcdmex('SetMultiTrackScan', trackHeight, numberTracks, iSIHStart, iSIHEnd, trackHBinning, trackVBinning, trackGap, trackOffset, trackSkip, numberSubFrames);

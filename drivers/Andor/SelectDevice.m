function [ret] = SelectDevice(devNum)
% SelectDevice THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SelectDevice(devNum)
%
% INPUT devNum: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SelectDevice(int devNum);
%
% SEE ALSO : 
[ret] = atmcdmex('SelectDevice', devNum);

function [ret, data] = SetNextAddress16(lowAdd, highAdd, length, physical)
% SetNextAddress16 THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, data] = SetNextAddress16(lowAdd, highAdd, length, physical)
%
% INPUT lowAdd: 
%       highAdd: 
%       length: 
%       physical: 
% OUTPUT ret: Return Code: 
%        data: 
% REMARKS : C++ Equiv : unsigned int SetNextAddress16(at_32 * data, long lowAdd, long highAdd, long length, long physical);
%
% SEE ALSO : 
[ret, data] = atmcdmex('SetNextAddress16', lowAdd, highAdd, length, physical);

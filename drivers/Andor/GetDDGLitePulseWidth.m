function [ret, fWidth] = GetDDGLitePulseWidth(channel)
% GetDDGLitePulseWidth THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, fWidth] = GetDDGLitePulseWidth(channel)
%
% INPUT channel: 
% OUTPUT ret: Return Code: 
%        fWidth: 
% REMARKS : C++ Equiv : unsigned int GetDDGLitePulseWidth(AT_DDGLiteChannelId channel, float * fWidth);
%
% SEE ALSO : 
[ret, fWidth] = atmcdmex('GetDDGLitePulseWidth', channel);

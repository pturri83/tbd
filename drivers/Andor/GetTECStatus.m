function [ret, piFlag] = GetTECStatus()
% GetTECStatus This function will return if the TEC has overheated.
%
% SYNOPSIS : [ret, piFlag] = GetTECStatus()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - TEC status not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with card.
%        piFlag: The status of the TEC
%          0 - Normal
%          1 - Tripped
% REMARKS : C++ Equiv : unsigned int GetTECStatus(int * piFlag);
%
% SEE ALSO : SetTECEvent 
[ret, piFlag] = atmcdmex('GetTECStatus');

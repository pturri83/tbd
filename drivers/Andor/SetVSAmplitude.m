function [ret] = SetVSAmplitude(index)
% SetVSAmplitude If you choose a high readout speed (a low readout time), then you should also consider increasing the amplitude of the Vertical Clock Voltage.
% There are five levels of amplitude available for you to choose from:
% * Normal
% * +1
% * +2
% * +3
% * +4
% Exercise caution when increasing the amplitude of the vertical clock voltage, since higher clocking voltages may result in increased clock-induced charge (noise) in your signal. In general, only the very highest vertical clocking speeds are likely to benefit from an increased vertical clock voltage amplitude.
%
% SYNOPSIS : [ret] = SetVSAmplitude(index)
%
% INPUT index: desired Vertical Clock Voltage Amplitude
%         0 - Normal
%         1 ->4 - Increasing Clock voltage Amplitude
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Amplitude set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_AVAILABLE - Your system does not support this feature
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid amplitude parameter.
% REMARKS : C++ Equiv : unsigned int SetVSAmplitude(int index);
%
% SEE ALSO : 
[ret] = atmcdmex('SetVSAmplitude', index);

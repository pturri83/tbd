function [ret] = SetRegisterDump(mode)
% SetRegisterDump THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetRegisterDump(mode)
%
% INPUT mode: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetRegisterDump(int mode);
%
% SEE ALSO : 
[ret] = atmcdmex('SetRegisterDump', mode);

function [ret] = Filter_SetThreshold(threshold)
% Filter_SetThreshold Sets the threshold value for the Noise Filter.
%
% SYNOPSIS : [ret] = Filter_SetThreshold(threshold)
%
% INPUT threshold: Threshold value used to process image.
%         0 - 65535  for Level Above filter.
%         0 - 10 for all other filters.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Threshold set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Noise Filter processing not available for this camera.
%          DRV_P1INVALID - Invalid threshold.
% REMARKS : C++ Equiv : unsigned int Filter_SetThreshold(float threshold);
%
% SEE ALSO : Filter_GetThreshold 
[ret] = atmcdmex('Filter_SetThreshold', threshold);

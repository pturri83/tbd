function [ret] = SetAccumulationCycleTime(time)
% SetAccumulationCycleTime This function will set the accumulation cycle time to the nearest valid value not less than the given value. The actual cycle time used is obtained by GetAcquisitionTimingsGetAcquisitionTimings. Please refer to SECTION 5 - ACQUISITION MODES for further information.
%
% SYNOPSIS : [ret] = SetAccumulationCycleTime(time)
%
% INPUT time: the accumulation cycle time in seconds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Cycle time accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Exposure time invalid.
% REMARKS : C++ Equiv : unsigned int SetAccumulationCycleTime(float time);
%
% SEE ALSO : SetNumberAccumulations GetAcquisitionTimings 
[ret] = atmcdmex('SetAccumulationCycleTime', time);

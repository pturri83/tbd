function [ret] = SetReadMode(mode)
% SetReadMode This function will set the readout mode to be used on the subsequent acquisitions.
%
% SYNOPSIS : [ret] = SetReadMode(mode)
%
% INPUT mode: readout mode
%         0 - Full Vertical Binning
%         1 - Multi-Track
%         2 - Random-Track
%         3 - Single-Track
%         4 - Image
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Readout mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid readout mode passed.
% REMARKS : C++ Equiv : unsigned int SetReadMode(int mode);
%
% SEE ALSO : GetAcquisitionTimings SetAccumulationCycleTime SetAcquisitionMode SetExposureTime SetKineticCycleTime SetNumberAccumulations SetNumberKinetics 
[ret] = atmcdmex('SetReadMode', mode);

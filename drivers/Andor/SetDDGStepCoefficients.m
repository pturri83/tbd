function [ret] = SetDDGStepCoefficients(mode, p1, p2)
% SetDDGStepCoefficients This function will configure the coefficients used in a kinetic series with gate step active. The lowest available resolution is 25 picoseconds and the maximum permitted value is 25 seconds for a PCI iStar.
% The lowest available resolution is 10 picoseconds and the maximum permitted value is 10 seconds for a USB iStar.
%
% SYNOPSIS : [ret] = SetDDGStepCoefficients(mode, p1, p2)
%
% INPUT mode: the gate step mode.
%         0 - constant  (p1*(n-1)).
%         1 - exponential (p1*exp(p2*n)).
%         2 - logarithmic (p1*log(p2*n)).
%         3 - linear (p1 + p2*n).
%         n - = 1, 2, ..., number in kinetic series
%       p1: 
%       p2: 
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gate step mode coefficients set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with system.
%          DRV_P1INVALID - Gate step mode invalid.
% REMARKS : C++ Equiv : unsigned int SetDDGStepCoefficients(at_u32 mode, double p1, double p2);
%
% SEE ALSO : StartAcquisition SetDDGStepMode GetDDGStepMode GetDDGStepCoefficients 
[ret] = atmcdmex('SetDDGStepCoefficients', mode, p1, p2);

function [ret, pInputImage, pOutputImage] = PostProcessDataAveraging(iOutputBufferSize, iNumImages, iAveragingFilterMode, iHeight, iWidth, iFrameCount, iAveragingFactor)
% PostProcessDataAveraging THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, pInputImage, pOutputImage] = PostProcessDataAveraging(iOutputBufferSize, iNumImages, iAveragingFilterMode, iHeight, iWidth, iFrameCount, iAveragingFactor)
%
% INPUT iOutputBufferSize: 
%       iNumImages: 
%       iAveragingFilterMode: 
%       iHeight: 
%       iWidth: 
%       iFrameCount: 
%       iAveragingFactor: 
% OUTPUT ret: Return Code: 
%        pInputImage: 
%        pOutputImage: 
% REMARKS : C++ Equiv : unsigned int PostProcessDataAveraging(at_32 * pInputImage, at_32 * pOutputImage, int iOutputBufferSize, int iNumImages, int iAveragingFilterMode, int iHeight, int iWidth, int iFrameCount, int iAveragingFactor);
%
% SEE ALSO : 
[ret, pInputImage, pOutputImage] = atmcdmex('PostProcessDataAveraging', iOutputBufferSize, iNumImages, iAveragingFilterMode, iHeight, iWidth, iFrameCount, iAveragingFactor);

function [ret, cameraHandle] = GetCameraHandle(cameraIndex)
% GetCameraHandle This function returns the handle for the camera specified by cameraIndex.  When multiple Andor cameras are installed the handle of each camera must be retrieved in order to select a camera using the SetCurrentCamera function.
% The number of cameras can be obtained using the GetAvailableCameras function.
%
% SYNOPSIS : [ret, cameraHandle] = GetCameraHandle(cameraIndex)
%
% INPUT cameraIndex: index of any of the installed cameras. 0 to NumberCameras-1 where NumberCameras is the value returned by the GetAvailableCamerasGetAvailableCameras functionGetAvailableCamerasGetNumberVerticalSpeedsGetNumberHSSpeeds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Camera handle returned.
%          DRV_P1INVALID - Invalid camera index.
%        cameraHandle: handle of the camera.
% REMARKS : C++ Equiv : unsigned int GetCameraHandle(long cameraIndex, long * cameraHandle);
%
% SEE ALSO : SetCurrentCamera GetAvailableCameras GetCurrentCamera 
[ret, cameraHandle] = atmcdmex('GetCameraHandle', cameraIndex);

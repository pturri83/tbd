function [ret, totalCameras] = GetAvailableCameras()
% GetAvailableCameras This function returns the total number of Andor cameras currently installed. It is possible to call this function before any of the cameras are initialized.
%
% SYNOPSIS : [ret, totalCameras] = GetAvailableCameras()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of available cameras returned.
%          DRV_GENERAL_ERRORS - An error occurred while obtaining the number of available cameras.
%        totalCameras: the number of cameras currently installed
% REMARKS : C++ Equiv : unsigned int GetAvailableCameras(long * totalCameras);
%
% SEE ALSO : SetCurrentCamera GetCurrentCamera GetCameraHandle 
[ret, totalCameras] = atmcdmex('GetAvailableCameras');

function [ret] = SetVerticalRowBuffer(rows)
% SetVerticalRowBuffer THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetVerticalRowBuffer(rows)
%
% INPUT rows: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetVerticalRowBuffer(int rows);
%
% SEE ALSO : 
[ret] = atmcdmex('SetVerticalRowBuffer', rows);

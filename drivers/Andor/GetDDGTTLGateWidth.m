function [ret, ttlWidth] = GetDDGTTLGateWidth(opticalWidth)
% GetDDGTTLGateWidth This function can be used to get the TTL gate width which corresponds to a particular optical gate width.
%
% SYNOPSIS : [ret, ttlWidth] = GetDDGTTLGateWidth(opticalWidth)
%
% INPUT opticalWidth: optical gate width in picoseconds.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timings returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Optical gate width not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P2_INVALID - Unable to communicate with system.
%        ttlWidth: TTL gate width in picoseconds.
% REMARKS : C++ Equiv : unsigned int GetDDGTTLGateWidth(at_u64 opticalWidth, at_u64 * ttlWidth);
%
% SEE ALSO : GetCapabilities SetDDGOpticalWidthEnabled SetDDGGateStep 
[ret, ttlWidth] = atmcdmex('GetDDGTTLGateWidth', opticalWidth);

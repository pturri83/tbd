function [ret, frames] = Filter_GetAveragingFrameCount()
% Filter_GetAveragingFrameCount Returns the current frame count value.
%
% SYNOPSIS : [ret, frames] = Filter_GetAveragingFrameCount()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Frame count returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid frame count (i.e. NULL pointer).
%        frames: The current frame count value.
% REMARKS : C++ Equiv : unsigned int Filter_GetAveragingFrameCount(int * frames);
%
% SEE ALSO : Filter_SetAveragingFrameCount 
[ret, frames] = atmcdmex('Filter_GetAveragingFrameCount');

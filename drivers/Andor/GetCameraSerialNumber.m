function [ret, number] = GetCameraSerialNumber()
% GetCameraSerialNumber This function will retrieve camera's serial number.
%
% SYNOPSIS : [ret, number] = GetCameraSerialNumber()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Serial Number returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%        number: Serial Number.
% REMARKS : C++ Equiv : unsigned int GetCameraSerialNumber(int * number);
%
% SEE ALSO : GetCameraHandle GetHeadModel GetCameraInformation GetCapabilities 
[ret, number] = atmcdmex('GetCameraSerialNumber');

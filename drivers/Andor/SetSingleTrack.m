function [ret] = SetSingleTrack(centre, height)
% SetSingleTrack This function will set the single track parameters. The parameters are validated in the following order: centre row and then track height.
%
% SYNOPSIS : [ret] = SetSingleTrack(centre, height)
%
% INPUT centre: centre row of track
%         Valid - range 0 to number of vertical pixels.
%       height: height of track
%         Valid - range > 1 (maximum value depends on centre row and number of vertical pixels).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Center row invalid.
%          DRV_P2INVALID - Track height invalid.
% REMARKS : C++ Equiv : unsigned int SetSingleTrack(int centre, int height);
%
% SEE ALSO : SetReadMode 
[ret] = atmcdmex('SetSingleTrack', centre, height);

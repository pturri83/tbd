function [ret] = SetCropMode(active, cropHeight, reserved)
% SetCropMode This function effectively reduces the height of the CCD by excluding some rows to achieve higher frame rates. This is currently only available on Newton cameras when the selected read mode is Full Vertical Binning. The cropHeight is the number of active rows measured from the bottom of the CCD.
% Note: it is important to ensure that no light falls on the excluded region otherwise the acquired data will be corrupted.
%
% SYNOPSIS : [ret] = SetCropMode(active, cropHeight, reserved)
%
% INPUT active: Crop mode active
%         0 - Crop mode is OFF
%         1 - Crop mode if ON
%       cropHeight: The selected crop height. This value must be between 1 and the CCD
%         height - int reserved: This value should be set to 0.
%       reserved: This value should be set to 0
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIAILIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Active parameter is not zero or one.
%          DRV_P2INVALID - Cropheight parameter is less than one or greater than the CCD height.
%          DRV_P3INVALID - Reserved parameter is not equal to zero.
%          DRV_NOT_SUPPORTED - Either the camera is not a Newton or the read mode is not Full Vertical Binning.
% REMARKS : C++ Equiv : unsigned int SetCropMode(int active, int cropHeight, int reserved);
%
% SEE ALSO : GetDetector SetIsolatedCropMode 
[ret] = atmcdmex('SetCropMode', active, cropHeight, reserved);

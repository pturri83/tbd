function [ret, piGatemode] = GetGateMode()
% GetGateMode Allows the user to get the current photocathode gating mode.
%
% SYNOPSIS : [ret, piGatemode] = GetGateMode()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Gating mode accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - gatemode has invalid memory address.
%        piGatemode: the gate mode.
%          0 - Fire ANDed with the Gate input.
%          1 - Gating controlled from Fire pulse only.
%          2 - Gating controlled from SMB Gate input only.
%          3 - Gating ON continuously.
%          4 - Gating OFF continuously.
%          5 - Gate using DDG
% REMARKS : C++ Equiv : unsigned int GetGateMode(int * piGatemode);
%
% SEE ALSO : GetCapabilities SetGateMode 
[ret, piGatemode] = atmcdmex('GetGateMode');

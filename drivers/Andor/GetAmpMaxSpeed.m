function [ret, speed] = GetAmpMaxSpeed(index)
% GetAmpMaxSpeed This function will return the maximum available horizontal shift speed for the amplifier selected by the index parameter.
%
% SYNOPSIS : [ret, speed] = GetAmpMaxSpeed(index)
%
% INPUT index: amplifier index
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Speed returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - The amplifier index is not valid
%        speed: horizontal shift speed
% REMARKS : C++ Equiv : unsigned int GetAmpMaxSpeed(int index, float * speed);
%
% SEE ALSO : GetNumberAmp 
[ret, speed] = atmcdmex('GetAmpMaxSpeed', index);

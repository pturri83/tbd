function [ret] = SetDDGLiteInitialDelay(channel, fDelay)
% SetDDGLiteInitialDelay THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetDDGLiteInitialDelay(channel, fDelay)
%
% INPUT channel: 
%       fDelay: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDDGLiteInitialDelay(AT_DDGLiteChannelId channel, float fDelay);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGLiteInitialDelay', channel, fDelay);

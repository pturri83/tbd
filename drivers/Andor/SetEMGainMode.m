function [ret] = SetEMGainMode(mode)
% SetEMGainMode Set the EM Gain mode to one of the following possible settings.
% Mode 0: The EM Gain is controlled by DAC settings in the range 0-255. Default mode.
% 1: The EM Gain is controlled by DAC settings in the range 0-4095.
% 2: Linear mode.
% 3: Real EM gain
% To access higher gain values (if available) it is necessary to enable advanced EM gain, see SetEMAdvanced.
%
% SYNOPSIS : [ret] = SetEMGainMode(mode)
%
% INPUT mode: EM Gain mode.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - EM Gain mode invalid.
% REMARKS : C++ Equiv : unsigned int SetEMGainMode(int mode);
%
% SEE ALSO : 
[ret] = atmcdmex('SetEMGainMode', mode);

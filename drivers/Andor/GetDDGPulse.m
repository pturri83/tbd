function [ret, Delay, Width] = GetDDGPulse(wid, resolution)
% GetDDGPulse This function attempts to find a laser pulse in a user-defined region with a given resolution. The values returned will provide an estimation of the location of the pulse.
%
% SYNOPSIS : [ret, Delay, Width] = GetDDGPulse(wid, resolution)
%
% INPUT wid: the time in picoseconds of the region to be searched.
%       resolution: the minimum gate pulse used to locate the laser.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Location returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_ERROR_ACK - Unable to communicate with card.
%        Delay: the approximate start of the laser pulse.
%        Width: the pulse width, which encapsulated the laser pulse.
% REMARKS : C++ Equiv : unsigned int GetDDGPulse(double wid, double resolution, double * Delay, double * Width);
%
% SEE ALSO : 
[ret, Delay, Width] = atmcdmex('GetDDGPulse', wid, resolution);

function [ret, threshold] = Filter_GetThreshold()
% Filter_GetThreshold Returns the current Noise Filter threshold value.
%
% SYNOPSIS : [ret, threshold] = Filter_GetThreshold()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Threshold returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED - Noise Filter processing not available for this camera.
%          DRV_P1INVALID - Invalid threshold (i.e. NULL pointer).
%        threshold: The current threshold value.
% REMARKS : C++ Equiv : unsigned int Filter_GetThreshold(float * threshold);
%
% SEE ALSO : Filter_SetThreshold 
[ret, threshold] = atmcdmex('Filter_GetThreshold');

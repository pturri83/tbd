function [ret] = SetDelayGenerator(board, address, typ)
% SetDelayGenerator This function sets parameters to control the delay generator through the GPIB card in your computer.
%
% SYNOPSIS : [ret] = SetDelayGenerator(board, address, typ)
%
% INPUT board: The GPIB board number of the card used to interface with the Delay Generator.
%         short - address: The number that allows the GPIB board to identify and send commands to the delay generator.
%       address: The number that allows the GPIB board to identify and send commands to the delay generator. 	

%       typ: The type of your Delay Generator.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Delay Generator set up.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - GPIB board invalid.
%          DRV_P2INVALID - GPIB address invalid
%          DRV_P3INVALID - Delay generator type invalid.
% REMARKS : C++ Equiv : unsigned int SetDelayGenerator(int board, short address, int typ);
%
% SEE ALSO : SetGate 
[ret] = atmcdmex('SetDelayGenerator', board, address, typ);

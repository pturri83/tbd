function [ret] = SetFVBHBin(bin)
% SetFVBHBin This function sets the horizontal binning used when acquiring in Full Vertical Binned read mode.
%
% SYNOPSIS : [ret] = SetFVBHBin(bin)
%
% INPUT bin: Binning size.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Binning set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid binning size.
% REMARKS : C++ Equiv : unsigned int SetFVBHBin(int bin);
%
% SEE ALSO : SetReadMode 
[ret] = atmcdmex('SetFVBHBin', bin);

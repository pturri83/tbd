function [ret, MaxBinning] = GetMaximumBinning(ReadMode, HorzVert)
% GetMaximumBinning This function will return the maximum binning allowable in either the vertical or horizontal dimension for a particular readout mode.
%
% SYNOPSIS : [ret, MaxBinning] = GetMaximumBinning(ReadMode, HorzVert)
%
% INPUT ReadMode: The readout mode for which to retrieve the maximum binning (see SetReadMode for possible values).
%       HorzVert: 0 to retrieve horizontal binning limit, 1 to retreive limit in the vertical.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Maximum Binning returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_P1INVALID - Invalid Readmode
%          DRV_P2INVALID - HorzVert not equal to 0 or 1
%          DRV_P3INVALID - Invalid MaxBinning address (i.e. NULL)
%        MaxBinning: Will contain the Maximum binning value on return.
% REMARKS : C++ Equiv : unsigned int GetMaximumBinning(int ReadMode, int HorzVert, int * MaxBinning);
%
% SEE ALSO : GetMinimumImageLength SetReadMode 
[ret, MaxBinning] = atmcdmex('GetMaximumBinning', ReadMode, HorzVert);

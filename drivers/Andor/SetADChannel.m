function [ret] = SetADChannel(channel)
% SetADChannel This function will set the AD channel to one of the possible A-Ds of the system. This AD channel will be used for all subsequent operations performed by the system.
%
% SYNOPSIS : [ret] = SetADChannel(channel)
%
% INPUT channel: the channel to be used 0 to GetNumberADChannelsGetNumberADChannels-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - AD channel set.
%          DRV_P1INVALID - Index is out of range.
% REMARKS : C++ Equiv : unsigned int SetADChannel(int channel);
%
% SEE ALSO : GetNumberADChannels 
[ret] = atmcdmex('SetADChannel', channel);

function [ret] = SetSifComment(comment)
% SetSifComment This function will set the user text that will be added to any sif files created with the SaveAsSif function. The stored comment can be cleared by passing NULL or an empty text string.
%
% SYNOPSIS : [ret] = SetSifComment(comment)
%
% INPUT comment: The comment to add to new sif files.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Sif comment set.
% REMARKS : C++ Equiv : unsigned int SetSifComment(char * comment);
%
% SEE ALSO : SaveAsSif SaveAsCommentedSif SaveAsSif SetReadMode 
[ret] = atmcdmex('SetSifComment', comment);

function [ret] = SetOverlapMode(mode)
% SetOverlapMode This function will set whether an acquisition will readout in Overlap Mode. If the acquisition mode is Single Scan or Fast Kinetics this call will have no affect.
%
% SYNOPSIS : [ret] = SetOverlapMode(mode)
%
% INPUT mode: mode
%         0 - OFF
%         1 - ON
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Overlap mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid parameter.
% REMARKS : C++ Equiv : unsigned int SetOverlapMode(int mode);
%
% SEE ALSO : SetAcquisitionMode 
[ret] = atmcdmex('SetOverlapMode', mode);

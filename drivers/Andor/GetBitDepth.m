function [ret, depth] = GetBitDepth(channel)
% GetBitDepth This function will retrieve the size in bits of the dynamic range for any available AD channel.
%
% SYNOPSIS : [ret, depth] = GetBitDepth(channel)
%
% INPUT channel: the AD channel.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Depth returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - Invalid channel
%        depth: dynamic range in bits
% REMARKS : C++ Equiv : unsigned int GetBitDepth(int channel, int * depth);
%
% SEE ALSO : GetNumberADChannels SetADChannel 
[ret, depth] = atmcdmex('GetBitDepth', channel);

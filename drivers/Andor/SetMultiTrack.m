function [ret, bottom, gap] = SetMultiTrack(number, height, offset)
% SetMultiTrack This function will set the multi-Track parameters. The tracks are automatically spread evenly over the detector. Validation of the parameters is carried out in the following order:
% * Number of tracks,
% * Track height
% * Offset.
% The first pixels row of the first track is returned via bottom.
% The number of rows between each track is returned via gap.
%
% SYNOPSIS : [ret, bottom, gap] = SetMultiTrack(number, height, offset)
%
% INPUT number: number tracks (1 to number of vertical pixels)
%       height: height of each track (>0 (maximum depends on number of tracks))
%       offset: vertical displacement of tracks.   (depends on number of tracks and track height)
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Number of tracks invalid.
%          DRV_P2INVALID - Track height invalid.
%          DRV_P3INVALID - Offset invalid.
%        bottom: first pixels row of the first track
%        gap: number of rows between each track (could be 0)
% REMARKS : C++ Equiv : unsigned int SetMultiTrack(int number, int height, int offset, int * bottom, int * gap);
%
% SEE ALSO : SetReadMode StartAcquisition SetRandomTracks 
[ret, bottom, gap] = atmcdmex('SetMultiTrack', number, height, offset);

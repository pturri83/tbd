function [ret] = SetDDGOpticalWidthEnabled(uiEnabled)
% SetDDGOpticalWidthEnabled This function can be used to configure a system to use optical gate width.
%
% SYNOPSIS : [ret] = SetDDGOpticalWidthEnabled(uiEnabled)
%
% INPUT uiEnabled: optical gate width option (0 - Off, 1 - On).
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Optical gate width not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
% REMARKS : C++ Equiv : unsigned int SetDDGOpticalWidthEnabled(at_u32 uiEnabled);
%
% SEE ALSO : GetCapabilities GetDDGTTLGateWidth GetDDGOpticalWidthEnabled 
[ret] = atmcdmex('SetDDGOpticalWidthEnabled', uiEnabled);

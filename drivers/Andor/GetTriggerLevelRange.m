function [ret, minimum, maximum] = GetTriggerLevelRange()
% GetTriggerLevelRange This function returns the valid range of triggers in volts which the system can use.
%
% SYNOPSIS : [ret, minimum, maximum] = GetTriggerLevelRange()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Levels returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Trigger levels not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%          DRV_P2INVALID - minimum has invalid memory address.
%        minimum: minimum trigger voltage
%        maximum: maximum trigger voltage
% REMARKS : C++ Equiv : unsigned int GetTriggerLevelRange(float * minimum, float * maximum);
%
% SEE ALSO : GetCapabilities SetTriggerLevel 
[ret, minimum, maximum] = atmcdmex('GetTriggerLevelRange');

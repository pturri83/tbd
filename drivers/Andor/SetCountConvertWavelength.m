function [ret] = SetCountConvertWavelength(wavelength)
% SetCountConvertWavelength This function configures the wavelength used in Count Convert mode.
%
% SYNOPSIS : [ret] = SetCountConvertWavelength(wavelength)
%
% INPUT wavelength: wavelength used to determine QE
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Count Convert wavelength set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_AVAILABLE - Count Convert not available for this camera
%          DRV_P1INVALID - Wavelength value was out of range.
% REMARKS : C++ Equiv : unsigned int SetCountConvertWavelength(float wavelength);
%
% SEE ALSO : GetCapabilities SetCountConvertMode 
[ret] = atmcdmex('SetCountConvertWavelength', wavelength);

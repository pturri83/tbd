function [ret, mintemp, maxtemp] = GetTemperatureRange()
% GetTemperatureRange This function returns the valid range of temperatures in centigrade to which the detector can be cooled.
%
% SYNOPSIS : [ret, mintemp, maxtemp] = GetTemperatureRange()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Temperature range returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%        mintemp: minimum temperature
%        maxtemp: maximum temperature
% REMARKS : C++ Equiv : unsigned int GetTemperatureRange(int * mintemp, int * maxtemp);
%
% SEE ALSO : GetTemperature GetTemperatureF SetTemperature CoolerON CoolerOFF 
[ret, mintemp, maxtemp] = atmcdmex('GetTemperatureRange');

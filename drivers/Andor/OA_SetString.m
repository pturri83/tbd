function [ret] = OA_SetString(pcModeName, pcModeParam, pcStringValue, uiStringLen)
% OA_SetString This function is used to set values for string type acquisition parameters where the
% new values are stored in memory.  To commit changes to file call WriteToFile().
%
% SYNOPSIS : [ret] = OA_SetString(pcModeName, pcModeParam, pcStringValue, uiStringLen)
%
% INPUT pcModeName: The name of the mode for which an acquisition parameter is to be edited.
%       pcModeParam: The name of the acquisition parameter to be edited.
%       pcStringValue: The value to assign to the acquisition parameter.
%       uiStringLen: The length of the input string.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null mode name.
%          DRV_P2INVALID - Null mode parameter.
%          DRV_P3INVALID - Null string value.
%          DRV_P4INVALID - Invalid string length
%          DRV_OA_INVALID_STRING_LENGTH - One or more of the string parameters has an invalid length, i.e. > 255.
%          DRV_OA_MODE_DOES_NOT_EXIST - The Mode does not exist.
% REMARKS : C++ Equiv : unsigned int OA_SetString(const char * pcModeName, const char * pcModeParam, char * pcStringValue, const int uiStringLen);
%
% SEE ALSO : OA_GetString OA_EnableMode OA_WriteToFile 
[ret] = atmcdmex('OA_SetString', pcModeName, pcModeParam, pcStringValue, uiStringLen);

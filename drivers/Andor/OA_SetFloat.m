function [ret] = OA_SetFloat(pcModeName, pcModeParam, fFloatValue)
% OA_SetFloat This function is used to set values for floating point type acquisition parameters where
% the new values are stored in memory.  To commit changes to file call WriteToFile().
%
% SYNOPSIS : [ret] = OA_SetFloat(pcModeName, pcModeParam, fFloatValue)
%
% INPUT pcModeName: The name of the mode for which an acquisition parameter will be edited.
%       pcModeParam: The name of the acquisition parameter to be edited.
%       fFloatValue: The value to assign to the acquisition parameter.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null mode name.
%          DRV_P2INVALID - Null mode parameter.
%          DRV_OA_INVALID_STRING_LENGTH - One or more of the string parameters has an invalid length, i.e. > 255.
%          DRV_OA_MODE_DOES_NOT_EXIST - The Mode does not exist.
% REMARKS : C++ Equiv : unsigned int OA_SetFloat(const char * pcModeName, const char * pcModeParam, const float fFloatValue);
%
% SEE ALSO : OA_GetFloat OA_EnableMode OA_WriteToFile 
[ret] = atmcdmex('OA_SetFloat', pcModeName, pcModeParam, fFloatValue);

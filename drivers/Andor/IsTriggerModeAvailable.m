function [ret] = IsTriggerModeAvailable(iTriggerMode)
% IsTriggerModeAvailable This function checks if the hardware and current settings permit the use of the specified trigger mode.
%
% SYNOPSIS : [ret] = IsTriggerModeAvailable(iTriggerMode)
%
% INPUT iTriggerMode: Trigger mode to check.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Trigger mode available
%          DRV_NOT_INITIALIZED - System not initialize
%          DRV_INVALID_MODE - Not a valid mode
% REMARKS : C++ Equiv : unsigned int IsTriggerModeAvailable(int iTriggerMode);
%
% SEE ALSO : SetTriggerMode 
[ret] = atmcdmex('IsTriggerModeAvailable', iTriggerMode);

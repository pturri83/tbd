function [ret, flag] = GetPhosphorStatus()
% GetPhosphorStatus This function will return if the phosphor has saturated.
%
% SYNOPSIS : [ret, flag] = GetPhosphorStatus()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Phosphor status not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%        flag: The status of the phosphor
%          0 - Normal
%          1 - Saturated
% REMARKS : C++ Equiv : unsigned int GetPhosphorStatus(int * flag);
%
% SEE ALSO : 
[ret, flag] = atmcdmex('GetPhosphorStatus');

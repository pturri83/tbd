function [ret, arr] = SetBackground(size)
% SetBackground THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, arr] = SetBackground(size)
%
% INPUT size: 
% OUTPUT ret: Return Code: 
%        arr: 
% REMARKS : C++ Equiv : unsigned int SetBackground(at_32 * arr, long size);
%
% SEE ALSO : 
[ret, arr] = atmcdmex('SetBackground', size);

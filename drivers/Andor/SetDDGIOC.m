function [ret] = SetDDGIOC(state)
% SetDDGIOC This function activates the integrate on chip (IOC) option.
%
% SYNOPSIS : [ret] = SetDDGIOC(state)
%
% INPUT state: ON/OFF switch for the IOC option.
%         0 - to switch IOC OFF.
%         1 - to switch IOC ON.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - IOC option accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_SUPPORTED - IOC not supported.
%          DRV_ERROR_ACK - Unable to communicate with system.
% REMARKS : C++ Equiv : unsigned int SetDDGIOC(int state);
%
% SEE ALSO : GetCapabilities SetDDGIOCFrequency GetDDGIOCFrequency SetDDGIOCNumber GetDDGIOCNumber GetDDGIOCPulses 
[ret] = atmcdmex('SetDDGIOC', state);

function [ret] = SetIsolatedCropModeEx(active, cropheight, cropwidth, vbin, hbin, cropleft, cropbottom)
% SetIsolatedCropModeEx This function effectively reduces the dimensions of the CCD by excluding some rows or columns to achieve higher throughput. This feature is currently only available for iXon Ultra and can only be used in Image readout mode with the EM output amplifier.
% Note: It is important to ensure that no light falls on the excluded region otherwise the acquired data will be corrupted.
% The following centralized regions of interest are recommended to be used with this mode to achieve the fastest possible frame rates. The table below shows the optimally positioned ROI coordinates recommended to be used with this mode:
% ROI
% Crop Left Start Position
% Crop Right Position
% Crop Bottom Start Position
% Crop Top Position
% 32 x 32
% 241
% 272
% 240
% 271
% 64 x 64
% 219
% 282
% 224
% 287
% 96 x 96
% 209
% 304
% 208
% 303
% 128 x 128
% 189
% 316
% 192
% 319
% 192 x 192
% 157
% 348
% 160
% 351
% 256 x 256
% 123
% 378
% 128
% 383
% 496 x 4
% 8
% 503
% 254
% 257
% 496 x 8
% 8
% 503
% 252
% 259
% 496 x 16
% 8
% 503
% 249
% 262
%
% SYNOPSIS : [ret] = SetIsolatedCropModeEx(active, cropheight, cropwidth, vbin, hbin, cropleft, cropbottom)
%
% INPUT active: Crop mode active.
%         1 - Crop mode is ON.
%         0 - Crop mode is OFF.
%       cropheight: The selected crop height. This value must be between 1 and the CCD height.
%       cropwidth: The selected crop width. This value must be between 1 and the CCD width.
%       vbin: vbinThe selected vertical binning.
%       hbin: hbinThe selected horizontal binning.
%       cropleft: The selected crop left start position
%       cropbottom: The selected crop bottom start position
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ACQUIRING - Acquisition in progress
%          DRV_P1INVALID - active parameter was not zero or one
%          DRV_P2INVALID - Invalid crop height
%          DRV_P3INVALID - Invalid crop width
%          DRV_P4INVALID - Invalid vertical binning
%          DRV_P5INVALID - Invalid horizontal binning
%          DRV_P6INVALID - Invalid crop left start position
%          DRV_P7INVALID - Invalid crop bottom start position
%          DRV_NOT_SUPPORTED - The camera does not support isolated crop mode
%          DRV_NOT_AVAILABLE - Invalid read mode
% REMARKS : C++ Equiv : unsigned int SetIsolatedCropModeEx(int active, int cropheight, int cropwidth, int vbin, int hbin, int cropleft, int cropbottom);
%
% SEE ALSO : GetDetector SetReadMode 
[ret] = atmcdmex('SetIsolatedCropModeEx', active, cropheight, cropwidth, vbin, hbin, cropleft, cropbottom);

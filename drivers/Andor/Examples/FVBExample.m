
disp('Single Scan Example')
% init system

disp('Initialising Camera');
ret=AndorInitialize('');
CheckError(ret);

disp('Configuring Acquisition');
[ret]=CoolerON();                             %   Turn on temperature cooler
CheckWarning(ret);
[ret]=SetAcquisitionMode(1);                  %   Set acquisition mode; 1 for Single Scan
CheckWarning(ret);
[ret]=SetExposureTime(0.02);                  %   Set exposure time in second
CheckWarning(ret);
[ret]=SetReadMode(0);                         %   Set read mode; 4 for FVB
CheckWarning(ret);
[ret]=SetTriggerMode(0);                      %   Set internal trigger mode
CheckWarning(ret);
[ret]=SetShutter(1, 1, 0, 0);                 %   Open Shutter
CheckWarning(ret);
[ret,XPixels, YPixels]=GetDetector();         %   Get the CCD size
CheckWarning(ret);
[ret]=SetImage(1, 1, 1, XPixels, 1, YPixels); %   Set the image size
CheckWarning(ret);


disp('Starting Acquisition');
[ret] = StartAcquisition();                   
CheckWarning(ret);

[ret,gstatus]=AndorGetStatus;
CheckWarning(ret);
while(gstatus ~= atmcd.DRV_IDLE)
  pause(1.0);
  disp('Acquiring');
  [ret,gstatus]=AndorGetStatus;
  CheckWarning(ret);
end


[ret, imageData] = GetMostRecentImage(XPixels);
CheckWarning(ret);

if ret == atmcd.DRV_SUCCESS
    plot(imageData);
end



disp('Acquisition Complete! Cleaning Up and Shutting Down');
[ret]=AbortAcquisition;
CheckWarning(ret);
[ret]=SetShutter(1, 2, 1, 1);
CheckWarning(ret);
[ret]=AndorShutDown;
CheckWarning(ret);


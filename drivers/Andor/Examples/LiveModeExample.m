
disp('iCam Live Mode Demo')
% init system

disp('Initialising Camera');
ret=AndorInitialize('');
CheckError(ret);

disp('Configuring Acquisition');
[ret]=CoolerON();                             %   Turn on temperature cooler
CheckWarning(ret);
[ret]=SetAcquisitionMode(5);                  %   Set acquisition mode; 5 for RTA
CheckWarning(ret);
[ret]=SetExposureTime(0.02);                  %   Set exposure time in second
CheckWarning(ret);
[ret]=SetReadMode(4);                         %   Set read mode; 4 for Image
CheckWarning(ret);
[ret]=SetTriggerMode(10);                     %   Set Software trigger mode
useSoftwareTrigger = true;

if ret == atmcd.DRV_INVALID_TRIGGER_MODE
    disp('Software trigger not available, using Internal trigger instead')
    SetTriggerMode(0);                        %   Set internal trigger mode
    useSoftwareTrigger = false;
end

CheckWarning(ret);
[ret]=SetShutter(1, 1, 0, 0);                 %   Open Shutter
CheckWarning(ret);
[ret,XPixels, YPixels]=GetDetector;           %   Get the CCD size
CheckWarning(ret);
[ret]=SetImage(1, 1, 1, XPixels, 1, YPixels); %   Set the image size
CheckWarning(ret);


disp('Starting Acquisition');
[ret] = StartAcquisition();                   
CheckWarning(ret);



I = zeros(YPixels,XPixels);
h = imagesc(I);
colormap(gray);

warndlg('To Abort the acquisition close the image display.','Starting Acquisition');
while(get(0,'CurrentFigure'))
    
    if useSoftwareTrigger == true
        [ret] = SendSoftwareTrigger();
        CheckWarning(ret);
        
        [ret] = WaitForAcquisition();
        CheckWarning(ret);
    end
    
    [ret, imageData] = GetMostRecentImage(XPixels * YPixels);
    CheckWarning(ret);
    
    if ret == atmcd.DRV_SUCCESS
        %display the acquired image
        I=flipdim(transpose(reshape(imageData, XPixels, YPixels)),1);
        set(h,'CData',I);
        drawnow;
    end
end


disp('Acquisition Complete! Cleaning Up and Shutting Down');
[ret]=AbortAcquisition;
CheckWarning(ret);
[ret]=SetShutter(1, 2, 1, 1);
CheckWarning(ret);
[ret]=AndorShutDown;
CheckWarning(ret);



disp('Kinetic Series')
% init system

disp('Initialising Camera');
ret=AndorInitialize('');
CheckError(ret);

disp('Configuring Acquisition');
[ret]=CoolerON();                             %   Turn on temperature cooler
CheckWarning(ret);
[ret]=SetAcquisitionMode(3);                  %   Set acquisition mode; 3 for Kinetic Series
CheckWarning(ret);

prompt = {'Enter Acquisition name','Enter number of images'};
dlg_title = 'Configure acquisition';
num_lines = 1;
def = {'acquisition','10'};
answer = inputdlg(prompt,dlg_title,num_lines,def);

filename = cell2mat(answer(1));
frameCount = str2double(cell2mat(answer(2)));

[ret]=SetNumberKinetics(frameCount);
CheckWarning(ret);
[ret]=SetExposureTime(0.02);                  %   Set exposure time in second
CheckWarning(ret);
[ret]=SetReadMode(4);                         %   Set read mode; 4 for Image
CheckWarning(ret);
[ret]=SetTriggerMode(0);                      %   Set internal trigger mode
CheckWarning(ret);
[ret]=SetShutter(1, 1, 0, 0);                 %   Open Shutter
CheckWarning(ret);
[ret,XPixels, YPixels]=GetDetector;           %   Get the CCD size
CheckWarning(ret);
[ret]=SetImage(1, 1, 1, XPixels, 1, YPixels); %   Set the image size
CheckWarning(ret);


disp('Starting Acquisition');
[ret] = StartAcquisition();                   
CheckWarning(ret);

currentSeries = 0;
while(currentSeries < frameCount)

    [ret, imageData] = GetOldestImage(XPixels * YPixels);
    
    if ret == atmcd.DRV_SUCCESS % data returned
        thisFilename = strcat(filename, num2str(currentSeries+1), '.tiff');
        disp(['Writing Image ', num2str(currentSeries+1), '/',num2str(frameCount),' to disk']);
        I = flipdim(transpose(reshape(imageData, XPixels, YPixels)),1);
        imwrite(uint16(I),thisFilename) % saves to current directory
        currentSeries=currentSeries+1;
    end
end


disp('Acquisition Complete! Cleaning Up and Shutting Down');
[ret]=AbortAcquisition;
CheckWarning(ret);
[ret]=SetShutter(1, 2, 1, 1);
CheckWarning(ret);
[ret]=AndorShutDown;
CheckWarning(ret);


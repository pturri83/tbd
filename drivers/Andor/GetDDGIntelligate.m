function [ret, piState] = GetDDGIntelligate()
% GetDDGIntelligate This function gets the current state of intelligate.
%
% SYNOPSIS : [ret, piState] = GetDDGIntelligate()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - intelligate state returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_NOT_SUPPORTED - intelligate not supported.
%          DRV_ERROR_ACK - Unable to communicate with system.
%        piState: current state of the intelligate option (0 - Off, 1 - On).
% REMARKS : C++ Equiv : unsigned int GetDDGIntelligate(int * piState);
%
% SEE ALSO : GetCapabilities SetDDGIntelligate SetDDGInsertionDelay 
[ret, piState] = atmcdmex('GetDDGIntelligate');

function [ret] = SetEMCCDGain(gain)
% SetEMCCDGain Allows the user to change the gain value. The valid range for the gain depends on what gain mode the camera is operating in. See SetEMGainMode to set the mode and GetEMGainRange to get the valid range to work with.  To access higher gain values (>x300) see SetEMAdvanced.
%
% SYNOPSIS : [ret] = SetEMCCDGain(gain)
%
% INPUT gain: amount of gain applied.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Value for gain accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Gain value invalid.
% REMARKS : C++ Equiv : unsigned int SetEMCCDGain(int gain);
%
% SEE ALSO : GetEMCCDGain SetEMGainMode GetEMGainRange SetEMAdvanced 
[ret] = atmcdmex('SetEMCCDGain', gain);

function [ret, numDevs] = GetNumberDevices()
% GetNumberDevices THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, numDevs] = GetNumberDevices()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        numDevs: 
% REMARKS : C++ Equiv : unsigned int GetNumberDevices(int * numDevs);
%
% SEE ALSO : 
[ret, numDevs] = atmcdmex('GetNumberDevices');

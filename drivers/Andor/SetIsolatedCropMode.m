function [ret] = SetIsolatedCropMode(active, cropheight, cropwidth, vbin, hbin)
% SetIsolatedCropMode This function effectively reduces the dimensions of the CCD by excluding some rows or columns to achieve higher throughput. In isolated crop mode iXon, Newton and iKon cameras can operate in either Full Vertical Binning or Imaging read modes. iDus can operate in Full Vertical Binning read mode only.
% Note: It is important to ensure that no light falls on the excluded region otherwise the acquired data will be corrupted.
%
% SYNOPSIS : [ret] = SetIsolatedCropMode(active, cropheight, cropwidth, vbin, hbin)
%
% INPUT active: Crop mode active
%         1 - Crop mode is ON.
%         Crop - 0 - Crop mode is OFF.
%       cropheight: The selected crop height. This value must be between 1 and the CCD height.
%       cropwidth: The selected crop width. This value must be between 1 and the CCD width.
%       vbin: The selected vertical binning.
%       hbin: The selected horizontal binning.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Parameters set
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ACQUIRING - Acquisition in progress
%          DRV_P1INVALID - active parameter was not zero or one
%          DRV_P2INVALID - Invalid crop height
%          DRV_P3INVALID - Invalid crop width
%          DRV_P4INVALID - Invalid vertical binning
%          DRV_P5INVALID - Invalid horizontal binning
%          DRV_NOT_SUPPORTED - Either the camera does not support isolated Crop mode or the read mode is invalid
% REMARKS : C++ Equiv : unsigned int SetIsolatedCropMode(int active, int cropheight, int cropwidth, int vbin, int hbin);
%
% SEE ALSO : GetDetector SetReadMode 
[ret] = atmcdmex('SetIsolatedCropMode', active, cropheight, cropwidth, vbin, hbin);

function [ret, controllerCardModel] = GetControllerCardModel()
% GetControllerCardModel This function will retrieve the type of PCI controller card included in your system. This function is not applicable for USB systems. The maximum number of characters that can be returned from this function is 10.
%
% SYNOPSIS : [ret, controllerCardModel] = GetControllerCardModel()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Name returned.
%          DRV_NOT_INITIALIZED - System not initialized
%        controllerCardModel: A user allocated array of characters for storage of the controller card model.
% REMARKS : C++ Equiv : unsigned int GetControllerCardModel(char * controllerCardModel);
%
% SEE ALSO : GetHeadModel GetCameraSerialNumber GetCameraInformation GetCapabilities 
[ret, controllerCardModel] = atmcdmex('GetControllerCardModel');

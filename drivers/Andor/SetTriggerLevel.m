function [ret] = SetTriggerLevel(f_level)
% SetTriggerLevel This function sets the trigger voltage which the system will use.
%
% SYNOPSIS : [ret] = SetTriggerLevel(f_level)
%
% INPUT f_level: trigger voltage
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Level set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Trigger levels not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
% REMARKS : C++ Equiv : unsigned int SetTriggerLevel(float f_level);
%
% SEE ALSO : GetCapabilities GetTriggerLevelRange 
[ret] = atmcdmex('SetTriggerLevel', f_level);

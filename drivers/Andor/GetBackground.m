function [ret, arr] = GetBackground(size)
% GetBackground THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, arr] = GetBackground(size)
%
% INPUT size: 
% OUTPUT ret: Return Code: 
%        arr: 
% REMARKS : C++ Equiv : unsigned int GetBackground(at_32 * arr, long size);
%
% SEE ALSO : 
[ret, arr] = atmcdmex('GetBackground', size);

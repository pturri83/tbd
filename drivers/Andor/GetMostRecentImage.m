function [ret, arr] = GetMostRecentImage(size)
% GetMostRecentImage This function will update the data array with the most recently acquired image in any acquisition mode. The data are returned as long integers (32-bit signed integers). The "array" must be exactly the same size as the complete image.
%
% SYNOPSIS : [ret, arr] = GetMostRecentImage(size)
%
% INPUT size: total number of pixels.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Image has been copied into array.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Array size is incorrect.
%          DRV_NO_NEW_DATA - There is no new data yet.
%        arr: data storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int GetMostRecentImage(at_32 * arr, unsigned long size);
%
% SEE ALSO : GetMostRecentImage16 GetOldestImage GetOldestImage16 GetImages 
[ret, arr] = atmcdmex('GetMostRecentImage', size);

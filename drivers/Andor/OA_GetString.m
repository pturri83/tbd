function [ret, pcStringValue] = OA_GetString(pcModeName, pcModeParam, uiStringLen)
% OA_GetString This function is used to get the values for string type acquisition parameters.  Values
% are retrieved from memory for the specified mode name.
%
% SYNOPSIS : [ret, pcStringValue] = OA_GetString(pcModeName, pcModeParam, uiStringLen)
%
% INPUT pcModeName: The name of the mode for which an acquisition parameter  will be retrieved.
%       pcModeParam: The name of the acquisition parameter for which a value will be retrieved.
%       uiStringLen: The length of the buffer.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null mode name.
%          DRV_P2INVALID - Null mode parameter.
%          DRV_P3INVALID - Null string value.
%          DRV_P4INVALID - Invalid string length
%        pcStringValue: The buffer to return the value of the acquisition parameter.
% REMARKS : C++ Equiv : unsigned int OA_GetString(const char * pcModeName, const char * pcModeParam, char * pcStringValue, const int uiStringLen);
%
% SEE ALSO : OA_SetString 
[ret, pcStringValue] = atmcdmex('OA_GetString', pcModeName, pcModeParam, uiStringLen);

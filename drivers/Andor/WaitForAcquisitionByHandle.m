function [ret] = WaitForAcquisitionByHandle(cameraHandle)
% WaitForAcquisitionByHandle Whilst using multiple cameras WaitForAcquisitionByHandle can be called after an acquisition is started using StartAcquisition to put the calling thread to sleep until an Acquisition Event occurs. This can be used as a simple alternative to the functionality provided by the SetDriverEvent function, as all Event creation and handling is performed internally by the SDK library. Like the SetDriverEvent functionality it will use less processor resources than continuously polling with the GetStatus function. If you wish to restart the calling thread without waiting for an Acquisition event, call the function CancelWait. An Acquisition Event occurs each time a new image is acquired during an Accumulation, Kinetic Series or Run-Till-Abort acquisition or at the end of a Single Scan Acquisition.
%
% SYNOPSIS : [ret] = WaitForAcquisitionByHandle(cameraHandle)
%
% INPUT cameraHandle: handle of camera to put into wait state.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition Event occurred.
%          DRV_P1INVALID - Handle not valid.
%          DRV_NO_NEW_DATA - Non-Acquisition Event occurred.(eg CancelWait () called)
% REMARKS : C++ Equiv : unsigned int WaitForAcquisitionByHandle(long cameraHandle);
%
% SEE ALSO : CancelWait GetCameraHandle StartAcquisition WaitForAcquisition WaitForAcquisitionTimeOut WaitForAcquisitionByHandleTimeOut 
[ret] = atmcdmex('WaitForAcquisitionByHandle', cameraHandle);

function [ret] = SetComplexImage(numAreas, areas)
% SetComplexImage This is a function that allows the setting up of random tracks with more options that the SetRandomTracks function.
% The minimum number of tracks is 1. The maximum number of tracks is the number of vertical pixels.
% There is a further limit to the number of tracks that can be set due to memory constraints in the camera. It is not a fixed number but depends upon the combinations of the tracks. For example, 20 tracks of different heights will take up more memory than 20 tracks of the same height.
% If attempting to set a series of random tracks and the return code equals DRV_RANDOM_TRACK_ERROR, change the makeup of the tracks to have more repeating heights and gaps so less memory is needed.
% Each track must be defined by a group of six integers.
% -The top and bottom positions of the tracks.
% -The left and right positions for the area of interest within each track
% -The horizontal and vertical binning for each track.
% The positions of the tracks are validated to ensure that the tracks are in increasing order.
% The left and right positions for each track must be the same.
% For iXon the range is between 8 and CCD width, inclusive
% For idus the range must be between 257 and CCD width, inclusive.
% Horizontal binning must be an integer between 1 and 64 inclusive, for iXon.
% Horizontal binning is not implementated for iDus and must be set to 1.
% Vertical binning is used in the following way. A track of:
% 1 10 1 1024 1 2
% is actually implemented as 5 tracks of height 2. . Note that a vertical binning of 1 will have the effect of vertically binning the entire track; otherwise vertical binning will operate as normal.
% 1 2 1 1024 1 1
% 3 4 1 1024 1 1
% 5 6 1 1024 1 1
% 7 8 1 1024 1 1
% 9 10 1 1024 1 1
%
% SYNOPSIS : [ret] = SetComplexImage(numAreas, areas)
%
% INPUT numAreas: number of areas
%       areas: Each track must be defined by a group of six integers. 
-The top and bottom positions of the tracks.
-The left and right positions for the area of interest within each track
-The horizontal and vertical binning for each track.

% OUTPUT ret: Return Code: 
%          Unsigned int - DRV_RANDOM_TRACK_ERROR
%          DRV_SUCCESS - Success
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Number of tracks invalid.
%          DRV_P2INVALID - Track positions invalid.
%          DRV_ERROR_FILELOAD - Serious internal error
% REMARKS : C++ Equiv : unsigned int SetComplexImage(int numAreas, int * areas);
%
% SEE ALSO : SetRandomTracks 
[ret] = atmcdmex('SetComplexImage', numAreas, areas);

function [ret, szVersionInfo] = GetVersionInfo(arr, ui32BufferLen)
% GetVersionInfo This function retrieves version information about different aspects of the Andor system. The information is copied into a passed string buffer. Currently, the version of the SDK and the Device Driver (USB or PCI) is supported.
%
% SYNOPSIS : [ret, szVersionInfo] = GetVersionInfo(arr, ui32BufferLen)
%
% INPUT arr: 
%         AT_SDKVersion - requests the SDK version information
%         AT_DeviceDriverVersion - requests the device driver version
%       ui32BufferLen: The size of the passed character array,
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS		Information returned - Information returned
%          DRV_NOT_INITIALIZED	System not initialized - System not initialized
%          DRV_P1INVALID - Invalid information type requested
%          DRV_P2INVALID - Storage array pointer is NULL
%          DRV_P3INVALID - Size of the storage array is zero
%        szVersionInfo: A user allocated array of characters for storage of the information
% REMARKS : C++ Equiv : unsigned int GetVersionInfo(AT_VersionInfoId arr, char * szVersionInfo, at_u32 ui32BufferLen);
%
% SEE ALSO : GetHeadModel GetCameraSerialNumber GetCameraInformation GetCapabilities 
[ret, szVersionInfo] = atmcdmex('GetVersionInfo', arr, ui32BufferLen);

function [ret] = IsAmplifierAvailable(iamp)
% IsAmplifierAvailable This function checks if the hardware and current settings permit the use of the specified amplifier.
%
% SYNOPSIS : [ret] = IsAmplifierAvailable(iamp)
%
% INPUT iamp: amplifier to check.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Amplifier available
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_INVALID_AMPLIFIER - Not a valid amplifier
% REMARKS : C++ Equiv : unsigned int IsAmplifierAvailable(int iamp);
%
% SEE ALSO : SetHSSpeed 
[ret] = atmcdmex('IsAmplifierAvailable', iamp);

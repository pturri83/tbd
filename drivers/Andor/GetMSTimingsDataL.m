function [ret, TimeOfStart_wYear, TimeOfStart_wMonth, TimeOfStart_wDayOfWeek, TimeOfStart_wDay, TimeOfStart_wHour, TimeOfStart_wMinute, TimeOfStart_wSecond, TimeOfStart_wMilliseconds, pfDifferences] = GetMSTimingsData(inoOfImages)
%  Legacy Wrapper for GetMSTimingsData THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, TimeOfStart_wYear, TimeOfStart_wMonth, TimeOfStart_wDayOfWeek, TimeOfStart_wDay, TimeOfStart_wHour, TimeOfStart_wMinute, TimeOfStart_wSecond, TimeOfStart_wMilliseconds, pfDifferences] = GetMSTimingsData(inoOfImages)
%
% INPUT inoOfImages: 
% OUTPUT ret: Return Code: 
%        TimeOfStart: 
%        pfDifferences: 
% REMARKS : C++ Equiv : unsigned int GetMSTimingsData(SYSTEMTIME * TimeOfStart, float * pfDifferences, int inoOfImages);
%
% SEE ALSO : 
[ret, TimeOfStart, pfDifferences] = atmcdmex('GetMSTimingsData', inoOfImages);
TimeOfStart_wYear = TimeOfStart.wYear;
TimeOfStart_wMonth = TimeOfStart.wMonth;
TimeOfStart_wDayOfWeek = TimeOfStart.wDayOfWeek;
TimeOfStart_wDay = TimeOfStart.wDay;
TimeOfStart_wHour = TimeOfStart.wHour;
TimeOfStart_wMinute = TimeOfStart.wMinute;
TimeOfStart_wSecond = TimeOfStart.wSecond;
TimeOfStart_wMilliseconds = TimeOfStart.wMilliseconds;

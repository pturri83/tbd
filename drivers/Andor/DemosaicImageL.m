function [ret, red, green, blue] = DemosaicImage(grey, info_iX, info_iY, info_iAlgorithm, info_iXPhase, info_iYPhase, info_iBackground)
%  Legacy Wrapper for DemosaicImage For colour sensors only
% Demosaics an image taken with a CYMG CCD into RGB using the parameters stored in info. Below is the ColorDemosaicInfo structure definition and a description of its members:
% struct COLORDEMOSAICINFO {
% int iX; // Number of X pixels. Must be >2.
% int iY; // Number of Y pixels. Must be >2.
% int iAlgorithm; // Algorithm to demosaic image.
% int iXPhase; // First pixel in data (Cyan or Yellow/Magenta or Green).
% int iYPhase; // First pixel in data (Cyan or Yellow/Magenta or Green).
% int iBackground; // Background to remove from raw data when demosaicing.
% ColorDemosaicInfo;
% * iX and iY are the image dimensions. The number of elements in the input red, green and blue arrays is iX x iY.
% * iAlgorithm sets the algorithm to use: 0 for a 2x2 matrix demosaic algorithm or 1 for a 3x3 one.
% The CYMG CCD pattern can be broken into cells of 2x4 pixels, e.g.:
% * iXPhase and iYPhase store what colour is the bottom-left pixel.
% * iBackground sets the numerical value to be removed from every pixel in the input image before demosaicing is done.
%
% SYNOPSIS : [ret, red, green, blue] = DemosaicImage(grey, info_iX, info_iY, info_iAlgorithm, info_iXPhase, info_iYPhase, info_iBackground)
%
% INPUT grey: image to demosaic
%       info: demosaic information structure.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Image demosaiced
%          DRV_P1INVALID - Invalid pointer (i.e. NULL).
%          DRV_P2INVALID - Invalid pointer (i.e. NULL).
%          DRV_P3INVALID - Invalid pointer (i.e. NULL).
%          DRV_P4INVALID - Invalid pointer (i.e. NULL).
%          DRV_P5INVALID - One or more parameters in info is out of range
%        red: the red plane storage allocated by the user.
%        green: the green plane storage allocated by the user.
%        blue: the blue plane storage allocated by the user.
% REMARKS : C++ Equiv : unsigned int DemosaicImage(WORD * grey, WORD * red, WORD * green, WORD * blue, ColorDemosaicInfo * info);
%
% SEE ALSO : GetMostRecentColorImage16 WhiteBalance 
info.iX = info_iX;
info.iY = info_iY;
info.iAlgorithm = info_iAlgorithm;
info.iXPhase = info_iXPhase;
info.iYPhase = info_iYPhase;
info.iBackground = info_iBackground;
[ret, red, green, blue] = atmcdmex('DemosaicImage', grey, info);

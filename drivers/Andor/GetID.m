function [ret, id] = GetID(devNum)
% GetID THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, id] = GetID(devNum)
%
% INPUT devNum: 
% OUTPUT ret: Return Code: 
%        id: 
% REMARKS : C++ Equiv : unsigned int GetID(int devNum, int * id);
%
% SEE ALSO : 
[ret, id] = atmcdmex('GetID', devNum);

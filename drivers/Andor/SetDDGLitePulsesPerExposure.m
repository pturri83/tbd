function [ret] = SetDDGLitePulsesPerExposure(channel, ui32Pulses)
% SetDDGLitePulsesPerExposure THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetDDGLitePulsesPerExposure(channel, ui32Pulses)
%
% INPUT channel: 
%       ui32Pulses: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetDDGLitePulsesPerExposure(AT_DDGLiteChannelId channel, at_u32 ui32Pulses);
%
% SEE ALSO : 
[ret] = atmcdmex('SetDDGLitePulsesPerExposure', channel, ui32Pulses);

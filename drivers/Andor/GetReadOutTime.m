function [ret, ReadOutTime] = GetReadOutTime()
% GetReadOutTime This function will return the time to readout data from a sensor. This function should be used after all the acquisitions settings have been set, e.g. SetExposureTimeSetExposureTime, SetKineticCycleTimeSetKineticCycleTime and SetReadModeSetReadMode etc. The value returned is the actual times used in subsequent acquisitions.
%
% SYNOPSIS : [ret, ReadOutTime] = GetReadOutTime()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Timing information returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_CODES - Error communicating with camera.
%        ReadOutTime: valid readout time in seconds
% REMARKS : C++ Equiv : unsigned int GetReadOutTime(float * ReadOutTime);
%
% SEE ALSO : GetAcquisitionTimings GetKeepCleanTime 
[ret, ReadOutTime] = atmcdmex('GetReadOutTime');

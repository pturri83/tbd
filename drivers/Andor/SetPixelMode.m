function [ret] = SetPixelMode(bitdepth, colormode)
% SetPixelMode THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret] = SetPixelMode(bitdepth, colormode)
%
% INPUT bitdepth: 
%       colormode: 
% OUTPUT ret: Return Code: 
% REMARKS : C++ Equiv : unsigned int SetPixelMode(int bitdepth, int colormode);
%
% SEE ALSO : 
[ret] = atmcdmex('SetPixelMode', bitdepth, colormode);

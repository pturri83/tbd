function [ret] = SetCameraStatusEnable(Enable)
% SetCameraStatusEnable Use this function to Mask out certain types of acquisition status events. The default is to notify on every type of event but this may cause missed events if different types of event occur very close together. The bits in the mask correspond to the following event types:
% Use0 - Fire pulse down event
% Use1 - Fire pulse up event
% Set the corresponding bit to 0 to disable the event type and 1 to enable the event type.
%
% SYNOPSIS : [ret] = SetCameraStatusEnable(Enable)
%
% INPUT Enable: bitmask with bits set for those events about which you wish to be notified.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Mask Set.
%          DRV_VXDNOTINSTALLED - Device Driver not installed.
% REMARKS : C++ Equiv : unsigned int SetCameraStatusEnable(DWORD Enable);
%
% SEE ALSO : SetAcqStatusEvent SetPCIMode 
[ret] = atmcdmex('SetCameraStatusEnable', Enable);

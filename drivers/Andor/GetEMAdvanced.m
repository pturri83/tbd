function [ret, state] = GetEMAdvanced()
% GetEMAdvanced Returns the current Advanced gain setting.
%
% SYNOPSIS : [ret, state] = GetEMAdvanced()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Advanced state returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - state has invalid memory address.
%        state: current EM advanced gain setting
% REMARKS : C++ Equiv : unsigned int GetEMAdvanced(int * state);
%
% SEE ALSO : 
[ret, state] = atmcdmex('GetEMAdvanced');

function [ret, numberPulses] = GetDDGIOCNumber()
% GetDDGIOCNumber This function can be used to return the actual number of pulses that will be triggered. It should only be called once all the conditions of the experiment have been defined.
%
% SYNOPSIS : [ret, numberPulses] = GetDDGIOCNumber()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number returned
%          DRV_NOT_INITIALIZED - System not initialized
%          DRV_ERROR_ACK - Unable to communicate with card
%        numberPulses: the number of integrate on chip pulses triggered within the fire pulse.
% REMARKS : C++ Equiv : unsigned int GetDDGIOCNumber(unsigned long * numberPulses);
%
% SEE ALSO : GetCapabilities SetDDGIOCFrequency GetDDGIOCFrequency SetDDGIOCNumber GetDDGIOCPulses SetDDGIOC SetDDGIOCFrequency 
[ret, numberPulses] = atmcdmex('GetDDGIOCNumber');

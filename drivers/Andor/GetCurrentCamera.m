function [ret, cameraHandle] = GetCurrentCamera()
% GetCurrentCamera When multiple Andor cameras are installed this function returns the handle of the currently selected one.
%
% SYNOPSIS : [ret, cameraHandle] = GetCurrentCamera()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Camera handle returned.
%        cameraHandle: handle of the currently selected camera
% REMARKS : C++ Equiv : unsigned int GetCurrentCamera(long * cameraHandle);
%
% SEE ALSO : SetCurrentCamera GetAvailableCameras GetCameraHandle 
[ret, cameraHandle] = atmcdmex('GetCurrentCamera');

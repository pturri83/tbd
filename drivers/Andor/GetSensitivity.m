function [ret, sensitivity] = GetSensitivity(channel, horzShift, amplifier, pa)
% GetSensitivity This function returns the sensitivity for a particular speed.
%
% SYNOPSIS : [ret, sensitivity] = GetSensitivity(channel, horzShift, amplifier, pa)
%
% INPUT channel: AD channel index.
%       horzShift: Type of output amplifier.
%       amplifier: Channel speed index.
%       pa: PreAmp gain index.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Sensitivity returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Invalid channel.
%          DRV_P2INVALID - Invalid amplifier.
%          DRV_P3INVALID - Invalid speed index.
%          DRV_P4INVALID - Invalid gain.
%        sensitivity: requested sensitivity.
% REMARKS : C++ Equiv : unsigned int GetSensitivity(int channel, int horzShift, int amplifier, int pa, float * sensitivity);
%
% SEE ALSO : GetCapabilities 
[ret, sensitivity] = atmcdmex('GetSensitivity', channel, horzShift, amplifier, pa);

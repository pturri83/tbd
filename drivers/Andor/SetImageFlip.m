function [ret] = SetImageFlip(iHFlip, iVFlip)
% SetImageFlip This function will cause data output from the SDK to be flipped on one or both axes. This flip is not done in the camera, it occurs after the data is retrieved and will increase processing overhead. If flipping could be implemented by the user more efficiently then use of this function is not recomended. E.g writing to file or displaying on screen.
%
% SYNOPSIS : [ret] = SetImageFlip(iHFlip, iVFlip)
%
% INPUT iHFlip: Sets horizontal flipping.
%       iVFlip: Sets vertical flipping..
%         1 - Enables Flipping
%         0 - Disables Flipping
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_P1INVALID - HFlip parameter invalid.
%          DRV_P2INVALID - VFlip parameter invalid
% REMARKS : C++ Equiv : unsigned int SetImageFlip(int iHFlip, int iVFlip);
%
% SEE ALSO : SetImageRotate 
[ret] = atmcdmex('SetImageFlip', iHFlip, iVFlip);

function [ret] = OA_EnableMode(pcModeName)
% OA_EnableMode This function will set all the parameters associated with the specified mode to be used for all subsequent acquisitions.  The mode specified by the user must be in either the Preset file or the User defined file.
%
% SYNOPSIS : [ret] = OA_EnableMode(pcModeName)
%
% INPUT pcModeName: The mode to be used for all subsequent acquisitions.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted
%          DRV_P1INVALID - Null mode name.
%          DRV_OA_MODE_DOES_NOT_EXIST - Mode name does not exist.
%          DRV_OA_CAMERA_NOT_SUPPORTED - Camera not supported.
% REMARKS : C++ Equiv : unsigned int OA_EnableMode(const char * pcModeName);
%
% SEE ALSO : OA_AddMode 
[ret] = atmcdmex('OA_EnableMode', pcModeName);

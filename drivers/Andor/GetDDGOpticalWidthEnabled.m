function [ret, puiEnabled] = GetDDGOpticalWidthEnabled()
% GetDDGOpticalWidthEnabled This function can be used to check whether optical gate widths are being used.
%
% SYNOPSIS : [ret, puiEnabled] = GetDDGOpticalWidthEnabled()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - State returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_NOT_SUPPORTED DRV_ACQUIRING - Optical gate width not supported.
%          DRV_ERROR_ACK - Acquisition in progress.
%          DRV_P1INVALID - Unable to communicate with system.
%        puiEnabled: optical gate width option (0 - Off, 1 - On).
% REMARKS : C++ Equiv : unsigned int GetDDGOpticalWidthEnabled(at_u32 * puiEnabled);
%
% SEE ALSO : GetCapabilities GetDDGTTLGateWidth 
[ret, puiEnabled] = atmcdmex('GetDDGOpticalWidthEnabled');

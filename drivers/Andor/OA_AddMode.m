function [ret, pcModeName] = OA_AddMode(uiModeNameLen, pcModeDescription, uiModeDescriptionLen)
% OA_AddMode This function will add a mode name and description to memory.  Note that this will not add the mode to file, a subsequent call to OA_WriteToFile must be made.
%
% SYNOPSIS : [ret, pcModeName] = OA_AddMode(uiModeNameLen, pcModeDescription, uiModeDescriptionLen)
%
% INPUT uiModeNameLen: Mode name string length.
%       pcModeDescription: A description of the user defined mode.
%       uiModeDescriptionLen: Mode Description string length.
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS            DRV_P1INVALID           DRV_P3INVALID DRV_OA_INVALID_STRING_LENGTH - All parameters accepted                                Null mode name.                                        Null mode description.                                            One or more parameters have an invalid length, i.e. > 255.
%          DRV_OA_INVALID_NAMING - Mode and description have the same name, this is not valid.
%          DRV_OA_MODE_BUFFER_FULL        DRV_OA_INVALID_CHARS_IN_NAME - Number of modes exceeds limit.             Mode name and/or description contain invalid characters.
%          DRV_OA_MODE_ALREADY_EXISTS - Mode name already exists in the file.
%          DRV_OA_INVALID_CHARS_IN_NAME - Invalid charcters in Mode Name or Mode Description
%        pcModeName: A name for the mode to be defined.
% REMARKS : C++ Equiv : unsigned int OA_AddMode(char * pcModeName, int uiModeNameLen, char * pcModeDescription, int uiModeDescriptionLen);
%
% SEE ALSO : OA_DeleteMode OA_WriteToFile 
[ret, pcModeName] = atmcdmex('OA_AddMode', uiModeNameLen, pcModeDescription, uiModeDescriptionLen);

function [ret, pcListOfModes] = OA_GetUserModeNames()
% OA_GetUserModeNames This function will return the available mode names from a User defined file.  The mode and the User defined file must exist.  The user must allocate enough memory for all of the acquisition parameters.
%
% SYNOPSIS : [ret, pcListOfModes] = OA_GetUserModeNames()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - All parameters accepted.
%          DRV_P1INVALID - Null list of modes.
%          DRV_OA_NULL_ERROR - Invalid pointer.
%        pcListOfModes: A user allocated array of characters for storage of the mode names.  Mode names will be delimited by a ','.
% REMARKS : C++ Equiv : unsigned int OA_GetUserModeNames(char * pcListOfModes);
%
% SEE ALSO : OA_GetNumberOfUserModes 
[ret, pcListOfModes] = atmcdmex('OA_GetUserModeNames');

function [ret] = SetFanMode(mode)
% SetFanMode Allows the user to control the mode of the camera fan. If the system is cooled, the fan should only be turned off for short periods of time. During this time the body of the camera will warm up which could compromise cooling capabilities.
% If the camera body reaches too high a temperature, depends on camera, the buzzer will sound. If this happens, turn off the external power supply and allow the system to stabilize before continuing.
%
% SYNOPSIS : [ret] = SetFanMode(mode)
%
% INPUT mode: Fan mode setting
%         0 - Fan on full.
%         1 - Fan on low.
%         2 - Fan off,
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Value for mode accepted.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_I2CTIMEOUT - I2C command timed out.
%          DRV_I2CDEVNOTFOUND - I2C device not present.
%          DRV_ERROR_ACK - Unable to communicate with card.
%          DRV_P1INVALID - Mode value invalid.
% REMARKS : C++ Equiv : unsigned int SetFanMode(int mode);
%
% SEE ALSO : GetCapabilities 
[ret] = atmcdmex('SetFanMode', mode);

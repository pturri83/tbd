function [ret, number] = GetNumberVerticalSpeeds()
% GetNumberVerticalSpeeds Deprecated see Note:
% As your Andor system may be capable of operating at more than one vertical shift speed this function will return the actual number of speeds available.
%
% SYNOPSIS : [ret, number] = GetNumberVerticalSpeeds()
%
% INPUT none
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Number of speeds returned.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%        number: number of allowed vertical speeds
% REMARKS : C++ Equiv : unsigned int GetNumberVerticalSpeeds(int * number); // deprecated
%
% SEE ALSO : GetVerticalSpeed SetVerticalSpeed 
[ret, number] = atmcdmex('GetNumberVerticalSpeeds');

function [ret] = SetAcquisitionMode(mode)
% SetAcquisitionMode This function will set the acquisition mode to be used on the next StartAcquisitionStartAcquisition.
%
% SYNOPSIS : [ret] = SetAcquisitionMode(mode)
%
% INPUT mode: the acquisition mode.
%         1 - Single Scan
%         2 - Accumulate
%         3 - Kinetics
%         4 - Fast Kinetics
%         5 - Run till abort
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Acquisition mode set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Acquisition Mode invalid.
% REMARKS : C++ Equiv : unsigned int SetAcquisitionMode(int mode);
%
% SEE ALSO : StartAcquisition 
[ret] = atmcdmex('SetAcquisitionMode', mode);

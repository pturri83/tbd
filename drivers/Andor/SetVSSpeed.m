function [ret] = SetVSSpeed(index)
% SetVSSpeed This function will set the vertical speed to be used for subsequent acquisitions
%
% SYNOPSIS : [ret] = SetVSSpeed(index)
%
% INPUT index: index into the vertical speed table
%         0 - to GetNumberVSSpeedsGetNumberVSSpeeds-1
% OUTPUT ret: Return Code: 
%          DRV_SUCCESS - Vertical speed set.
%          DRV_NOT_INITIALIZED - System not initialized.
%          DRV_ACQUIRING - Acquisition in progress.
%          DRV_P1INVALID - Index out of range.
% REMARKS : C++ Equiv : unsigned int SetVSSpeed(int index);
%
% SEE ALSO : GetNumberVSSpeeds GetVSSpeed GetFastestRecommendedVSSpeed 
[ret] = atmcdmex('SetVSSpeed', index);

function [ret, Address1, Address2] = GetPhysicalDMAAddress()
% GetPhysicalDMAAddress THIS FUNCTION IS RESERVED.
%
% SYNOPSIS : [ret, Address1, Address2] = GetPhysicalDMAAddress()
%
% INPUT none
% OUTPUT ret: Return Code: 
%        Address1: 
%        Address2: 
% REMARKS : C++ Equiv : unsigned int GetPhysicalDMAAddress(unsigned long * Address1, unsigned long * Address2);
%
% SEE ALSO : 
[ret, Address1, Address2] = atmcdmex('GetPhysicalDMAAddress');
